# 2022_HS_polybench_parallelization

This repository contains parallel versions of `Seidel-2D` and `Trmm` of the [PolyBench benchmark suite](http://web.cse.ohio-state.edu/~pouchet.2/software/polybench/). 

