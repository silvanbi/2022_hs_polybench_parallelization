#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <mkl_scalapack.h>
#include "mkl.h"
#include "mkl_blacs.h"
#include "mkl_pblas.h"
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "pblas_mkl_trmm_example"

#define MAX(a,b)((a)<(b)?(b):(a))

// Definition of matrix descriptor
typedef MKL_INT MDESC[9];

const double zero = 0.0E+0, one = 1.0E+0, two = 2.0E+0, negone = -1.0E+0;
const MKL_INT i_zero = 0, i_one = 1, i_four = 4, i_five = 5, i_negone = -1;

int main(int argc, char** argv) {

  MDESC descA_local, descB_local, descA_global, descB_global;

  MKL_INT iam, nprocs, ictxt, myrow, mycol, nprow, npcol, mb, nb;
  MKL_INT m, n, m_a, n_a, m_b, n_b;
  MKL_INT lld_local_a, lld_local_b, lld_global_a, lld_global_b;
  MKL_INT info;

  double *A_global, *B_global, *A_local, *B_local;
  double alpha;

  blacs_pinfo_(&iam, &nprocs);

  // Specification of matrix and grid dimensions
  // IBM Example 1
  // https://www.ibm.com/docs/en/pessl/5.4?topic=subroutines-pdtrmm-pztrmm-triangular-matrix-matrix-product
  m = 5;
  n = 3;
  alpha = 1.0;

  mb = 2;
  nb = 2;

  nprow = 2;
  npcol = 2;

  // Init 2D process grid - row-major:
  // 00 | 01
  // 10 | 11
  blacs_get_(&i_negone, &i_zero, &ictxt);
  blacs_gridinit_(&ictxt, "R", &nprow, &npcol);
  blacs_gridinfo_(&ictxt, &nprow, &npcol, &myrow, &mycol);

  // Init global matrix A and B
  if (iam == 0) {
    A_global = (double*) mkl_calloc(m * m, sizeof(double), 64);
    B_global = (double*) mkl_calloc(m * n, sizeof(double), 64);

    // Row-major
    // Not supported by ScaLAPACK / BLACS / PBLAS
    // => trick with transposition upon distribution possible to support this
//    A_global[0] = 3.0;
//    A_global[1] = -1.0;
//    A_global[2] = 2.0;
//    A_global[3] = 2.0;
//    A_global[4] = 1.0;
//
//    A_global[5] = 0.0;
//    A_global[6] = -2.0;
//    A_global[7] = 4.0;
//    A_global[8] = -1.0;
//    A_global[9] = 3.0;
//
//    A_global[10] = 0.0;
//    A_global[11] = 0.0;
//    A_global[12] = -3.0;
//    A_global[13] = 0.0;
//    A_global[14] = 2.0;
//
//    A_global[15] = 0.0;
//    A_global[16] = 0.0;
//    A_global[17] = 0.0;
//    A_global[18] = 4.0;
//    A_global[19] = -2.0;
//
//    A_global[20] = 0.0;
//    A_global[21] = 0.0;
//    A_global[22] = 0.0;
//    A_global[23] = 0.0;
//    A_global[24] = 1.0;
//
//
//    B_global[0] = 2.0;
//    B_global[1] = 3.0;
//    B_global[2] = 1.0;
//
//    B_global[3] = 5.0;
//    B_global[4] = 5.0;
//    B_global[5] = 4.0;
//
//    B_global[6] = 0.0;
//    B_global[7] = 1.0;
//    B_global[8] = 2.0;
//
//    B_global[9] = 3.0;
//    B_global[10] = 1.0;
//    B_global[11] = -3.0;
//
//    B_global[12] = -1.0;
//    B_global[13] = 2.0;
//    B_global[14] = 1.0;

    // Column-major
    A_global[0] = 3.0;
    A_global[1] = 0.0;
    A_global[2] = 0.0;
    A_global[3] = 0.0;
    A_global[4] = 0.0;

    A_global[5] = -1.0;
    A_global[6] = -2.0;
    A_global[7] = 0.0;
    A_global[8] = 0.0;
    A_global[9] = 0.0;

    A_global[10] = 2.0;
    A_global[11] = 4.0;
    A_global[12] = -3.0;
    A_global[13] = 0.0;
    A_global[14] = 0.0;

    A_global[15] = 2.0;
    A_global[16] = -1.0;
    A_global[17] = 0.0;
    A_global[18] = 4.0;
    A_global[19] = 0.0;

    A_global[20] = 1.0;
    A_global[21] = 3.0;
    A_global[22] = 2.0;
    A_global[23] = -2.0;
    A_global[24] = 1.0;


    B_global[0] = 2.0;
    B_global[1] = 5.0;
    B_global[2] = 0.0;
    B_global[3] = 3.0;
    B_global[4] = -1.0;

    B_global[5] = 3.0;
    B_global[6] = 5.0;
    B_global[7] = 1.0;
    B_global[8] = 1.0;
    B_global[9] = 2.0;

    B_global[10] = 1.0;
    B_global[11] = 4.0;
    B_global[12] = 2.0;
    B_global[13] = -3.0;
    B_global[14] = 1.0;

    printf("Data initialized\n");
    print_array_c(m, m, A_global);
    printf("\n");
    print_array_c(m, n, B_global);
  }
  else {
    A_global = NULL;
    B_global = NULL;
  }

  // Calculate local process sub-matrix dimensions
  m_a = numroc_(&m, &mb, &myrow, &i_zero, &nprow);
  n_a = numroc_(&m, &mb, &mycol, &i_zero, &npcol);
  m_b = numroc_(&m, &mb, &myrow, &i_zero, &nprow);
  n_b = numroc_(&n, &nb, &mycol, &i_zero, &npcol);

  // Init local sub-matrices
  A_local = (double*) mkl_calloc(m_a * n_a, sizeof(double), 64);
  B_local = (double*) mkl_calloc(m_b * n_b, sizeof(double), 64);

  // Calculate leading dimensions for bot local and global matrices
  lld_local_a = MAX(1, m_a);
  lld_local_b = MAX(1, m_b);
  lld_global_a = MAX(1, m);
  lld_global_b = MAX(1, m);

  // Prepare matrix descriptors
  descinit_(descA_local, &m, &m, &mb, &mb, &i_zero, &i_zero, &ictxt, &lld_local_a, &info);
  descinit_(descB_local, &m, &n, &mb, &nb, &i_zero, &i_zero, &ictxt, &lld_local_b, &info);
  descinit_(descA_global, &m, &m, &m, &m, &i_zero, &i_zero, &ictxt, &lld_global_a, &info);
  descinit_(descB_global, &m, &n, &m, &n, &i_zero, &i_zero, &ictxt, &lld_global_b, &info);

  // Block-cyclic redistribution of global matrices (alternative approach commented out)
  pdgemr2d_(&m, &m, A_global, &i_one, &i_one, descA_global, A_local, &i_one, &i_one, descA_local, &ictxt);
  pdgemr2d_(&m, &n, B_global, &i_one, &i_one, descB_global, B_local, &i_one, &i_one, descB_local, &ictxt);
//  pdgeadd_("N", &m, &m, &one, A_global, &i_one, &i_one, descA_global, &zero, A_local, &i_one, &i_one, descA_local);
//  pdgeadd_("N", &m, &n, &one, B_global, &i_one, &i_one, descB_global, &zero, B_local, &i_one, &i_one, descB_local);

  // Print local A and B before trmm
  sleep(iam * 2);
  printf("rank %d: A %d x %d, B %d x %d, lld_a %d, lld_b %d\n", iam, m_a, n_a, m_b, n_b, lld_local_a, lld_local_b);
  print_array_c(m_a, n_a, A_local);
  printf("\n");
  print_array_c(m_b, n_b, B_local);
  printf("\n");

  pdtrmm_("L", "U", "N", "N", &m, &n, &alpha, A_local, &i_one, &i_one, descA_local, B_local, &i_one, &i_one, descB_local);

  // Print local B after trmm
  sleep(iam * 2 + 5);
  printf("rank %d: result B %d x %d\n", iam, m_b, n_b);
  print_array_c(m_b, n_b, B_local);
  printf("\n");

  // Gather redistributed result back to global matrix (alternative approach commented out)
  pdgemr2d_(&m, &n, B_local, &i_one, &i_one, descB_local, B_global, &i_one, &i_one, descB_global, &ictxt);
//  pdgeadd_("N", &m, &n, &one, B_local, &i_one, &i_one, descB_local, &zero, B_global, &i_one, &i_one, descB_global);


  // Print global B (result) and cleanup
  if (iam == 0) {
    print_array_c(m, n, B_global);

    mkl_free(A_global);
    mkl_free(B_global);
  }
  mkl_free(A_local);
  mkl_free(B_local);

  blacs_gridexit_(&ictxt);
  blacs_exit_(&i_zero);

  return 0;
}
