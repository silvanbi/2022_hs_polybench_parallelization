#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_blas.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "blas_trmm"

int main(int argc, char** argv) {
  struct timeval start;
  struct timeval end;
  double times[NRUNS];
  int test = 0;
  int m, n;
  double alpha;
  parse_args(argc, argv, &m, &n, &alpha, &test);

  double* A = aligned_alloc(64, m * m * sizeof(double));
  double* B = aligned_alloc(64, m * n * sizeof(double));

  init_data(m, n, A, B);

  for (int run = 0; run < NRUNS; run++) {
    gettimeofday(&start, NULL);
    cblas_dtrmm(CblasRowMajor, CblasLeft, CblasLower, CblasTrans, CblasUnit, m, n, alpha, A, m, B, n);
    gettimeofday(&end, NULL);
    times[run] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
  }

  if (test && NRUNS > 1)
    printf("Test cannot be run for NRUNS > 1\n");
  else if (test)
    run_test(m, n, alpha, A, B);

  printf("%s\n", PGM_NAME);
  print_times(times, NRUNS, 0);
  print_cpu_info(0);

  free(A);
  free(B);
  return 0;
}
