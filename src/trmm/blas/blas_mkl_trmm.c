#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include "mkl.h"
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#ifndef PGM_NAME
#define PGM_NAME "blas_trmm"
#endif

int main(int argc, char** argv) {
  struct timeval start;
  struct timeval end;
  double times[NRUNS];
  int test = 0;
  int m, n;
  double alpha;
  parse_args(argc, argv, &m, &n, &alpha, &test);

  double* A = (double *) mkl_calloc(m * m, sizeof(double), 64);
  double* B = (double *) mkl_calloc(m * n, sizeof(double), 64);

  init_data(m, n, A, B);

  for (int run = 0; run < NRUNS; run++) {
    gettimeofday(&start, NULL);
    dtrmm("L", "L", "T", "U", &m, &n, &alpha, A, &m, B, &n);
    gettimeofday(&end, NULL);
    times[run] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
  }

  if (test && NRUNS > 1)
    printf("Test cannot be run for NRUNS > 1\n");
  else if (test)
    run_test(m, n, alpha, A, B);

  printf("%s\n", PGM_NAME);
  print_times(times, NRUNS, 0);
  print_cpu_info(0);

  mkl_free(A);
  mkl_free(B);
  return 0;
}
