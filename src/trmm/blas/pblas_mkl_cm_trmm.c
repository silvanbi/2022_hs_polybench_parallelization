#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <mkl_scalapack.h>
#include "mkl.h"
#include "mkl_blacs.h"
#include "mkl_pblas.h"
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#ifndef MB
#define MB 2
#endif

#ifndef NB
#define NB 2
#endif

#ifndef NPROW
#define NPROW 2
#endif

#ifndef NPCOL
#define NPCOL 2
#endif

#define PGM_NAME "pblas_mkl_rm_trmm"
#define MAX(a,b)((a)<(b)?(b):(a))

typedef MKL_INT MDESC[9];
const double zero = 0.0E+0, one = 1.0E+0, two = 2.0E+0, negone = -1.0E+0;
const MKL_INT i_zero = 0, i_one = 1, i_two = 2, i_five = 5, i_negone = -1;

int main(int argc, char** argv) {
  struct timeval start;
  struct timeval end;
  double times[NRUNS];
  int test = 0;

  MDESC descA_local, descB_local, descA_global, descB_global;

  MKL_INT rank, nprocs, ictxt, myrow, mycol, nprow, npcol, mb, nb;
  MKL_INT m, n, m_a, n_a, m_b, n_b, m_times;
  MKL_INT lld_local_a, lld_local_b, lld_global_a, lld_global_b;
  MKL_INT rsrc_times, csrc_times;
  MKL_INT info;

  MKL_INT params[2];

  double *A_global, *B_global, *A_local, *B_local;
  double alpha;

  blacs_pinfo_(&rank, &nprocs);

  // Note:
  // - block size (MB) - communication overhead VS load imbalance
  // - NPROW * NPCOL = nprocs
  mb = MB;
  nb = NB;
  nprow = NPROW;
  npcol = NPCOL;
  m_times = NRUNS;

  // Prepare global matrix A and B on rank 0
  if (rank == 0) {
    parse_args(argc, argv, &m, &n, &alpha, &test);
    params[0] = m;
    params[1] = n;
    A_global = (double *) mkl_calloc(m * m, sizeof(double), 64);
    B_global = (double *) mkl_calloc(m * n, sizeof(double), 64);

    init_data(m, n, A_global, B_global);

    // Transpose (row-major → column-major)
    // Commented for measure
//    mkl_dimatcopy('R', 'T', m, m, alpha, A_global, m, m);
//    mkl_dimatcopy('R', 'T', m, n, alpha, B_global, n, m);
  }

  for (int run = 0; run < NRUNS; run++) {
    gettimeofday(&start, NULL);

    blacs_get_(&i_negone, &i_zero, &ictxt);
    blacs_gridinit_(&ictxt, "R", &nprow, &npcol);
    blacs_gridinfo_(&ictxt, &nprow, &npcol, &myrow, &mycol);

    // Parameter redistribution along 2D process grid
    if (rank == 0) {
      igebs2d_(&ictxt, "All", " ", &i_two, &i_one, params, &i_two);
      dgebs2d_(&ictxt, "All", " ", &i_one, &i_one, &alpha, &i_one);
    }
    else {
      igebr2d_(&ictxt, "All", " ", &i_two, &i_one, params, &i_two, &i_zero, &i_zero);
      dgebr2d_(&ictxt, "All", " ", &i_one, &i_one, &alpha, &i_one, &i_zero, &i_zero);
      m = params[0];
      n = params[1];
    }

    // Local matrix setup
    m_a = numroc_(&m, &mb, &myrow, &i_zero, &nprow);
    n_a = numroc_(&m, &nb, &mycol, &i_zero, &npcol);
    m_b = numroc_(&m, &mb, &myrow, &i_zero, &nprow);
    n_b = numroc_(&n, &nb, &mycol, &i_zero, &npcol);

    A_local = (double *) mkl_calloc(m_a * n_a, sizeof(double), 64);
    B_local = (double *) mkl_calloc(m_b * n_b, sizeof(double), 64);

    lld_local_a = MAX(1, m_a);
    lld_local_b = MAX(1, m_b);
    lld_global_a = MAX(1, m);
    lld_global_b = MAX(1, m);

    descinit_(descA_local, &m, &m, &mb, &nb, &i_zero, &i_zero, &ictxt, &lld_local_a, &info);
    descinit_(descB_local, &m, &n, &mb, &nb, &i_zero, &i_zero, &ictxt, &lld_local_b, &info);
    descinit_(descA_global, &m, &m, &m, &m, &i_zero, &i_zero, &ictxt, &lld_global_a, &info);
    descinit_(descB_global, &m, &n, &m, &n, &i_zero, &i_zero, &ictxt, &lld_global_b, &info);

    // 2D process grid block-cyclic redistribution
    pdgeadd_("N", &m, &m, &one, A_global, &i_one, &i_one, descA_global, &zero, A_local, &i_one, &i_one, descA_local);
    pdgeadd_("N", &m, &n, &one, B_global, &i_one, &i_one, descB_global, &zero, B_local, &i_one, &i_one, descB_local);

    pdtrmm_("L", "L", "T", "U", &m, &n, &alpha, A_local, &i_one, &i_one, descA_local, B_local, &i_one, &i_one, descB_local);

    // Gather result back to global matrix B on rank 0
    pdgeadd_("N", &m, &n, &one, B_local, &i_one, &i_one, descB_local, &zero, B_global, &i_one, &i_one, descB_global);

    blacs_gridexit_(&ictxt);
    mkl_free(A_local);
    mkl_free(B_local);

    gettimeofday(&end, NULL);
    times[run] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
  }

  // Gather and print timing arrays via temporary 1D process grid on rank 0
  blacs_get_(&i_negone, &i_zero, &ictxt);
  blacs_gridinit_(&ictxt, "R", &nprocs, &i_one);
  if (rank == 0) {
    print_times(times, NRUNS, 0);
    for (int i = 1; i < nprocs; i++) {
      rsrc_times = i;
      dgerv2d_(&ictxt, &m_times, &i_one, times, &m_times, &rsrc_times, &i_zero);
      print_times(times, NRUNS, i);
    }
  }
  else {
    dgesd2d_(&ictxt, &m_times, &i_one, times, &m_times, &i_zero, &i_zero);
  }
  blacs_gridexit_(&ictxt);

  if (rank == 0) {
    if (test && NRUNS > 1)
      printf("Test cannot be run for NRUNS > 1\n");
    else if (test) {
      // Transpose (column-major → row-major)
      // Commented for measurement
//      mkl_dimatcopy('R', 'T', m, m, alpha, A_global, m, m);
//      mkl_dimatcopy('R', 'T', m, n, alpha, B_global, n, m);
      run_test(m, n, alpha, A_global, B_global);
    }
    mkl_free(A_global);
    mkl_free(B_global);
  }

  print_cpu_info(rank);
  blacs_exit_(&i_zero);

  return 0;
}