target_list_omp=(
    "./m_mkl_blas_p_trmm"
    "./m_omp_trmm12"
    "./m_omp_trmm13"
    "./m_omp_trmm14"
)

target_list_mpi=(
    "./m_mpi_trmm_opt3"
)

NTHREADS_list=(1 2 4 8 16)
size_list=(3680 4640 5848 7368 9288)

make clean && make measure && 

base_dir="measurements"
mkdir -p $base_dir &&
x=0
mkdir ${base_dir}/${x} > /dev/null 2>&1;
while [ $? -ne 0 ]
do
    x=$(( $x + 1 ))
    mkdir ${base_dir}/${x} > /dev/null 2>&1;
done

echo "result directory: ${base_dir}/${x}"

for i in ${!NTHREADS_list[@]}; do
    size=${size_list[$i]}
    N_THREADS=${NTHREADS_list[$i]}
    echo "running $size with $N_THREADS threads measurements"
    for target in ${target_list_omp[@]}; do
        mkdir ${base_dir}/${x}/${target}
        export OMP_NUM_THREADS=$N_THREADS &&
        echo "measuring $target $size $size 1.5" &&
        $target $size $size 1.5 > ${base_dir}/${x}/${target}/${size}_${size}_${N_THREADS}
    done

    for target in ${target_list_mpi[@]}; do
        mkdir ${base_dir}/${x}/${target}
        echo "measuring mpiexec -np $N_THREADS $target $size $size 1.5" &&
        mpiexec -np $N_THREADS $target $size $size 1.5 > ${base_dir}/${x}/${target}/${size}_${size}_${N_THREADS}
    done
    echo "finished $size with $N_THREADS threads"
done