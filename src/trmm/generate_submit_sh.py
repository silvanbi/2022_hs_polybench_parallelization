"""
USE AS FOLLOWS:
1. make sure to configure NTHREADS_list
2. make sure that target_list_seq, target_list_omp, target_list_mpi
   contain all the binaries to benchmark and that these binaries are 
   produced by running "make measure"
3. generate job submission script: python3 generate_submit_sh.py > submit.sh
4. scp submit.sh [nethz-login]@euler.ethz.ch:2022_hs_polybench_parallelization/src/trmm
5. on euler, run env2lmod
6. cd 2022_hs_polybench_parallelization/src/trmm on euler and submit script: sbatch < submit.sh
7. check output: 2022_hs_polybench_parallelization/src/trmm/measurements/[script generation tsd]

"""

from datetime import datetime

NTHREADS_list=[1, 8, 16, 24, 32]

# programs to benchmark
target_list_seq=[
    "./m_original_trmm",
    "./m_blas_trmm"
]

target_list_omp=[
    "./m_omp_trmm2",
    "./m_omp_trmm6",
    "./m_omp_trmm7"
]

target_list_mpi=[
    "./m_mpi_trmm_opt2"
]

from datetime import datetime
ct = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')

create_dir = """
mkdir -p measurements &&
mkdir measurements/""" +ct+""" > /dev/null 2>&1;
"""
print("#!/bin/bash")
print("module load openmpi/4.1.4")
print("module load gcc/9.3.0")
print("module load gsl")
print("module load openblas")
print("make clean && make measure")
print(create_dir)

for p in (target_list_seq + target_list_omp + target_list_mpi):
    print("mkdir measurements/"+ct+"/"+p[2:])
print("")

for p in target_list_seq:
    out = "measurements/{}/{}/nt_{}.txt"
    err = "measurements/{}/{}/nt_{}.err"
    wrap_cmd ="{} 3456 3456 1.5"
    sbatch_cmd = "sbatch --error=\"{}\" --output=\"{}\" --ntasks=1 --wrap=\"{}\" --time=04:00:00"
    print(sbatch_cmd.format(err.format(ct, p[2:], 1), out.format(ct, p[2:], 1), wrap_cmd.format(p)))
print("")

for p in target_list_omp:
    for n_threads in NTHREADS_list:
        out = "measurements/{}/{}/nt_{}.txt"
        err = "measurements/{}/{}/nt_{}.err"
        wrap_cmd ="{} 3456 3456 1.5"
        sbatch_cmd = "sbatch --error=\"{}\" --output=\"{}\" --ntasks=1 --cpus-per-task={} --wrap=\"export OMP_NUM_THREADS={} && {}\" --time=04:00:00"
        print(sbatch_cmd.format(err.format(ct, p[2:], n_threads), out.format(ct, p[2:], n_threads), n_threads, n_threads, wrap_cmd.format(p)))
print("")

for p in target_list_mpi:
    for n_threads in NTHREADS_list:
        out = "measurements/{}/{}/nt_{}.txt"
        err = "measurements/{}/{}/nt_{}.err"
        wrap_cmd ="mpirun {} 3456 3456 1.5"
        # Nodes set to 1 for now (shared-memory system)
        sbatch_cmd = "sbatch --error=\"{}\" --output=\"{}\" --ntasks={} --nodes=1 --wrap=\"{}\" --time=04:00:00"
        print(sbatch_cmd.format(err.format(ct, p[2:], n_threads), out.format(ct, p, n_threads), n_threads, wrap_cmd.format(p)))
