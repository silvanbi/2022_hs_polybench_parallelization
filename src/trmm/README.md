# trmm

This source sub-folder contains work on optimization of trmm (triangular matrix-matrix multiplication) algorithm. Specifically, usage of parallelization techniques to optimize runtime of the algorithm with various matrix sizes and shapes.

## Overview
Each optimization variant is a standalone program, which can be independently compiled and run. Additionally, test method is linked to each program to enable checking correctness of the computation (see Test section below). Common functions such as initialization of matrices, argument parsing and matrix printing are present in the utils folder.

## Build
Run `make measure` to generate programs for measurement purposes.
To build for non-measurement purposes, use `make build`
To build on euler, use `make measure-euler` or `make measure-mkl-euler`

## Measure
Use `measure_local.sh` and `measure_local_weak.sh` to perform measurements locally and use `generate_submit_strong.py` and `generate_submit_weak.py` to generate scripts to run on euler.

## Run
Build for non-measurement purposes
- `./program <m> <n> <alpha>` Note that n is required to be a multiple of 8.

## Test
Please make sure that `-D NRUNS=1` or `NRUNS` is not set when compiling as tests are only correct when executing a single iteration
`./program -t <m> <n> <alpha>`

## Best Performing version
The best performing MPI version is mpi/mpi_trmm_opt3.c
For omp optimizations, the best performing version is openmp/omp_trmm_13.c. In the report, it has been renamed OPT2. openmp/omp_trmm_12.c and openmp/omp_trmm_14.c are similar but with unrolling factors of 2 and 8 respectively.
