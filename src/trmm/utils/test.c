#include <stdio.h>
#include <stdlib.h>
#include "test.h"
#include "common.h"
#include "../original/trmm.h"

void assert_matrices(int m, int n, double* A, double* B) {
  double eps = 1.2e-07;
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      // Note: precision problems may arise - consider adding epsilon
      if (A[i*n+j] + eps > B[i*n+j] && A[i*n+j] - eps < B[i*n+j])
        continue;
      printf("Assert FAIL - expected: %0.2lf, was: %0.2lf (at: [%d, %d])\n", A[i*n+j], B[i*n+j], i, j);
      printf("Expected matrix: \n");
      print_array(m, n, A);
      printf("\nWas matrix: \n");
      print_array(m, n, B);
      exit(1);
    }
  }
}

void run_test(int m, int n, double alpha, double* A, double* tst_B) {
  printf("TEST: ");
  double* exp_B = malloc(m * n * sizeof(double));
  init_array_b(m, n, exp_B);
  kernel_trmm(m, n, alpha, A, exp_B);
  assert_matrices(m, n, exp_B, tst_B);
  free(exp_B);
  printf("Passed\n");
}
