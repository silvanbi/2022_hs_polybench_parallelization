#pragma once

void init_data(int m, int n, double* A, double* B);
void init_array_a(int m, double* A);
void init_array_b(int m, int n, double* B);
void print_array(int m, int n, double* X);
void print_array_c(int m, int n, double* X);
void print_times(double* times, int nruns, int rank);
void print_cpu_info(int rank);

void parse_args(int argc, char** argv, int* m, int* n, double* alpha, int* run_tests);
