#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

void init_data(int m, int n, double* A, double* B) {
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < i; j++)
      A[i*m+j] = (double)((i+j) % m)/m;

    A[i*m+i] = 1.0;

    for (int j = 0; j < n; j++)
      B[i*n+j] = (double)((n+(i-j)) % n)/n;
  }
}

void init_array_a(int m, double* A) {
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < i; j++)
      A[i*m+j] = (double)((i+j) % m)/m;
    A[i*m+i] = 1.0;
  }
}

void init_array_b(int m, int n, double* B) {
  for (int i = 0; i < m; i++)
    for (int j = 0; j < n; j++)
      B[i*n+j] = (double)((n+(i-j)) % n)/n;
}

void print_array(int m, int n, double* X) {
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++)
      printf("%0.2lf ", X[i * n + j]);
    printf("\n");
  }
  printf("\n");
}

void print_array_c(int m, int n, double* X) {
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++)
      printf("%0.2lf ", X[j * m + i]);
    printf("\n");
  }
  printf("\n");
}

void print_times(double* times, int nruns, int rank) {
  printf("Process %d: ", rank);
  for (int i = 0; i < nruns; i++) {
    if (i < nruns-1)
      printf("%lf, ", times[i]);
    else
      printf("%lf", times[i]);
  }
  printf("\n");
}

void print_cpu_info(int rank) {
  FILE *fp;
  char output[32768];

  fp = popen("lscpu | grep -e 'Model\\|L1d\\|L2\\|L3\\|L4'", "r");
  if (fp == NULL)
    fprintf(stderr, "Failed to run command 'lscpu'\n" );

  fprintf(stderr, "Process %d:\n", rank);
  while (fgets(output, sizeof(output), fp) != NULL)
    fprintf(stderr, "%s", output);

  pclose(fp);
}

void parse_args(int argc, char** argv, int* m, int* n, double* alpha, int* run_tests) {
  if (argc == 5 && strcmp(argv[1], "-t") == 0) {
    *run_tests = 1;
    *m = atoi(argv[2]);
    *n = atoi(argv[3]);
    *alpha = atof(argv[4]);
    if (*m == 0) {
      printf("Invalid 'm' dimension supplied (must be an integer number)\n");
      exit(1);
    }
    if (*n == 0) {
      printf("Invalid 'n' dimension supplied (must be an integer number)\n");
      exit(1);
    }
    if (*alpha == 0) {
      printf("Invalid 'alpha' scaling factor supplied (must be a floating point number)\n");
      exit(1);
    }
  }
  else if (argc == 4) {
    *m = atoi(argv[1]);
    *n = atoi(argv[2]);
    *alpha = atof(argv[3]);
    if (*m == 0) {
      printf("Invalid 'm' dimension supplied (must be an integer number)\n");
      exit(1);
    }
    if (*n == 0) {
      printf("Invalid 'n' dimension supplied (must be an integer number)\n");
      exit(1);
    }
    if (*alpha == 0) {
      printf("Invalid 'alpha' scaling factor supplied (must be a floating point number)\n");
      exit(1);
    }
  }
  else {
    printf("Invalid arguments - correct usage:\n");
    printf("  ./program -t <m> <n> <alpha>    [runs benchmark and test]\n");
    printf("  ./program <m> <n> <alpha>       [runs benchmark]\n");
    exit(1);
  }
}
