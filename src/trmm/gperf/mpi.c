#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "mpi_trmm_opt3"

typedef struct {
  int m;
  int n;
  double alpha;
} params_s;

int main(int argc, char** argv) {
  double start;
  int rank, size, m, n;
  int test = 0;
  double alpha;
  double* A;
  double* B;
  params_s params;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // sudo gdb --pid=xxxx
  // use up to get into main function frame
  // set var gd = 7
  // UNCOMMENT TO USE GDB

  // volatile int gd = 0;
  // printf("PID %d ready for attach\n", getpid());
  // fflush(stdout);
  // while (0 == gd)
  //   sleep(5);

  // Broadcast parameters
  // - parse on node 0 and broadcast to all other nodes
  // - safe packed way => see common notes for further details
  MPI_Aint p_lb, p_m, p_alpha, p_ub;
  MPI_Get_address(&params, &p_lb);
  MPI_Get_address(&params.m, &p_m);
  MPI_Get_address(&params.alpha, &p_alpha);
  MPI_Get_address(&params+1, &p_ub);

  int p_blocks[] = {0, 2, 1, 0};
  MPI_Datatype p_types[] = {MPI_LB, MPI_INT, MPI_DOUBLE, MPI_UB};
  MPI_Aint p_offsets[] = {0, p_m-p_lb, p_alpha-p_lb, p_ub-p_lb};
  MPI_Datatype params_t;
  MPI_Type_create_struct(4, p_blocks, p_offsets, p_types, &params_t);
  MPI_Type_commit(&params_t);

  // Prepare global matrix A and B on rank 0
  if (rank == 0) {
    parse_args(argc, argv, &params.m, &params.n, &params.alpha, &test);
    m = params.m;
    n = params.n;
    alpha = params.alpha;

    if (n != m) {
      printf("This version only works for square matrices (m=n)\n");
      exit(1);
    }

    A = aligned_alloc(64, m * n * sizeof(double));
    B = aligned_alloc(64, m * n * sizeof(double));
    init_data(m, n, A, B);
  }

  MPI_Bcast(&params, 1, params_t, 0, MPI_COMM_WORLD);

  m = params.m;
  n = params.n;
  alpha = params.alpha;

  // Prepare ideal block sizes (e.g., m=n=21 across 4 processes => [5, 5, 5, 6])
  // Assume m=n (checked above)
  int lines = n/8;
  int *block_sizes = aligned_alloc(64, size * sizeof(int));
  int block_size_s = lines / size;
  int block_size_l = block_size_s + 1;

  int no_blocks_l = lines - block_size_s * size;
  int no_blocks_s = size - no_blocks_l;
  int no_blocks_total = no_blocks_s + no_blocks_l;

  block_size_l *= 8;
  block_size_s *= 8;

  for (int i = 0; i < no_blocks_s; i++)
    block_sizes[i] = block_size_s;
  for (int i = no_blocks_s; i < size; i++)
    block_sizes[i] = block_size_l;

  // Block size of this rank
  int rank_block_size = block_sizes[rank];

  // Prepare MPI data types for both small and large blocks
  MPI_Datatype tmp_block_s_t, block_s_t;
  MPI_Type_vector(m, block_size_s, n, MPI_DOUBLE, &tmp_block_s_t);
  MPI_Type_create_resized(tmp_block_s_t, 0, block_size_s * sizeof(double), &block_s_t);
  MPI_Type_commit(&block_s_t);

  MPI_Datatype tmp_block_l_t, block_l_t;
  MPI_Type_vector(m, block_size_l, n, MPI_DOUBLE, &tmp_block_l_t);
  MPI_Type_create_resized(tmp_block_l_t, 0, block_size_l * sizeof(double), &block_l_t);
  MPI_Type_commit(&block_l_t);

  // Scatter matrix B across processes (due to non-equal blocks - scatter cannot be used)
  double *B_block = B;
  if (rank != 0)
    B_block = aligned_alloc(64, m * rank_block_size * sizeof(double));
  double *B_comm_pointer = B;
  if (rank == 0) {
    if (no_blocks_s == 0) {
      B_comm_pointer += block_size_l;
      for (int i = 1; i < size; i++) {
        MPI_Send(B_comm_pointer, 1, block_l_t, i, 0, MPI_COMM_WORLD);
        B_comm_pointer += block_size_l;
      }
    }
    else {
      B_comm_pointer += block_size_s;
      for (int i = 1; i < no_blocks_s; i++) {
        MPI_Send(B_comm_pointer, 1, block_s_t, i, 0, MPI_COMM_WORLD);
        B_comm_pointer += block_size_s;
      }
      for (int i = no_blocks_s; i < size; i++) {
        MPI_Send(B_comm_pointer, 1, block_l_t, i, 0, MPI_COMM_WORLD);
        B_comm_pointer += block_size_l;
      }
    }
  }
  else {
    MPI_Recv(B_block, m * rank_block_size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }


  // Compute kernel for each block of A
  double *A_block = A;
  if (rank != 0)
    A_block = aligned_alloc(64, m * block_size_l * sizeof(double));
  double *A_comm_pointer = A;
  int i_global = 0;
  for (int block_index = 0; block_index < no_blocks_total; block_index++) {
    // Distributed block of A across all processes (due to non-equal blocks - broadcast cannot be used)
    int A_block_size = block_sizes[block_index];
    if (rank == 0) {
      if (A_block_size == block_size_s) {
        for (int i = 1; i < size; i++)
          MPI_Send(A_comm_pointer, 1, block_s_t, i, 0, MPI_COMM_WORLD);
        A_block = A_comm_pointer;
        A_comm_pointer += block_size_s;
      }
      else {
        for (int i = 1; i < size; i++)
          MPI_Send(A_comm_pointer, 1, block_l_t, i, 0, MPI_COMM_WORLD);
        A_block = A_comm_pointer;
        A_comm_pointer += block_size_l;
      }
    }
    else {
      MPI_Recv(A_block, m * A_block_size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    // Compute kernel - i_global needed to keep track of the i offset across blocks of A
    if (rank == 0) {

      for (int i = 0; i < A_block_size; i+= 4) {
        int k = i_global+i+1;
        for (int j = 0; j < rank_block_size; j++) {

          B_block[(i_global+i+0)*n+j] = B_block[(i_global+i+0)*n+j] + A_block[(k+0)*m+(i+0)] * B_block[(k+0)*n+j];

          B_block[(i_global+i+0)*n+j] = B_block[(i_global+i+0)*n+j] + A_block[(k+1)*m+(i+0)] * B_block[(k+1)*n+j];
          B_block[(i_global+i+1)*n+j] = B_block[(i_global+i+1)*n+j] + A_block[(k+1)*m+(i+1)] * B_block[(k+1)*n+j];

          B_block[(i_global+i+0)*n+j] = B_block[(i_global+i+0)*n+j] + A_block[(k+2)*m+(i+0)] * B_block[(k+2)*n+j];
          B_block[(i_global+i+1)*n+j] = B_block[(i_global+i+1)*n+j] + A_block[(k+2)*m+(i+1)] * B_block[(k+2)*n+j];
          B_block[(i_global+i+2)*n+j] = B_block[(i_global+i+2)*n+j] + A_block[(k+2)*m+(i+2)] * B_block[(k+2)*n+j];

        }
        for (k = i_global+i+4; k < m; k+=4) {
          for (int j = 0; j < rank_block_size; j++) {
            B_block[(i_global+i+0)*n+j] = B_block[(i_global+i+0)*n+j] + (A_block[(k+0)*m+(i+0)] * B_block[(k+0)*n+j])
              + (A_block[(k+1)*m+(i+0)] * B_block[(k+1)*n+j])
              + (A_block[(k+2)*m+(i+0)] * B_block[(k+2)*n+j])
              + (A_block[(k+3)*m+(i+0)] * B_block[(k+3)*n+j]);

            B_block[(i_global+i+1)*n+j] = B_block[(i_global+i+1)*n+j] + (A_block[(k+0)*m+(i+1)] * B_block[(k+0)*n+j])
              + (A_block[(k+1)*m+(i+1)] * B_block[(k+1)*n+j])
              + (A_block[(k+2)*m+(i+1)] * B_block[(k+2)*n+j])
              + (A_block[(k+3)*m+(i+1)] * B_block[(k+3)*n+j]);

            B_block[(i_global+i+2)*n+j] = B_block[(i_global+i+2)*n+j] + (A_block[(k+0)*m+(i+2)] * B_block[(k+0)*n+j])
              + (A_block[(k+1)*m+(i+2)] * B_block[(k+1)*n+j])
              + (A_block[(k+2)*m+(i+2)] * B_block[(k+2)*n+j])
              + (A_block[(k+3)*m+(i+2)] * B_block[(k+3)*n+j]);

            B_block[(i_global+i+3)*n+j] = B_block[(i_global+i+3)*n+j] + (A_block[(k+0)*m+(i+3)] * B_block[(k+0)*n+j])
              + (A_block[(k+1)*m+(i+3)] * B_block[(k+1)*n+j])
              + (A_block[(k+2)*m+(i+3)] * B_block[(k+2)*n+j])
              + (A_block[(k+3)*m+(i+3)] * B_block[(k+3)*n+j]);

          }
        }
        for (int j = rank_block_size-1; j >= 0; j--) {
          B_block[(i_global+i+0)*n+j] = alpha * B_block[(i_global+i+0)*n+j];
          B_block[(i_global+i+1)*n+j] = alpha * B_block[(i_global+i+1)*n+j];
          B_block[(i_global+i+2)*n+j] = alpha * B_block[(i_global+i+2)*n+j];
          B_block[(i_global+i+3)*n+j] = alpha * B_block[(i_global+i+3)*n+j];
        }
      }
    }
    else {
      for (int i = 0; i < A_block_size; i+= 4) {
        int k = i_global+i+1;
        for (int j = 0; j < rank_block_size; j++) {

          B_block[(i_global+i+0)*rank_block_size+j] = B_block[(i_global+i+0)*rank_block_size+j] + A_block[(k+0)*A_block_size+(i+0)] * B_block[(k+0)*rank_block_size+j];

          B_block[(i_global+i+0)*rank_block_size+j] = B_block[(i_global+i+0)*rank_block_size+j] + A_block[(k+1)*A_block_size+(i+0)] * B_block[(k+1)*rank_block_size+j];
          B_block[(i_global+i+1)*rank_block_size+j] = B_block[(i_global+i+1)*rank_block_size+j] + A_block[(k+1)*A_block_size+(i+1)] * B_block[(k+1)*rank_block_size+j];

          B_block[(i_global+i+0)*rank_block_size+j] = B_block[(i_global+i+0)*rank_block_size+j] + A_block[(k+2)*A_block_size+(i+0)] * B_block[(k+2)*rank_block_size+j];
          B_block[(i_global+i+1)*rank_block_size+j] = B_block[(i_global+i+1)*rank_block_size+j] + A_block[(k+2)*A_block_size+(i+1)] * B_block[(k+2)*rank_block_size+j];
          B_block[(i_global+i+2)*rank_block_size+j] = B_block[(i_global+i+2)*rank_block_size+j] + A_block[(k+2)*A_block_size+(i+2)] * B_block[(k+2)*rank_block_size+j];
        }
        for (k = i_global+i+4; k < m; k+=4) {
          for (int j = 0; j < rank_block_size; j++) {
            B_block[(i_global+i+0)*rank_block_size+j] = B_block[(i_global+i+0)*rank_block_size+j] 
              + (A_block[(k+0)*A_block_size+(i+0)] * B_block[(k+0)*rank_block_size+j])
              + (A_block[(k+1)*A_block_size+(i+0)] * B_block[(k+1)*rank_block_size+j])
              + (A_block[(k+2)*A_block_size+(i+0)] * B_block[(k+2)*rank_block_size+j])
              + (A_block[(k+3)*A_block_size+(i+0)] * B_block[(k+3)*rank_block_size+j]);

            B_block[(i_global+i+1)*rank_block_size+j] = B_block[(i_global+i+1)*rank_block_size+j] 
              + (A_block[(k+0)*A_block_size+(i+1)] * B_block[(k+0)*rank_block_size+j])
              + (A_block[(k+1)*A_block_size+(i+1)] * B_block[(k+1)*rank_block_size+j])
              + (A_block[(k+2)*A_block_size+(i+1)] * B_block[(k+2)*rank_block_size+j])
              + (A_block[(k+3)*A_block_size+(i+1)] * B_block[(k+3)*rank_block_size+j]);

            B_block[(i_global+i+2)*rank_block_size+j] = B_block[(i_global+i+2)*rank_block_size+j] 
              + (A_block[(k+0)*A_block_size+(i+2)] * B_block[(k+0)*rank_block_size+j])
              + (A_block[(k+1)*A_block_size+(i+2)] * B_block[(k+1)*rank_block_size+j])
              + (A_block[(k+2)*A_block_size+(i+2)] * B_block[(k+2)*rank_block_size+j])
              + (A_block[(k+3)*A_block_size+(i+2)] * B_block[(k+3)*rank_block_size+j]);

            B_block[(i_global+i+3)*rank_block_size+j] = B_block[(i_global+i+3)*rank_block_size+j] 
              + (A_block[(k+0)*A_block_size+(i+3)] * B_block[(k+0)*rank_block_size+j])
              + (A_block[(k+1)*A_block_size+(i+3)] * B_block[(k+1)*rank_block_size+j])
              + (A_block[(k+2)*A_block_size+(i+3)] * B_block[(k+2)*rank_block_size+j])
              + (A_block[(k+3)*A_block_size+(i+3)] * B_block[(k+3)*rank_block_size+j]);
          }
        }
        for (int j = rank_block_size-1; j >= 0; j--) {
          B_block[(i_global+i+0)*rank_block_size+j] = alpha * B_block[(i_global+i+0)*rank_block_size+j];
          B_block[(i_global+i+1)*rank_block_size+j] = alpha * B_block[(i_global+i+1)*rank_block_size+j];
          B_block[(i_global+i+2)*rank_block_size+j] = alpha * B_block[(i_global+i+2)*rank_block_size+j];
          B_block[(i_global+i+3)*rank_block_size+j] = alpha * B_block[(i_global+i+3)*rank_block_size+j];
        }
      }
    }
    i_global += A_block_size;
  }

  // Gather matrix B from all processes (due to non-equal blocks - gather cannot be used)
  if (rank != 0)
    MPI_Send(B_block, m * rank_block_size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);

  if (rank == 0) {
    B_comm_pointer = B;
    if (no_blocks_s == 0) {
      B_comm_pointer += block_size_l;
      for (int i = 1; i < size; i++) {
        MPI_Recv(B_comm_pointer, 1, block_l_t, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        B_comm_pointer += block_size_l;
      }
    }
    else {
      B_comm_pointer += block_size_s;
      for (int i = 1; i < no_blocks_s; i++) {
        MPI_Recv(B_comm_pointer, 1, block_s_t, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        B_comm_pointer += block_size_s;
      }
      for (int i = no_blocks_s; i < size; i++) {
        MPI_Recv(B_comm_pointer, 1, block_l_t, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        B_comm_pointer += block_size_l;
      }
    }
  }

  MPI_Type_free(&block_s_t);
  MPI_Type_free(&block_l_t);

  if (rank != 0) {
    free(A_block);
    free(B_block);
  }

  MPI_Type_free(&params_t);

//  print_cpu_info(rank);
  MPI_Finalize();

  return 0;
}