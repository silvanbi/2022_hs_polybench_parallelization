#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_trmm_opt14"

/**
 * Expecting n % 8 = 0
*/
int main(int argc, char** argv) {
  int test = 0;
  int m, n;
  double alpha;
  parse_args(argc, argv, &m, &n, &alpha, &test);

  double* A = aligned_alloc(64, m * m * sizeof(double));
  double* B = aligned_alloc(64, m * n * sizeof(double));
  init_data(m, n, A, B);

  for (int i = 0; i < m; i+=8) {
    for (int j = 0; j < n; j++) {
      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+1)*m+(i+0)] * B[(i+1)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+2)*m+(i+0)] * B[(i+2)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+2)*m+(i+1)] * B[(i+2)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+3)*m+(i+0)] * B[(i+3)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+3)*m+(i+1)] * B[(i+3)*n+j];
      B[(i+2)*n+j] = B[(i+2)*n+j] + A[(i+3)*m+(i+2)] * B[(i+3)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+4)*m+(i+0)] * B[(i+4)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+4)*m+(i+1)] * B[(i+4)*n+j];
      B[(i+2)*n+j] = B[(i+2)*n+j] + A[(i+4)*m+(i+2)] * B[(i+4)*n+j];
      B[(i+3)*n+j] = B[(i+3)*n+j] + A[(i+4)*m+(i+3)] * B[(i+4)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+5)*m+(i+0)] * B[(i+5)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+5)*m+(i+1)] * B[(i+5)*n+j];
      B[(i+2)*n+j] = B[(i+2)*n+j] + A[(i+5)*m+(i+2)] * B[(i+5)*n+j];
      B[(i+3)*n+j] = B[(i+3)*n+j] + A[(i+5)*m+(i+3)] * B[(i+5)*n+j];
      B[(i+4)*n+j] = B[(i+4)*n+j] + A[(i+5)*m+(i+4)] * B[(i+5)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+6)*m+(i+0)] * B[(i+6)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+6)*m+(i+1)] * B[(i+6)*n+j];
      B[(i+2)*n+j] = B[(i+2)*n+j] + A[(i+6)*m+(i+2)] * B[(i+6)*n+j];
      B[(i+3)*n+j] = B[(i+3)*n+j] + A[(i+6)*m+(i+3)] * B[(i+6)*n+j];
      B[(i+4)*n+j] = B[(i+4)*n+j] + A[(i+6)*m+(i+4)] * B[(i+6)*n+j];
      B[(i+5)*n+j] = B[(i+5)*n+j] + A[(i+6)*m+(i+5)] * B[(i+6)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+7)*m+(i+0)] * B[(i+7)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+7)*m+(i+1)] * B[(i+7)*n+j];
      B[(i+2)*n+j] = B[(i+2)*n+j] + A[(i+7)*m+(i+2)] * B[(i+7)*n+j];
      B[(i+3)*n+j] = B[(i+3)*n+j] + A[(i+7)*m+(i+3)] * B[(i+7)*n+j];
      B[(i+4)*n+j] = B[(i+4)*n+j] + A[(i+7)*m+(i+4)] * B[(i+7)*n+j];
      B[(i+5)*n+j] = B[(i+5)*n+j] + A[(i+7)*m+(i+5)] * B[(i+7)*n+j];
      B[(i+6)*n+j] = B[(i+6)*n+j] + A[(i+7)*m+(i+6)] * B[(i+7)*n+j];

    }
    for (int k = i+8; k < m; k+=8) {
      for (int j = 0; j < n; j++) {
        B[(i+0)*n+j] = B[(i+0)*n+j] + (A[(k+0)*m+(i+0)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+0)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+0)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+0)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+0)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+0)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+0)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+0)] * B[(k+7)*n+j]);

        B[(i+1)*n+j] = B[(i+1)*n+j] + (A[(k+0)*m+(i+1)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+1)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+1)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+1)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+1)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+1)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+1)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+1)] * B[(k+7)*n+j]);

        B[(i+2)*n+j] = B[(i+2)*n+j] + (A[(k+0)*m+(i+2)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+2)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+2)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+2)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+2)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+2)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+2)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+2)] * B[(k+7)*n+j]);

        B[(i+3)*n+j] = B[(i+3)*n+j] + (A[(k+0)*m+(i+3)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+3)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+3)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+3)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+3)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+3)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+3)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+3)] * B[(k+7)*n+j]);

        B[(i+4)*n+j] = B[(i+4)*n+j] + (A[(k+0)*m+(i+4)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+4)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+4)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+4)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+4)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+4)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+4)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+4)] * B[(k+7)*n+j]);

        B[(i+5)*n+j] = B[(i+5)*n+j] + (A[(k+0)*m+(i+5)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+5)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+5)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+5)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+5)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+5)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+5)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+5)] * B[(k+7)*n+j]);

        B[(i+6)*n+j] = B[(i+6)*n+j] + (A[(k+0)*m+(i+6)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+6)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+6)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+6)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+6)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+6)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+6)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+6)] * B[(k+7)*n+j]);

        B[(i+7)*n+j] = B[(i+7)*n+j] + (A[(k+0)*m+(i+7)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+7)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+7)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+7)] * B[(k+3)*n+j])
          + (A[(k+4)*m+(i+7)] * B[(k+4)*n+j])
          + (A[(k+5)*m+(i+7)] * B[(k+5)*n+j])
          + (A[(k+6)*m+(i+7)] * B[(k+6)*n+j])
          + (A[(k+7)*m+(i+7)] * B[(k+7)*n+j]);

      }
    }
    for (int j = n-1; j >= 0; j--) {
      B[(i+0)*n+j] = alpha * B[(i+0)*n+j];
      B[(i+1)*n+j] = alpha * B[(i+1)*n+j];
      B[(i+2)*n+j] = alpha * B[(i+2)*n+j];
      B[(i+3)*n+j] = alpha * B[(i+3)*n+j];
      B[(i+4)*n+j] = alpha * B[(i+4)*n+j];
      B[(i+5)*n+j] = alpha * B[(i+5)*n+j];
      B[(i+6)*n+j] = alpha * B[(i+6)*n+j];
      B[(i+7)*n+j] = alpha * B[(i+7)*n+j];
    }
  }

  free(A);
  free(B);
  return 0;
}