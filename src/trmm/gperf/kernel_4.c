#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_trmm_opt13"

/**
 * Expecting n % 8 = 0
*/
int main(int argc, char** argv) {
  int test = 0;
  int m, n;
  double alpha = 1.5;
  parse_args(argc, argv, &m, &n, &alpha, &test);

  double* A = aligned_alloc(64, m * m * sizeof(double));
  double* B = aligned_alloc(64, m * n * sizeof(double));

  for (int i = 0; i < m; i+= 4) {
    for (int j = 0; j < n; j++) {
      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+1)*m+(i+0)] * B[(i+1)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+2)*m+(i+0)] * B[(i+2)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+2)*m+(i+1)] * B[(i+2)*n+j];

      B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+3)*m+(i+0)] * B[(i+3)*n+j];
      B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+3)*m+(i+1)] * B[(i+3)*n+j];
      B[(i+2)*n+j] = B[(i+2)*n+j] + A[(i+3)*m+(i+2)] * B[(i+3)*n+j];

    }
    for (int k = i+4; k < m; k+=4) {
      for (int j = 0; j < n; j++) {
        B[(i+0)*n+j] = B[(i+0)*n+j] + (A[(k+0)*m+(i+0)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+0)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+0)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+0)] * B[(k+3)*n+j]);

        B[(i+1)*n+j] = B[(i+1)*n+j] + (A[(k+0)*m+(i+1)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+1)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+1)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+1)] * B[(k+3)*n+j]);

        B[(i+2)*n+j] = B[(i+2)*n+j] + (A[(k+0)*m+(i+2)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+2)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+2)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+2)] * B[(k+3)*n+j]);

        B[(i+3)*n+j] = B[(i+3)*n+j] + (A[(k+0)*m+(i+3)] * B[(k+0)*n+j])
          + (A[(k+1)*m+(i+3)] * B[(k+1)*n+j])
          + (A[(k+2)*m+(i+3)] * B[(k+2)*n+j])
          + (A[(k+3)*m+(i+3)] * B[(k+3)*n+j]);

      }
    }
    for (int j = n-1; j >= 0; j--) {
      B[(i+0)*n+j] = alpha * B[(i+0)*n+j];
      B[(i+1)*n+j] = alpha * B[(i+1)*n+j];
      B[(i+2)*n+j] = alpha * B[(i+2)*n+j];
      B[(i+3)*n+j] = alpha * B[(i+3)*n+j];
    }
  }

  free(A);
  free(B);
  return 0;
}