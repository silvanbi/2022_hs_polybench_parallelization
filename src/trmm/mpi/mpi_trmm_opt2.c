#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "mpi_trmm_opt2"

int main(int argc, char** argv) {
  double start;
  double times[NRUNS];
  int rank, size, m, n;
  int test = 0;
  double alpha;
  double* A;
  double* B;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if (rank == 0) {
    parse_args(argc, argv, &m, &n, &alpha, &test);
    A = aligned_alloc(64, m * m * sizeof(double));
    B = aligned_alloc(64, m * n * sizeof(double));
    init_data(m, n, A, B);
  }

  for (int run = 0; run < NRUNS; run++) {
    start = MPI_Wtime();

    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&alpha, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (rank > 0)
      A = aligned_alloc(64, m * m * sizeof(double));

    MPI_Bcast(A, m * m, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // TODO: Convert to generalized approach (assume perfect division for now)
    int n_block = n / size;

    double *B_block = aligned_alloc(64, m * n_block * sizeof(double));

    MPI_Datatype tmp_block_t, block_t;
    MPI_Type_vector(m, n_block, n, MPI_DOUBLE, &tmp_block_t);
    MPI_Type_create_resized(tmp_block_t, 0, n_block * sizeof(double), &block_t);
    MPI_Type_commit(&block_t);

    MPI_Scatter(B, 1, block_t, B_block, m * n_block, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n_block; j++) {
        for (int k = i + 1; k < m; k++)
          B_block[i * n_block + j] = B_block[i * n_block + j] + (A[k * m + i] * B_block[k * n_block + j]);
        B_block[i * n_block + j] = alpha * B_block[i * n_block + j];
      }
    }

    MPI_Gather(B_block, m * n_block, MPI_DOUBLE, B, 1, block_t, 0, MPI_COMM_WORLD);
    MPI_Type_free(&block_t);

    if (rank > 0)
      free(A);
    free(B_block);

    times[run] = MPI_Wtime() - start;
  }

  if (rank != 0)
    MPI_Send(times, NRUNS, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);

  if (rank == 0) {
    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, rank);
    for (int source_rank = 1; source_rank < size; source_rank++) {
      MPI_Recv(times, NRUNS, MPI_DOUBLE, source_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      print_times(times, NRUNS, source_rank);
    }

    if (test && NRUNS > 1)
      printf("Test cannot be run for NRUNS > 1\n");
    else if (test)
      run_test(m, n, alpha, A, B);
    free(A);
    free(B);
  }

  print_cpu_info(rank);
  MPI_Finalize();
}