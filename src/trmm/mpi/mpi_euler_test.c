#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "mpi_euler_test"

int main(int argc, char** argv) {
  double start;
  double times[NRUNS];
  int size, rank;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  for (int i = 0; i < NRUNS; i++) {
    start = MPI_Wtime();
    sleep(2);
    times[i] = MPI_Wtime() - start;
  }

  printf("%s\n", PGM_NAME);
  print_times(times, NRUNS, 0);

  MPI_Finalize();
}