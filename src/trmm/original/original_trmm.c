#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include "trmm.h"
#include "../utils/common.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "original_trmm"

int main(int argc, char** argv) {
  struct timeval start;
  struct timeval end;
  double times[NRUNS];
  int test = 0;
  int m, n;
  double alpha;
  parse_args(argc, argv, &m, &n, &alpha, &test);

  double* A = aligned_alloc(64, m * m * sizeof(double));
  double* B = aligned_alloc(64, m * n * sizeof(double));
  init_data(m, n, A, B);

  for (int run = 0; run < NRUNS; run++) {
    gettimeofday(&start, NULL);
    // Inlined from trmm.c to eliminate method call overhead
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        for (int k = i + 1; k < m; k++)
          B[i*n+j] = B[i*n+j] + (A[k*m+i] * B[k*n+j]);
        B[i*n+j] = alpha * B[i*n+j];
      }
    }
    gettimeofday(&end, NULL);
    times[run] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
  }

  if (test)
    printf("No test to be run for baseline version\n");

  printf("%s\n", PGM_NAME);
  print_times(times, NRUNS, 0);
  print_cpu_info(0);

  free(A);
  free(B);
  return 0;
}