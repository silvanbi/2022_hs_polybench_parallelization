"""
USE AS FOLLOWS:

"""

import math
from datetime import datetime

NTHREADS_list = [1, 2, 4, 8, 16, 32, 64, 128]
sizes = [3680, 4640, 5848, 7368, 9288, 11704, 14752, 18600]

# programs to benchmark
target_list_seq=[
    "./m_original_trmm"
]

target_list_omp=[
    "./m_omp_trmm12",
    "./m_omp_trmm13",
    "./m_omp_trmm14"
]

target_list_mpi=[
    "./m_mpi_trmm_opt3"
]

target_list_mkl_blas=[
    "m_mkl_blas_trmm"
]

target_list_mkl_blas_omp=[
    "./m_mkl_blas_p_trmm"
]

target_list_mkl_pblas=[
    "./m_pblas_mkl_cm_trmm_weak",
    "./m_pblas_mkl_rm_trmm_weak"
]

ct = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')

print("#!/bin/bash")
print(f"mkdir -p measurements && mkdir measurements/{ct} > /dev/null 2>&1&&")
print("module load openmpi/4.1.4 &&")
print("module load gcc/9.3.0 &&")

# Sequential
for p in target_list_seq:
    print(f"mkdir measurements/{ct}/{p[2:]}")
print("")

for p in target_list_seq:
    size = sizes[0]
    pg_name = p[2:]
    mem_per_cpu = math.ceil((size**2)*8/1e6)*3
    err = f"measurements/{ct}/{pg_name}/{size}_{size}_{1}.err"
    out = f"measurements/{ct}/{pg_name}/{size}_{size}_{1}.txt"
    wrap_cmd = f"{p} {size} {size} 1.5"
    sbatch_cmd = f"sbatch --job-name={pg_name} --error={err} --output={out} --ntasks=1 --constraint=\"EPYC_7763\" --cpus-per-task=1 --wrap=\"{wrap_cmd}\""
    print(sbatch_cmd)
print("")

# OMP
for p in target_list_omp:
    print(f"mkdir measurements/{ct}/{p[2:]}")
print("")

for p in target_list_omp:
    for i in range(len(sizes)):
        size = sizes[i]
        n_threads = NTHREADS_list[i]
        pg_name = p[2:]
        mem_per_cpu = math.ceil((size**2)*8/1e6)*3
        err = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.err"
        out = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.txt"
        bash_cmd = "bash -c \"export OMP_NUM_THREADS={} && {}\""
        wrap_cmd = f"{p} {size} {size} 1.5"
        sbatch_cmd = f"sbatch --job-name={pg_name} --error={err} --output={out} --ntasks=1 --constraint=\\\"EPYC_7763\\\" --cpus-per-task={n_threads} --wrap=\\\"{wrap_cmd}\\\""
        print(bash_cmd.format(n_threads, sbatch_cmd))
print("")

# MPI
for p in target_list_mpi:
    print(f"mkdir measurements/{ct}/{p[2:]}")
print("")

for p in target_list_mpi:
    for i in range(len(sizes)):
        size = sizes[i]
        n_threads = NTHREADS_list[i]
        pg_name = p[2:]
        mem_per_cpu = math.ceil((size**2)*8/1e6)*5
        err = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.err"
        out = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.txt"
        wrap_cmd = f"{p} {size} {size} 1.5"
        sbatch_cmd = f"sbatch --job-name={pg_name} --nodes=1 --error={err} --output={out} --ntasks={n_threads} --constraint=\"EPYC_7763\" --wrap=\"srun {wrap_cmd}\""
        print(sbatch_cmd)

print("\n\n")
print("module load intel &&")

# MKL BLAS
for p in target_list_mkl_blas:
    print(f"mkdir measurements/{ct}/{p[2:]}")
print("")

for p in target_list_mkl_blas:
    size = sizes[0]
    pg_name = p[2:]
    mem_per_cpu = math.ceil((size**2)*8/1e6)*3
    err = f"measurements/{ct}/{pg_name}/{size}_{size}_{1}.err"
    out = f"measurements/{ct}/{pg_name}/{size}_{size}_{1}.txt"
    wrap_cmd = f"{p} {size} {size} 1.5"
    sbatch_cmd = f"sbatch --job-name={pg_name} --error={err} --output={out} --ntasks=1 --constraint=\"EPYC_7763\" --cpus-per-task=1 --wrap=\"./{wrap_cmd}\""
    print(sbatch_cmd)
print("")

# MKL BLAS OMP
for p in target_list_mkl_blas_omp:
    print(f"mkdir measurements/{ct}/{p[2:]}")
print("")

for p in target_list_mkl_blas_omp:
    for i in range(len(sizes)):
        size = sizes[i]
        n_threads = NTHREADS_list[i]
        pg_name = p[2:]
        mem_per_cpu = math.ceil((size**2)*8/1e6)*3
        err = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.err"
        out = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.txt"
        bash_cmd = "bash -c \"export OMP_NUM_THREADS={} && {}\""
        wrap_cmd = f"{p} {size} {size} 1.5"
        sbatch_cmd = f"sbatch --job-name={pg_name} --error={err} --output={out} --ntasks=1 --constraint=\\\"EPYC_7763\\\" --cpus-per-task={n_threads} --wrap=\\\"{wrap_cmd}\\\""
        print(bash_cmd.format(n_threads, sbatch_cmd))
print("")

# MKL PBLAS
for p in target_list_mkl_pblas:
    print(f"mkdir measurements/{ct}/{p[2:]}")
print("")

for p in target_list_mkl_pblas:
    for i in range(len(sizes)):
        size = sizes[i]
        n_threads = NTHREADS_list[i]
        pg_name = p[2:]
        mem_per_cpu = math.ceil((size**2)*8/1e6)*5
        err = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.err"
        out = f"measurements/{ct}/{pg_name}/{size}_{size}_{n_threads}.txt"
        wrap_cmd = f"{p}_{n_threads} {size} {size} 1.5"
        sbatch_cmd = f"sbatch --job-name={pg_name} --nodes=1 --constraint=\"EPYC_7763\" --time=05:00:00 --error={err} --output={out} --ntasks={n_threads} --wrap=\"srun {wrap_cmd}\""
        print(sbatch_cmd)

