

NTHREADS_list=(1 2 4 8 16)
size=3456

target_list_seq=(
    "./m_original_trmm"
    "./m_blas_trmm"
)

target_list_omp=(
    "./m_omp_trmm2"
    "./m_omp_trmm6"
    "./m_omp_trmm7"
)

target_list_mpi=(
    "./m_mpi_trmm_opt2"
)

make clean && make measure && 

base_dir="measurements"
mkdir -p $base_dir &&
x=0
mkdir ${base_dir}/${x} > /dev/null 2>&1;
while [ $? -ne 0 ]
do
    x=$(( $x + 1 ))
    mkdir ${base_dir}/${x} > /dev/null 2>&1;
done

echo "result directory: ${base_dir}/${x}"

echo "running sequential measurements ..."
for target in ${target_list_seq[@]}; do
    mkdir ${base_dir}/${x}/${target}
    echo "measuring $target $size $size"
    $target $size $size 1.5 > ${base_dir}/${x}/${target}/${size}_${size}
done
echo "finished sequential measurements ..."

echo "running openMP measurements"
for target in ${target_list_omp[@]}; do
    mkdir ${base_dir}/${x}/${target}
    for N_THREADS in ${NTHREADS_list[@]}; do
        export OMP_NUM_THREADS=$N_THREADS &&
        echo "measuring $target $size $size" &&
        $target $size $size 1.5 > ${base_dir}/${x}/${target}/${size}_${size}_${N_THREADS}
    done
done
echo "finished openMP measurements ..."

echo "running MPI measurements\n"
for target in ${target_list_mpi[@]}; do
    mkdir ${base_dir}/${x}/${target}
    for N_THREADS in ${NTHREADS_list[@]}; do
        echo "measuring mpiexec -np $N_THREADS $target $size $size" &&
        mpiexec -np $N_THREADS $target $size $size 1.5 > ${base_dir}/${x}/${target}/${size}_${size}_${N_THREADS}
    done
done
echo "finished MPI measurements ..."