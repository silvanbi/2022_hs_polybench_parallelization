#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "../utils/common.h"
#include "../utils/test.h"

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_trmm_opt13"

/**
 * Expecting n % 8 = 0
*/
int main(int argc, char** argv) {
  double timing_start;
  double times[NRUNS];
  int test = 0;
  int m, n;
  double alpha;
  parse_args(argc, argv, &m, &n, &alpha, &test);

  double* A = aligned_alloc(64, m * m * sizeof(double));
  double* B = aligned_alloc(64, m * n * sizeof(double));
  init_data(m, n, A, B);

  for (int run = 0; run < NRUNS; run++) {
    timing_start = omp_get_wtime();
    #pragma omp parallel 
    {
      //Assuming more jobs than threads, n/8 > nthreads
      int id = omp_get_thread_num();
      int nth = omp_get_num_threads();
      int njobs = n /8;
      int start = ((double)njobs/nth)* id;
      int end = ((double)njobs/nth)* (id+1);
      //Multiplying by 8 in a searate statement for rounding to happen
      start *= 8;
      end *= 8;

      for (int i = 0; i < m; i+= 4) {
				for (int j = start; j < end; j++) {
					B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+1)*m+(i+0)] * B[(i+1)*n+j];

					B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+2)*m+(i+0)] * B[(i+2)*n+j];
					B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+2)*m+(i+1)] * B[(i+2)*n+j];

					B[(i+0)*n+j] = B[(i+0)*n+j] + A[(i+3)*m+(i+0)] * B[(i+3)*n+j];
					B[(i+1)*n+j] = B[(i+1)*n+j] + A[(i+3)*m+(i+1)] * B[(i+3)*n+j];
					B[(i+2)*n+j] = B[(i+2)*n+j] + A[(i+3)*m+(i+2)] * B[(i+3)*n+j];

				}
				for (int k = i+4; k < m; k+=4) {
					for (int j = start; j < end; j++) {
						B[(i+0)*n+j] = B[(i+0)*n+j] + (A[(k+0)*m+(i+0)] * B[(k+0)*n+j])
						  + (A[(k+1)*m+(i+0)] * B[(k+1)*n+j])
						  + (A[(k+2)*m+(i+0)] * B[(k+2)*n+j])
						  + (A[(k+3)*m+(i+0)] * B[(k+3)*n+j]);

						B[(i+1)*n+j] = B[(i+1)*n+j] + (A[(k+0)*m+(i+1)] * B[(k+0)*n+j])
						  + (A[(k+1)*m+(i+1)] * B[(k+1)*n+j])
						  + (A[(k+2)*m+(i+1)] * B[(k+2)*n+j])
						  + (A[(k+3)*m+(i+1)] * B[(k+3)*n+j]);

						B[(i+2)*n+j] = B[(i+2)*n+j] + (A[(k+0)*m+(i+2)] * B[(k+0)*n+j])
						  + (A[(k+1)*m+(i+2)] * B[(k+1)*n+j])
						  + (A[(k+2)*m+(i+2)] * B[(k+2)*n+j])
						  + (A[(k+3)*m+(i+2)] * B[(k+3)*n+j]);

						B[(i+3)*n+j] = B[(i+3)*n+j] + (A[(k+0)*m+(i+3)] * B[(k+0)*n+j])
						  + (A[(k+1)*m+(i+3)] * B[(k+1)*n+j])
						  + (A[(k+2)*m+(i+3)] * B[(k+2)*n+j])
						  + (A[(k+3)*m+(i+3)] * B[(k+3)*n+j]);

					}
				}
				for (int j = end-1; j >= start; j--) {
					B[(i+0)*n+j] = alpha * B[(i+0)*n+j];
					B[(i+1)*n+j] = alpha * B[(i+1)*n+j];
					B[(i+2)*n+j] = alpha * B[(i+2)*n+j];
					B[(i+3)*n+j] = alpha * B[(i+3)*n+j];
				}
			}
    }
    times[run] = omp_get_wtime() - timing_start;
  }

  if (test && NRUNS > 1)
    printf("Test cannot be run for NRUNS > 1\n");
  else if (test)
    run_test(m, n, alpha, A, B);

  printf("%s\n", PGM_NAME);
  print_times(times, NRUNS, 0);
  print_cpu_info(0);

  free(A);
  free(B);
  return 0;
}