

# with open('generated.txt', 'w') as f:
#     f.write('\t\t\tfor (int j = start; j < end; j+= 8) {\n')
#     f.write('\t\t\t\tfor (int i = 0; i < m; i+= 8) {\n')
#     indent = '\t\t\t\t\t'
#     f.write(indent+'//Triangular part\n')
#     for vk in range(1,8):
#         for vi in range(0,vk):
#             for vj in range(0,8):
#                 i = '(i+'+str(vi)+')'
#                 j = '(j+'+str(vj)+')'
#                 k = '(i+'+str(vk)+')'
#                 f.write(indent+'B['+i+'*n+'+j+'] = B['+i+'*n+'+j+'] + A['+k+'*m+'+i+'] * B['+k+'*n+'+j+'];\n')
#             f.write('\n')

#     f.write(indent+'//Retangular part\n')
#     f.write(indent+'for (int k = i+8; k < m; k++) {\n')
#     for vi in range(0, 8):
#         for vj in range(0,8):
#             i = '(i+'+str(vi)+')'
#             j = '(j+'+str(vj)+')'
#             f.write(indent+'\tB['+i+'*n+'+j+'] = B['+i+'*n+'+j+'] + A[k*m+'+i+'] * B[k*n+'+j+'];\n')
#         f.write('\n')
#     f.write(indent+'}\n')
#     for vi in range(0,8):
#         for vj in range(0,8):
#             i = '(i+'+str(vi)+')'
#             j = '(j+'+str(vj)+')'
#             f.write('\t\t\t\t\tB['+i+'*n+'+j+'] = B['+i+'*n+'+j+'] * alpha;\n')
#     f.write('\t\t\t\t}\n')
#     f.write('\t\t\t}\n')

with open('generated.txt', 'w') as f:
    step = 16
    s = str(step)
    t3 = '\t\t\t'
    t4 = '\t\t\t\t'
    t5 = '\t\t\t\t\t'
    t6 = '\t\t\t\t\t\t'
    f.write(t3+'for (int i = 0; i < m; i+='+s+') {\n')

    f.write(t4+'for (int j = start; j < end; j++) {\n')
    for i in range(1,step):
        for j in range(0, i):
            vi = '(i+'+str(j)+')'
            vk = '(i+'+str(i)+')'
            f.write(t5+'B['+vi+'*n+j] = B['+vi+'*n+j] + A['+vk+'*m+'+vi+'] * B['+vk+'*n+j];\n')
        f.write("\n")
    
    f.write(t4+'}\n')

    f.write(t4+'for (int k = i+'+s+'; k < m; k+='+s+') {\n')
    f.write(t5+'for (int j = start; j < end; j++) {\n')
    for i in range(0, step):
        for k in range(0, step):
            vi = '(i+'+str(i)+')'
            vk = '(k+'+str(k)+')'
            f.write(t6+'B['+vi+'*n+j] = B['+vi+'*n+j] + (A['+vk+'*m+'+vi+'] * B['+vk+'*n+j]);\n')
        f.write('\n')

    f.write(t5+'}\n')
    f.write(t4+'}\n')
    f.write(t4+'for (int j = end-1; j >= start; j--) {\n')
    for i in range(0, step):
        vi = '(i+'+str(i)+')'
        f.write(t5+'B['+vi+'*n+j] = alpha * B['+vi+'*n+j];\n')

    f.write(t4+'}\n')

    f.write(t3+'}\n')
    
