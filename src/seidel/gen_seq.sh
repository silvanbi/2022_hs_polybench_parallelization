#!/bin/bash

EXE_NAME="$1"
EXE_PATH="./${EXE_NAME}.out"
SIZES=(4000 6000 8000 10000)
TIMES=(80 60 40 20)

for i in ${!SIZES[@]}
do
    python3 generate_submit_scaling.py euler weak_scaling ${EXE_PATH} 1 ${SIZES[$i]} ${TIMES[$i]} 0 >> tmp.sh
done

mv tmp.sh OUT/submit_${EXE_NAME}.sh
chmod +x OUT/submit_${EXE_NAME}.sh