from datetime import datetime
from os import listdir
from os.path import isfile, join
import sys
import math

local_Ns = [1000, 2000]
local_Ts = [10, 30]
local_N_threads = [1,2]
local_N_OMP_threads_hybrid = [2, 4]

ct = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')


"""
USAGE:
------
GENERATE SCRIPTS FOR EULER
- arguments
  1. "euler"
    - indicates for what platform to generate the script
  2. [weak_scaling | strong_scaling] 
    - what kind of scaling to apply
  3. [path to program to benchmark]
    - relative to the directory from which you submit the submit_script from
  4. [nr of scaling steps to apply]
    - one scaling step doubles the number of of processes (and in case of weak scaling also scales the 
      problem size to keep FLOPS/process ~constant)
  5. initial problem size (N)
  6. nr of time steps per run (T)
  7. [0 | 1]
     - 0 indicates that the nr of processes should be increased
     - 1 indicates that nr of processes is constant 1, can be used for baseline measurements
  8. nr of cpus-per-task
     - only used for hybrid programs (in case running OpenMP threads inside a single MPI thread)

GENERATE SCRIPTS FOR LOCAL EXECUTION (STRONG SCALING)
  1. "local"
    - indicates for what platform to generate the script
  2. [combine]
    - run the program for each combination of N (local_Ns), T (local_Ts),
      #threads (local_N_threads) and in case of hybrid jobs also
      #OMP threads per MPI process (local_N_OMP_threads_hybrid)
    -
  3. [all]
    - include all the programs in the base directory in the script
  4. relative path to base directory (containing the programs to benchmark)

example: python3 generate_submit_scaling.py local combine all ./OUT
GENERATE SCRIPTS FOR LOCAL EXECUTION (WEAK SCALING)
  1. "local"
    - indicates for what platform to generate the script
  2. [weak_scaling]
  3. [all]
    - include all the programs in the base directory in the script
  4. relative path to base directory (containing the programs to benchmark)
  5. initial problem size (N)
  6. number of timesteps per run (T)
  7. number of scaling steps to take

example: python3 generate_submit_scaling.py local weak_scaling all ./OUT 1000 10 5
"""

create_dir = "mkdir -p measurements/{}/{}/{}/ > /dev/null 2>&1&&"
out = "measurements/{}/{}/{}/{}_{}_{}.txt "
out_hybrids = "measurements/{}/{}/{}/{}_{}_{}_{}.txt "

wrap_cmd ="export OMP_NUM_THREADS={}; {} {} {}"
wrap_cmd_seq ="{} {} {}"
wrap_cmd_srun="export OMP_NUM_THREADS={}; srun --cpus-per-task={} {} {} {}"


def scale_euler(pgm, steps, initial_n, t, baseline, scaling):
    print("#!/bin/bash")
    print("module load openmpi/4.1.4 &&")
    print("module load gcc/9.3.0 &&")

    print("")
    print("")
    print("mkdir -p measurements &&")
    print(create_dir.format(scaling, pgm[2:-4], ct))
    n = int(initial_n)
    n_threads = 1
    mem_per_cpu = math.ceil((n**2)*8/1e6)*3

    for i in range(steps):
        if "omp" in pgm:
            bash_cmd = "bash -c \"export OMP_NUM_THREADS={} && {}\""
            sbatch_cmd = "sbatch --constraint=EPYC_7763 --job-name={} --output={} --ntasks=1 --cpus-per-task={} --mem-per-cpu={}MB --wrap=\\\"{}\\\""
            if baseline == "1" and scaling == "weak_scaling":
                print(bash_cmd.format(1,sbatch_cmd.format(out.format(pgm[2:], scaling, pgm[2:-4],ct, n, t, 1), 1,mem_per_cpu, wrap_cmd.format(1, pgm, n, t))))
            else:
                print(bash_cmd.format(n_threads, sbatch_cmd.format(pgm[2:], out.format(scaling, pgm[2:-4],ct, n, t, n_threads), n_threads, mem_per_cpu, wrap_cmd.format(n_threads, pgm, n, t))))    
        
        elif "mpi" in pgm and "openmp" in pgm:
            cpus_per_task = sys.argv[8]
            bash_cmd = "bash -c \"export OMP_NUM_THREADS={} && {}\""
            sbatch_cmd = "sbatch --constraint=EPYC_7763 --job-name={} --nodes=1 --output={} --ntasks={} --mem-per-cpu=4G --cpus-per-task={} --wrap=\"{}\""
            print(bash_cmd.format(cpus_per_task,sbatch_cmd.format(pgm[2:], out_hybrids.format(scaling, pgm[2:-4],ct, n, t, 1, cpus_per_task), n_threads, cpus_per_task, wrap_cmd_srun.format(cpus_per_task, cpus_per_task, pgm, n, t))))

        
        elif "mpi" in pgm:
            sbatch_cmd = "sbatch --constraint=EPYC_7763 --job-name={} --nodes=1 --output={} --ntasks={} --mem-per-cpu=4GB --wrap=\"{}\""
            print(sbatch_cmd.format(pgm[2:], out.format(scaling, pgm[2:-4],ct, n, t, n_threads), n_threads, wrap_cmd_srun.format(1, 1, pgm, n, t)))
            

        elif "seq" in pgm and not "omp" in pgm:
            if scaling == "weak_scaling":
                sbatch_cmd = "sbatch --constraint=EPYC_7763 --job-name={} --output={} --ntasks=1 --cpus-per-task={} --mem-per-cpu={}G --wrap=\"{}\""
                print(sbatch_cmd.format(pgm[2:], out.format(scaling, pgm[2:-4], ct, n, t, 1), 1, 10, wrap_cmd_seq.format(pgm, n, t)))
            else:
                print("cannot do strong scaling on sequential program")


        n_threads *=2
        if scaling == "weak_scaling":
            n = math.ceil(math.sqrt(2)*n)
        if scaling == "strong_scaling":
            mem_per_cpu = mem_per_cpu // 2


def combine_local_seq_single(pgm, scaling, local_Ns=local_Ns, local_Ts=local_Ts):
    print(create_dir.format(scaling, pgm[2:-4], ct))
    for n in local_Ns:
        for t in local_Ts:
                print("echo \"benchmarking {} {} {}\" &&".format(pgm, n, t))
                print ("{} {} {} > {} &&".format(pgm, n, t, out.format(scaling, pgm[2:-4],ct, n, t, 1)))
    print("")
                
def combine_local_seq(pgm_base_dir):
    print("echo \"benchmarking sequential programs\" &&")
    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "seq" in f and not "omp" in f)]
    for pgm in pgms:
        combine_local_seq_single(pgm, scaling="combine")
    
    print("echo \"finished benchmarking sequential programs\" &&")    
    print("")

def combine_local_omp_single(pgm, scaling, local_Ns=local_Ns, local_Ts=local_Ts, local_N_threads=local_N_threads):
    print(create_dir.format(scaling, pgm[2:-4], ct))
    for n in local_Ns:
        for t in local_Ts:
            for n_threads in local_N_threads:
                print("echo \"benchmarking {} {} {} with {} threads\" &&".format(pgm, n, t, n_threads))
                print ("export OMP_NUM_THREADS={} && {} {} {} > {} &&".format(n_threads, pgm, n, t, out.format( scaling, pgm[2:-4],ct, n, t, n_threads)))
    print("")

def combine_local_omp(pgm_base_dir):
    print("echo \"benchmarking OpenMP programs\" &&")
    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "omp" in f)]
    for pgm in pgms:
        combine_local_omp_single(pgm, scaling="combine")
    
    print("echo \"finished benchmarking OpenMP programs\" &&")    
    print("")

def combine_local_mpi_single(pgm, scaling, local_Ns=local_Ns, local_Ts=local_Ts, local_N_threads=local_N_threads):
    print(create_dir.format(scaling, pgm[2:-4], ct))
    for n in local_Ns:
        for t in local_Ts:
            for n_threads in local_N_threads:
                print("echo \"benchmarking {} {} {} with {} MPI processes\" &&".format(pgm, n, t, n_threads))
                print ("mpiexec -np {} {} {} {} > {} &&".format(n_threads, pgm, n, t, out.format(scaling, pgm[2:-4],ct, n, t, n_threads)))
    print("")

def combine_local_mpi(pgm_base_dir):
    print("echo \"benchmarking MPI programs\" &&")
    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "mpi" in f and not "openmp" in f)]
    for pgm in pgms:
        combine_local_mpi_single(pgm, scaling="combine")
    
    print("echo \"finished benchmarking MPI programs\" &&")    
    print("")

def combine_local_hybrid_single(pgm, scaling, local_Ns=local_Ns, local_Ts=local_Ts, local_N_threads=local_N_threads):
    print(create_dir.format(scaling, pgm[2:-4], ct))
    for n in local_Ns:
        for t in local_Ts:
            for n_threads in local_N_threads:
                for n_omp_threads_per_MPI_proc in local_N_OMP_threads_hybrid:
                    print("echo \"benchmarking {} {} {} with {} MPI processes and {} OMP threads / MPI process \" &&".format(pgm, n, t, n_threads, n_omp_threads_per_MPI_proc))
                    print ("export OMP_NUM_THREADS={} && mpiexec -np {} {} {} {} > {} &&".format(n_omp_threads_per_MPI_proc, n_threads, pgm, n, t, out_hybrids.format( scaling, pgm[2:-4],ct, n, t, n_threads, n_omp_threads_per_MPI_proc)))
    print("")

def combine_local_hybrid(pgm_base_dir):
    print("echo \"benchmarking hybrid programs\" &&")
    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "mpi" in f and "openmp" in f)]
    for pgm in pgms:
        combine_local_hybrid_single(pgm, scaling="combine")
    
    print("echo \"finished benchmarking hybrid programs\"")    
    print("")

def combine_local(pgm_base_dir):
    print("#!/bin/bash")
    print("")
    print("")
    print("mkdir -p measurements &&")
    print("")
    combine_local_seq(pgm_base_dir)
    print("")
    combine_local_omp(pgm_base_dir)
    print("")
    combine_local_mpi(pgm_base_dir)
    print("")
    combine_local_hybrid(pgm_base_dir)
    
def weak_scale_local(pgm_base_dir, initial_n, t, steps):
    print("#!/bin/bash")
    print("")
    print("")
    print("mkdir -p measurements &&")
    print("")
    n_list = [initial_n]
    cur_n_threads_list = [1]
    cur_n = initial_n
    cur_n_threads = 1
    for i in range(steps):
        cur_n = math.ceil(cur_n*math.sqrt(2))
        cur_n_threads *= 2
        n_list.append(cur_n)
        cur_n_threads_list.append(cur_n_threads)
    c = 0
    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "seq" in f and not "omp" in f)]
    for pgm in pgms:
        for i in range(steps+1):
            combine_local_seq_single(pgm, scaling="weak_scaling", local_Ns=[n_list[i]], local_Ts=[t])
    
    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "omp" in f)]
    for pgm in pgms:
        for i in range(steps+1):
            combine_local_omp_single(pgm, scaling="weak_scaling", local_Ns=[n_list[i]], local_Ts=[t], local_N_threads=[cur_n_threads_list[i]])

    # NOTE: as of now, only #MPI processes are scaled
    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "mpi" in f and "openmp" in f)]
    for pgm in pgms:
        for i in range(steps+1):
            combine_local_hybrid_single(pgm, scaling="weak_scaling", local_Ns=[n_list[i]], local_Ts=[t], local_N_threads=[cur_n_threads_list[i]])


    pgms = ["./{}".format(f) for f in listdir(pgm_base_dir) if (isfile(join(pgm_base_dir, f)) and "mpi" in f and not "openmp" in f)]
    for pgm in pgms:
        for i in range(steps+1):
            combine_local_mpi_single(pgm, scaling="weak_scaling", local_Ns=[n_list[i]], local_Ts=[t], local_N_threads=[cur_n_threads_list[i]])

    print("echo \"finished measurements\"")

if __name__ == "__main__":
    functionality = sys.argv[1]
    if functionality == "euler":
        scaling = sys.argv[2]
        pgm = sys.argv[3]
        steps = int(sys.argv[4])
        initial_n = sys.argv[5]
        t = sys.argv[6]
        baseline = sys.argv[7]
        scale_euler(pgm, steps, initial_n, t, baseline, scaling)  
    elif functionality == "local":
        scaling = sys.argv[2]
        kind = sys.argv[3]
        if scaling == "combine":
            if kind == "all":
                pgm_base_dir = sys.argv[4] 
                combine_local(pgm_base_dir)
        elif scaling == "weak_scaling":
            if kind == "all":
                pgm_base_dir = sys.argv[4]
                initial_n = int(sys.argv[5])
                t = int(sys.argv[6])
                steps = int(sys.argv[7])
                weak_scale_local(pgm_base_dir, initial_n, t, steps)
    else:
        print("unknown functionality")

