#!/bin/bash

# 4000 80

mkdir -p measurements &&

echo "benchmarking sequential programs" &&
mkdir -p measurements/combine/seq_optimized/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 4000 80" &&
./seq_optimized.out 4000 80 > measurements/combine/seq_optimized/2022-12-15-16-33-38/4000_80_1.txt  &&

mkdir -p measurements/combine/seq_original/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 4000 80" &&
./seq_original.out 4000 80 > measurements/combine/seq_original/2022-12-15-16-33-38/4000_80_1.txt  &&

echo "finished benchmarking sequential programs" &&


echo "benchmarking OpenMP programs" &&
mkdir -p measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_auto.out 4000 80 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-33-38/4000_80_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-33-38/4000_80_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-33-38/4000_80_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime.out 4000 80 > measurements/combine/omp_wave_singletime/2022-12-15-16-33-38/4000_80_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-33-38/4000_80_16.txt  &&

mkdir -p measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-33-38/4000_80_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_auto.out 4000 80 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-33-38/4000_80_16.txt  &&

echo "finished benchmarking OpenMP programs" &&


echo "benchmarking MPI programs" &&
mkdir -p measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-33-38/4000_80_16.txt  &&

mkdir -p measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_1.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_2.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_4.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_8.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_12.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_13.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_14.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_15.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_serial.out 4000 80 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-33-38/4000_80_16.txt  &&

echo "finished benchmarking MPI programs" &&


# 6000 60


echo "benchmarking sequential programs" &&
mkdir -p measurements/combine/seq_optimized/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 6000 60" &&
./seq_optimized.out 6000 60 > measurements/combine/seq_optimized/2022-12-15-16-36-50/6000_60_1.txt  &&

mkdir -p measurements/combine/seq_original/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 6000 60" &&
./seq_original.out 6000 60 > measurements/combine/seq_original/2022-12-15-16-36-50/6000_60_1.txt  &&

echo "finished benchmarking sequential programs" &&


echo "benchmarking OpenMP programs" &&
mkdir -p measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 6000 60 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_auto.out 6000 60 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-36-50/6000_60_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 6000 60 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_ssa.out 6000 60 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-36-50/6000_60_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa_unroll2.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-36-50/6000_60_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./omp_wave_singletime.out 6000 60 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime.out 6000 60 > measurements/combine/omp_wave_singletime/2022-12-15-16-36-50/6000_60_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 6000 60 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa.out 6000 60 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-36-50/6000_60_16.txt  &&

mkdir -p measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 6000 60 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_parallelize_seq_opt.out 6000 60 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-36-50/6000_60_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 6000 60 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_auto.out 6000 60 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-36-50/6000_60_16.txt  &&

echo "finished benchmarking OpenMP programs" &&


echo "benchmarking MPI programs" &&
mkdir -p measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 6000 60 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_optimisedserial.out 6000 60 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-36-50/6000_60_16.txt  &&

mkdir -p measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_1.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_2.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_4.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_8.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_12.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_13.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_14.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_15.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 6000 60 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_serial.out 6000 60 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-36-50/6000_60_16.txt  &&

echo "finished benchmarking MPI programs"  &&

# 8000 40

echo "benchmarking sequential programs" &&
mkdir -p measurements/combine/seq_optimized/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 8000 40" &&
./seq_optimized.out 8000 40 > measurements/combine/seq_optimized/2022-12-15-16-38-43/8000_40_1.txt  &&

mkdir -p measurements/combine/seq_original/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 8000 40" &&
./seq_original.out 8000 40 > measurements/combine/seq_original/2022-12-15-16-38-43/8000_40_1.txt  &&

echo "finished benchmarking sequential programs" &&


echo "benchmarking OpenMP programs" &&
mkdir -p measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 8000 40 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_auto.out 8000 40 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-38-43/8000_40_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 8000 40 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_ssa.out 8000 40 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-38-43/8000_40_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa_unroll2.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-38-43/8000_40_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./omp_wave_singletime.out 8000 40 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime.out 8000 40 > measurements/combine/omp_wave_singletime/2022-12-15-16-38-43/8000_40_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8000 40 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa.out 8000 40 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-38-43/8000_40_16.txt  &&

mkdir -p measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 8000 40 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_parallelize_seq_opt.out 8000 40 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-38-43/8000_40_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 8000 40 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_auto.out 8000 40 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-38-43/8000_40_16.txt  &&

echo "finished benchmarking OpenMP programs" &&


echo "benchmarking MPI programs" &&
mkdir -p measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8000 40 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_optimisedserial.out 8000 40 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-38-43/8000_40_16.txt  &&

mkdir -p measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_1.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_2.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_4.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_8.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_12.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_13.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_14.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_15.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 8000 40 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_serial.out 8000 40 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-38-43/8000_40_16.txt  &&

echo "finished benchmarking MPI programs" &&



# 10000 20

echo "benchmarking sequential programs" &&
mkdir -p measurements/combine/seq_optimized/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 10000 20" &&
./seq_optimized.out 10000 20 > measurements/combine/seq_optimized/2022-12-15-16-40-58/10000_20_1.txt  &&

mkdir -p measurements/combine/seq_original/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 10000 20" &&
./seq_original.out 10000 20 > measurements/combine/seq_original/2022-12-15-16-40-58/10000_20_1.txt  &&

echo "finished benchmarking sequential programs" &&


echo "benchmarking OpenMP programs" &&
mkdir -p measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./omp_wave_multitime_auto.out 10000 20 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_auto.out 10000 20 > measurements/combine/omp_wave_multitime_auto/2022-12-15-16-40-58/10000_20_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./omp_wave_singletime_ssa.out 10000 20 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_ssa.out 10000 20 > measurements/combine/omp_wave_singletime_ssa/2022-12-15-16-40-58/10000_20_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa_unroll2.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58/10000_20_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./omp_wave_singletime.out 10000 20 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime.out 10000 20 > measurements/combine/omp_wave_singletime/2022-12-15-16-40-58/10000_20_16.txt  &&

mkdir -p measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 10000 20 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa.out 10000 20 > measurements/combine/omp_wave_multitime_better_ssa/2022-12-15-16-40-58/10000_20_16.txt  &&

mkdir -p measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./omp_parallelize_seq_opt.out 10000 20 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_parallelize_seq_opt.out 10000 20 > measurements/combine/omp_parallelize_seq_opt/2022-12-15-16-40-58/10000_20_16.txt  &&

mkdir -p measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 12 threads" &&
export OMP_NUM_THREADS=12 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 13 threads" &&
export OMP_NUM_THREADS=13 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 14 threads" &&
export OMP_NUM_THREADS=14 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 15 threads" &&
export OMP_NUM_THREADS=15 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./omp_wave_singletime_auto.out 10000 20 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_auto.out 10000 20 > measurements/combine/omp_wave_singletime_auto/2022-12-15-16-40-58/10000_20_16.txt  &&

echo "finished benchmarking OpenMP programs" &&


echo "benchmarking MPI programs" &&
mkdir -p measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 10000 20 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_optimisedserial.out 10000 20 > measurements/combine/mpi_insiderank_optimisedserial/2022-12-15-16-40-58/10000_20_16.txt  &&

mkdir -p measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_1.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_2.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_4.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_8.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 12 MPI processes" &&
mpiexec -np 12 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_12.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 13 MPI processes" &&
mpiexec -np 13 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_13.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 14 MPI processes" &&
mpiexec -np 14 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_14.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 15 MPI processes" &&
mpiexec -np 15 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_15.txt  &&
echo "benchmarking ./mpi_insiderank_serial.out 10000 20 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_serial.out 10000 20 > measurements/combine/mpi_insiderank_serial/2022-12-15-16-40-58/10000_20_16.txt  &&

echo "finished benchmarking MPI programs" 