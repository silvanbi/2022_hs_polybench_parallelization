# Seidel-2D

This source sub-folder contains work on parallelization of the Seidel-2D algorithm using MPI and OpenMP.

## Overview
Each optimization variant is a standalone program, which can be independently compiled and run. Additionally, a test method is linked to each program to enable checking correctness of the computation (see Test section below). Common functions such as initialization of matrices, argument parsing and matrix printing are present in the utils folder.

## Build
Run `make build` to compile all relevant programs. The result binaries are located in the `.OUT` folder.

## Run
To run a specific program do the following:
- `./program -t <n> <tsteps>` for running benchmark on <supplied matrix size>, <number of time steps> + test. Note that this requires `NRUNS=1` in the Makefile. For more convenient testing we recommend using `make test`.
- `./program <n> <tsteps>` for running benchmark on <supplied matrix size>, <number of time steps>

## Test
Run `make test` compiles all relevant programs and runs tests on all of them (regression testing). 

## References to versions mentioned in the report
The following table contains a mapping from Seidel-2D versions mentioned in the report to source code:
| Version      | Link to source code |
| ----------- | ----------- |
| Base PolyBench      | https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/original/original_seidel.c       |
| (Base) SO   | https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/seq/v1.c        |
| OpenMP MT | https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/openmp/auto/seidel_auto_better_alloc_ssa_unroll2.c | 
| OpenMP ST | https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/openmp/wavefront_fixed_t/auto_ssa.c | 
| MPI | https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/mpi/mpi_seidel_insiderank_serial.c |
| MPI w/ SO | https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/mpi/mpi_seidel_insiderank_optimisedserial.c | 

## Versions that did not get mentioned in the report
### Hybrid versions
We have implemented various hybrid versions that make use of both MPI and OpenMP. Unfortunately due to missing privileges we were unable to benchmark these versions on more than one node on Euler. These versions can be found here:
 - https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/mpi/mpi_seidel_insiderank_openmp.c
 - https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/mpi/mpi_seidel_insiderank_openmp_handopt_ssa.c

### Blocking single-time wavefront
We have implemented a blocking version of the single-time wavefront that reduces the FLOP count by reusing intermediate computations. Unfortunately, this version did not lead to a decrease of runtime. Its (very complex) source code can be found here:
 - https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/openmp/wavefront_fixed_t/v1_4.c

 ### Parallelization of Sequential Optimization
We have implemented  a parallel version of the sequential optimization described in the report with the goal of making better use of locality compared to a wavefront.  
This was done by parallelizing the work-intensive first pass by assigning each processor a consecutive chunk of elements to process. 
This introduces a Read-before-Write dependency as the update of the last element assigned to  processor i makes use of the value of the first element assigned to processor i+1 before it is updated. Due to this dependency, processors have to be synchronized using `omp barrier`.
Unfortunately, this approach did not perform well as approximately 2/3 of the total runtime is spent in the inherently sequential second pass, limiting the potential speedup attainable through parallelization.
The corresponding source code can be found here: 
 - https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/openmp/parallelization_seq_opt/v1.c

 ## Euler Batch Submission generator script
 We have implemented a python script that generates a shell script for batch-submitting jobs to evaluate strong-scaling and weak-scaling of programs. The script can be found here:
 - https://gitlab.ethz.ch/silvanbi/2022_hs_polybench_parallelization/-/blob/main/src/seidel/generate_submit_scaling.py
