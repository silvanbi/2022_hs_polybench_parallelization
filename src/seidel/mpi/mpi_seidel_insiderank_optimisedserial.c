#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>

#include "../utils/common.h"
#include "../utils/test.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "MPI_insiderank_optimisedserial"

int main(int argc, char** argv)
{
    double start;
    double times[NRUNS];

    int mpi_rank;
    int mpi_size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    int test = 0;
    int n;
    int tsteps;
    double *A;
    int *local_rows;
    int *local_rows_index_start;

    if (mpi_rank == 0)
    {
        parse_args(argc, argv, &n, &tsteps, &test);

        A = malloc(n * n * sizeof(double));
        init_array(n, A);
    }

    for (int run = 0; run < NRUNS; run++)
    {
        MPI_Barrier(MPI_COMM_WORLD);

        start = MPI_Wtime();

        // Broadcast -------------------------------------------------------------------------
        // Broadcast 'n' and 'tsteps' from the root
        MPI_Bcast(&n,      1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&tsteps, 1, MPI_INT, 0, MPI_COMM_WORLD);

        // Rows per thread
        // Ignore the first and last row, as they are never changed, and therefore are ghost
        int realRows = n-2;
        local_rows = malloc(mpi_size * sizeof(int));
        local_rows_index_start = malloc(mpi_size * sizeof(int));

        for (int i = 0; i < mpi_size; i++)
        {
            local_rows[i] = realRows / mpi_size;
        }

        for (int i = 0; i < realRows - (realRows / mpi_size) * mpi_size; i++)
        {
            local_rows[i] += 1;
        }

        local_rows_index_start[0] = 0;
        for (int i = 1; i < mpi_size; i++)
        {
            local_rows_index_start[i] = local_rows_index_start[i-1] + local_rows[i-1];
        }

        if (mpi_rank > 0)
        {
            A = malloc((local_rows[mpi_rank]+2) * n * sizeof(double));
        }

        // Broadcast 'A'
        if (mpi_rank == 0)
        {
            for (int source_rank = 1; source_rank < mpi_size; source_rank++)
            {
                MPI_Send( 
                    A + (local_rows_index_start[source_rank]) * n,  // buffer
                    (local_rows[source_rank]+2) * n, // count
                    MPI_DOUBLE, // datatype
                    source_rank, // dest
                    0, // tag
                    MPI_COMM_WORLD // MPI_comm
                );
            }
        } else {
            MPI_Recv(
                A, // buffer
                (local_rows[mpi_rank]+2) * n, // count
                MPI_DOUBLE, // datatype
                0, // source
                0, // tag
                MPI_COMM_WORLD, // MPI_comm
                MPI_STATUS_IGNORE // status
            );
        }

        // Compute -------------------------------------------------------------------------
        int top_real_row    = 1;
        int bottom_real_row = local_rows[mpi_rank];

        int top_ghost_row    = top_real_row    - 1;
        int bottom_ghost_row = bottom_real_row + 1;

        for (int t = 0; t < tsteps; t++)
        {
            // The first thread does not receive the top
            if (mpi_rank != 0)
            {
                // Receive top
                MPI_Recv(
                    A + top_ghost_row * n, // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank - 1, // source
                    0, // tag
                    MPI_COMM_WORLD, // MPI_comm
                    MPI_STATUS_IGNORE // status
                );
            }

            // Compute the first row and send it to the previous thread as soon as possible
            {
                int i = top_real_row;
                for (int j = 1; j < n - 1; j++)
                {
                    A[(i  )*(n)+j] = (A[(i-1)*n+(j-1)] + A[(i-1)*n+(j)] + A[(i-1)*n+(j+1)]
                                    + A[(i  )*n+(j-1)] + A[(i  )*n+(j)] + A[(i  )*n+(j+1)]
                                    + A[(i+1)*n+(j-1)] + A[(i+1)*n+(j)] + A[(i+1)*n+(j+1)])/9.0;
                }
            }
            
            // The first thread does not send anything to the top
            if (mpi_rank != 0)
            {
                // Send    top
                MPI_Send( 
                    A + top_real_row * n,  // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank - 1, // dest
                    0, // tag
                    MPI_COMM_WORLD // MPI_comm
                );
            }

            // Compute all other rows
            //
            // Based on: seq/no_lsb/v1
            {
                double inv9 = 1.0 / 9.0;

                for (int i = top_real_row + 1; i<= bottom_real_row; i++)
                {
                    double temp = A[(i-1)*n] + A[(i-1)*n+1] + A[(i+1)*n] + A[(i+1)*n+1] + A[(i+1)*n+2] + A[(i)*n+2];
                    for(int j = 1; j <= n - 2; j++)
                    {
                        double t0, t1, t2, t3, t4, t5, t6, t7;
                        double s0, s1, s2, s3;
                        double ss1, ss2;

                        t0 = A[(i-1)*n+j-1];
                        t1 = A[(i-1)*n+j];
                        t2 = A[(i-1)*n+j+1];
                        t3 = A[(i)*n+j];
                        t4 = A[(i)*n+j+1];
                        t5 = A[(i+1)*n+j-1];
                        t6 = A[(i+1)*n+j];
                        t7 = A[(i+1)*n+j+1];

                        s0 = t0 + t1;
                        s1 = t2 + t3;
                        s2 = t4 + t5;
                        s3 = t6 + t7;

                        ss1 = s0 + s1;
                        ss2 = s2 + s3;

                        A[(i)*n+j] = (ss1 + ss2)*inv9;

                    }
                    for (int j = 1; j <= n - 2; j++)
                    {
                        A[(i  )*(n)+j] += A[(i  )*(n)+j-1]*inv9;
                    }
                }
            }

            // The last thread does not send the bottom
            if (mpi_rank != mpi_size - 1)
            {
                // Send    bottom
                MPI_Send( 
                    A + bottom_real_row * n,  // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank + 1, // dest
                    0, // tag
                    MPI_COMM_WORLD // MPI_comm
                );

                // Receive bottom
                MPI_Recv(
                    A + bottom_ghost_row * n, // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank + 1, // source
                    0, // tag
                    MPI_COMM_WORLD, // MPI_comm
                    MPI_STATUS_IGNORE // status
                );
            }
        }

        // Gather -------------------------------------------------------------------------

        if (mpi_rank == 0)
        {
            for (int source_rank = 1; source_rank < mpi_size; source_rank++)
            {
                MPI_Recv(
                    A + (local_rows_index_start[source_rank] + 1) * n, // buffer
                    local_rows[source_rank] * n, // count
                    MPI_DOUBLE, // datatype
                    source_rank, // source
                    0, // tag
                    MPI_COMM_WORLD, // MPI_comm
                    MPI_STATUS_IGNORE // status
                );
            }
        } else {
            MPI_Send( 
                A + top_real_row * n,  // buffer
                local_rows[mpi_rank] * n, // count
                MPI_DOUBLE, // datatype
                0, // dest
                0, // tag
                MPI_COMM_WORLD // MPI_comm
            );
        }

        if (mpi_rank > 0)
        {
            free(A);
        }

        free(local_rows);
        free(local_rows_index_start);

        times[run] = MPI_Wtime() - start;
        
        MPI_Barrier(MPI_COMM_WORLD);
    }

    if (mpi_rank != 0)
    {
        MPI_Send( 
            times,  // buffer
            NRUNS, // count
            MPI_DOUBLE, // datatype
            0, // dest
            0, // tag
            MPI_COMM_WORLD // MPI_comm
        );
    }

    if (mpi_rank == 0)
    {
        printf("%s\n", PGM_NAME);
        print_times(times, NRUNS, mpi_rank);

        for (int source_rank = 1; source_rank < mpi_size; source_rank++)
        {
            MPI_Recv(
                times, // buffer
                NRUNS, // count
                MPI_DOUBLE, // datatype
                source_rank, // source
                0, // tag
                MPI_COMM_WORLD, // MPI_comm
                MPI_STATUS_IGNORE // status
            );

            print_times(times, NRUNS, source_rank);
        }

        if (test)
            if (NRUNS == 1)
                run_test(n, tsteps, A);
            else
                printf("Test cannot be run for NRUNS != 1\n");
    }

    if (mpi_rank == 0)
    {
        free(A);
    }

    MPI_Finalize();
    return 0;
}
