#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>

#include "../utils/common.h"
#include "../utils/test.h"


#define FMAX(a, b) ((a) > (b) ? (a) : (b))
#define FMIN(a, b) ((a) < (b) ? (a) : (b))
#define FFLOOR(a) ((int)((a) + 268435456.) - 268435456)
#define FCEIL(a) (268435456 - (int)(268435456. - (a)))

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "MPI_insiderank_openmp"

int main(int argc, char** argv)
{
    double start;
    double times[NRUNS];

    int mpi_rank;
    int mpi_size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    int test = 0;
    int n;
    int tsteps;
    double *A;
    int *local_rows;
    int *local_rows_index_start;

    if (mpi_rank == 0)
    {
        parse_args(argc, argv, &n, &tsteps, &test);

        A = malloc(n * n * sizeof(double));
        init_array(n, A);
    }

    for (int run = 0; run < NRUNS; run++)
    {
        MPI_Barrier(MPI_COMM_WORLD);

        start = MPI_Wtime();

        // Broadcast -------------------------------------------------------------------------
        // Broadcast 'n' and 'tsteps' from the root
        MPI_Bcast(&n,      1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&tsteps, 1, MPI_INT, 0, MPI_COMM_WORLD);

        // Rows per thread
        // Ignore the first and last row, as they are never changed, and therefore are ghost
        int realRows = n-2;
        local_rows = malloc(mpi_size * sizeof(int));
        local_rows_index_start = malloc(mpi_size * sizeof(int));

        for (int i = 0; i < mpi_size; i++)
        {
            local_rows[i] = realRows / mpi_size;
        }

        for (int i = 0; i < realRows - (realRows / mpi_size) * mpi_size; i++)
        {
            local_rows[i] += 1;
        }

        local_rows_index_start[0] = 0;
        for (int i = 1; i < mpi_size; i++)
        {
            local_rows_index_start[i] = local_rows_index_start[i-1] + local_rows[i-1];
        }

        if (mpi_rank > 0)
        {
            A = malloc((local_rows[mpi_rank]+2) * n * sizeof(double));
        }

        // Broadcast 'A'
        if (mpi_rank == 0)
        {
            for (int source_rank = 1; source_rank < mpi_size; source_rank++)
            {
                MPI_Send( 
                    A + (local_rows_index_start[source_rank]) * n,  // buffer
                    (local_rows[source_rank]+2) * n, // count
                    MPI_DOUBLE, // datatype
                    source_rank, // dest
                    0, // tag
                    MPI_COMM_WORLD // MPI_comm
                );
            }
        } else {
            MPI_Recv(
                A, // buffer
                (local_rows[mpi_rank]+2) * n, // count
                MPI_DOUBLE, // datatype
                0, // source
                0, // tag
                MPI_COMM_WORLD, // MPI_comm
                MPI_STATUS_IGNORE // status
            );
        }

        // Compute -------------------------------------------------------------------------
        int top_real_row    = 1;
        int bottom_real_row = local_rows[mpi_rank];

        int top_ghost_row    = top_real_row    - 1;
        int bottom_ghost_row = bottom_real_row + 1;

        for (int t = 0; t < tsteps; t++)
        {
            // The first thread does not receive the top
            if (mpi_rank != 0)
            {
                // Receive top
                MPI_Recv(
                    A + top_ghost_row * n, // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank - 1, // source
                    0, // tag
                    MPI_COMM_WORLD, // MPI_comm
                    MPI_STATUS_IGNORE // status
                );
            }

            // Compute the first row and send it to the previous thread as soon as possible
            {
                int i = top_real_row;
                for (int j = 1; j < n - 1; j++)
                {
                    A[(i  )*(n)+j] = (A[(i-1)*n+(j-1)] + A[(i-1)*n+(j)] + A[(i-1)*n+(j+1)]
                                    + A[(i  )*n+(j-1)] + A[(i  )*n+(j)] + A[(i  )*n+(j+1)]
                                    + A[(i+1)*n+(j-1)] + A[(i+1)*n+(j)] + A[(i+1)*n+(j+1)])/9.0;
                }
            }
            
            // The first thread does not send anything to the top
            if (mpi_rank != 0)
            {
                // Send    top
                MPI_Send( 
                    A + top_real_row * n,  // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank - 1, // dest
                    0, // tag
                    MPI_COMM_WORLD // MPI_comm
                );
            }

            // Compute all other rows
            //
            // Based on: Jack's wavefront
            {
                // Ihnore the first line
                double *AA = A+n;
                int height = bottom_ghost_row - top_ghost_row;
                int width = n;
                double inv9 = 1.0 / 9.0;

                for(int new_i = 3; new_i <= 2 * height + width - 6; new_i++) {
                    # pragma omp parallel
                    {
                        int new_j_ub = new_i - 2 * FCEIL(FMAX(1, (1.0 / 2.0) * (new_i - width + 2)));
                        int new_j_lb = new_i - 2 * FFLOOR(FMIN(height - 2, (1.0 / 2.0) * (new_i - 1)));

                        int enter = 0;
                        int i = 0;

                        #pragma omp for private(i)
                        for(int j = new_j_lb; j <= new_j_ub; j += 2) {
                            if (enter == 0) {
                                i = (new_i - j) >> 1;
                                enter = 1;
                            }

                            // Declaration
                            double a00, a01, a02, a03, a04, a05, a06, a07, a08;
                            double s00, s01, s02, s03, s04, s05, s06, s07;
                            double r0;

                            // Load
                            a00 = AA[(i-1)*width+j-1];
                            a01 = AA[(i-1)*width+j  ];
                            a02 = AA[(i-1)*width+j+1];
                            a03 = AA[(i  )*width+j-1];
                            a04 = AA[(i  )*width+j  ];
                            a05 = AA[(i  )*width+j+1];
                            a06 = AA[(i+1)*width+j-1];
                            a07 = AA[(i+1)*width+j  ];
                            a08 = AA[(i+1)*width+j+1];

                            // Compute
                            s00 = a00 + a01;
                            s01 = a02 + a03;
                            s02 = a04 + a05;
                            s03 = a06 + a07;

                            s04 = s00 + s01;
                            s05 = s02 + s03;

                            s06 = s04 + s05;
                            s07 = s06 + a08;

                            r0 = s07 * inv9;

                            // Store
                            AA[i*width+j  ] = r0;

                            i--;
                        }
                    }
                }
            }

            // The last thread does not send the bottom
            if (mpi_rank != mpi_size - 1)
            {
                // Send    bottom
                MPI_Send( 
                    A + bottom_real_row * n,  // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank + 1, // dest
                    0, // tag
                    MPI_COMM_WORLD // MPI_comm
                );

                // Receive bottom
                MPI_Recv(
                    A + bottom_ghost_row * n, // buffer
                    n, // count
                    MPI_DOUBLE, // datatype
                    mpi_rank + 1, // source
                    0, // tag
                    MPI_COMM_WORLD, // MPI_comm
                    MPI_STATUS_IGNORE // status
                );
            }
        }

        // Gather -------------------------------------------------------------------------

        if (mpi_rank == 0)
        {
            for (int source_rank = 1; source_rank < mpi_size; source_rank++)
            {
                MPI_Recv(
                    A + (local_rows_index_start[source_rank] + 1) * n, // buffer
                    local_rows[source_rank] * n, // count
                    MPI_DOUBLE, // datatype
                    source_rank, // source
                    0, // tag
                    MPI_COMM_WORLD, // MPI_comm
                    MPI_STATUS_IGNORE // status
                );
            }
        } else {
            MPI_Send( 
                A + top_real_row * n,  // buffer
                local_rows[mpi_rank] * n, // count
                MPI_DOUBLE, // datatype
                0, // dest
                0, // tag
                MPI_COMM_WORLD // MPI_comm
            );
        }

        if (mpi_rank > 0)
        {
            free(A);
        }

        free(local_rows);
        free(local_rows_index_start);

        times[run] = MPI_Wtime() - start;

        MPI_Barrier(MPI_COMM_WORLD);
    }

    if (mpi_rank != 0)
    {
        MPI_Send( 
            times,  // buffer
            NRUNS, // count
            MPI_DOUBLE, // datatype
            0, // dest
            0, // tag
            MPI_COMM_WORLD // MPI_comm
        );
    }

    if (mpi_rank == 0)
    {
        printf("%s\n", PGM_NAME);
        print_times(times, NRUNS, mpi_rank);

        for (int source_rank = 1; source_rank < mpi_size; source_rank++)
        {
            MPI_Recv(
                times, // buffer
                NRUNS, // count
                MPI_DOUBLE, // datatype
                source_rank, // source
                0, // tag
                MPI_COMM_WORLD, // MPI_comm
                MPI_STATUS_IGNORE // status
            );

            print_times(times, NRUNS, source_rank);
        }

        if (test)
            if (NRUNS == 1)
                run_test(n, tsteps, A);
            else
                printf("Test cannot be run for NRUNS != 1\n");
    }

    if (mpi_rank == 0)
    {
        free(A);
    }

    MPI_Finalize();
    return 0;
}
