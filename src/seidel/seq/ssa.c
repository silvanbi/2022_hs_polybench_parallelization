#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "../../utils/common.h"
#include "../../utils/test.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "seq_opt_v1_ssa"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    struct timeval start;
    struct timeval end;
    double times[NRUNS];

    parse_args(argc, argv, &n, &tsteps, &test);
    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        gettimeofday(&start, NULL);

        double inv9 = 1.0/9.0;
        for (int t = 0; t <= tsteps - 1; t++)
        {
            for (int i = 1; i<= n - 2; i++)
            {
                for (int j = 1; j <= n - 2; j++)
                {
                    double t1, t2, t3, t4, t5, t6, t7, t8, t9;
                    double s14, s25, s36;
                    double s147, s258, s369;
                    double st;
                    t1 = A[(i-1)*n+j-1];
                    t2 = A[(i-1)*n+j];
                    t3 = A[(i-1)*n+j+1];

                    t4 = A[i*n+j-1];
                    t5 = A[i*n+j];
                    t6 = A[i*n+j+1];

                    t7 = A[(i+1)*n+j-1];
                    t8 = A[(i+1)*n+j];
                    t9 = A[(i+1)*n+j+1];

                    s14 = t1 + t4;
                    s25 = t2 + t5;
                    s36 = t3 + t6;
                    
                    s147 = s14 + t7;
                    s258 = s25 + t8;
                    s369 = s36 + t9;

                    st = (s147 + s258 + s369)*inv9;
                    A[i*n+j] = st;
                }
            }
        }

        gettimeofday(&end, NULL);
        times[r] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);
    free(A);

    return 0;
}