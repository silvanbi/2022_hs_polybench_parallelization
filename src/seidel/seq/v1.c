#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "../utils/common.h"
#include "../utils/test.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "seq_optimized"


int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    struct timeval start;
    struct timeval end;
    double times[NRUNS];

    parse_args(argc, argv, &n, &tsteps, &test);
    

    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        gettimeofday(&start, NULL);

        double inv9 = 1.0 / 9.0;
        for (int t = 0; t <= tsteps - 1; t++)
        {

            for (int i = 1; i<= n - 2; i++)
            {
                for(int j = 1; j <= n - 2; j++)
                {
                    double t0, t1, t2, t3, t4, t5, t6, t7;
                    double s0, s1, s2, s3;
                    double ss1, ss2;

                    t0 = A[(i-1)*n+j-1];
                    t1 = A[(i-1)*n+j];
                    t2 = A[(i-1)*n+j+1];
                    t3 = A[(i)*n+j];
                    t4 = A[(i)*n+j+1];
                    t5 = A[(i+1)*n+j-1];
                    t6 = A[(i+1)*n+j];
                    t7 = A[(i+1)*n+j+1];

                    s0 = t0 + t1;
                    s1 = t2 + t3;
                    s2 = t4 + t5;
                    s3 = t6 + t7;

                    ss1 = s0 + s1;
                    ss2 = s2 + s3;

                    A[(i)*n+j] = (ss1 + ss2)*inv9;

                }
                for (int j = 1; j <= n - 2; j++)
                {
                    A[(i  )*(n)+j] += A[(i  )*(n)+j-1]*inv9;
                }
            }
        }

        gettimeofday(&end, NULL);
        times[r] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
    }

    if (test)
        run_test(n, tsteps, A);
    
    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
