#!/bin/bash

EXE_NAME="$1"
EXE_PATH="./${EXE_NAME}.out"
START_SIZE=4000
TIME=80

python3 generate_submit_scaling.py euler weak_scaling ${EXE_PATH} 8 ${START_SIZE} ${TIME} 0 >> tmp.sh

mv tmp.sh OUT/submit_ws_${EXE_NAME}.sh
chmod +x OUT/submit_ws_${EXE_NAME}.sh