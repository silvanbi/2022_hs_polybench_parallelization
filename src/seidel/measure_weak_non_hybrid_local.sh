#!/bin/bash


mkdir -p measurements &&

mkdir -p measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 4000 80" &&
./seq_optimized.out 4000 80 > measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 5657 80" &&
./seq_optimized.out 5657 80 > measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/5657_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 8001 80" &&
./seq_optimized.out 8001 80 > measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/8001_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 11316 80" &&
./seq_optimized.out 11316 80 > measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/11316_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_optimized.out 16004 80" &&
./seq_optimized.out 16004 80 > measurements/weak_scaling/seq_optimized/2022-12-15-16-48-41/16004_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_original/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 4000 80" &&
./seq_original.out 4000 80 > measurements/weak_scaling/seq_original/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_original/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 5657 80" &&
./seq_original.out 5657 80 > measurements/weak_scaling/seq_original/2022-12-15-16-48-41/5657_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_original/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 8001 80" &&
./seq_original.out 8001 80 > measurements/weak_scaling/seq_original/2022-12-15-16-48-41/8001_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_original/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 11316 80" &&
./seq_original.out 11316 80 > measurements/weak_scaling/seq_original/2022-12-15-16-48-41/11316_80_1.txt  &&

mkdir -p measurements/weak_scaling/seq_original/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./seq_original.out 16004 80" &&
./seq_original.out 16004 80 > measurements/weak_scaling/seq_original/2022-12-15-16-48-41/16004_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_auto.out 4000 80 > measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 5657 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_auto.out 5657 80 > measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 8001 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_auto.out 8001 80 > measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 11316 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_auto.out 11316 80 > measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_auto.out 16004 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_auto.out 16004 80 > measurements/weak_scaling/omp_wave_multitime_auto/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_ssa.out 4000 80 > measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 5657 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_ssa.out 5657 80 > measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 8001 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_ssa.out 8001 80 > measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 11316 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_ssa.out 11316 80 > measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_ssa.out 16004 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_ssa.out 16004 80 > measurements/weak_scaling/omp_wave_singletime_ssa/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa_unroll2.out 4000 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 5657 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa_unroll2.out 5657 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 8001 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa_unroll2.out 8001 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 11316 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa_unroll2.out 11316 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa_unroll2.out 16004 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa_unroll2.out 16004 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime.out 4000 80 > measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 5657 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime.out 5657 80 > measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 8001 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime.out 8001 80 > measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 11316 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime.out 11316 80 > measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime.out 16004 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime.out 16004 80 > measurements/weak_scaling/omp_wave_singletime/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_multitime_better_ssa.out 4000 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 5657 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_multitime_better_ssa.out 5657 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 8001 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_multitime_better_ssa.out 8001 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 11316 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_multitime_better_ssa.out 11316 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_multitime_better_ssa.out 16004 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_multitime_better_ssa.out 16004 80 > measurements/weak_scaling/omp_wave_multitime_better_ssa/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_parallelize_seq_opt.out 4000 80 > measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 5657 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_parallelize_seq_opt.out 5657 80 > measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 8001 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_parallelize_seq_opt.out 8001 80 > measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 11316 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_parallelize_seq_opt.out 11316 80 > measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_parallelize_seq_opt.out 16004 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_parallelize_seq_opt.out 16004 80 > measurements/weak_scaling/omp_parallelize_seq_opt/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 4000 80 with 1 threads" &&
export OMP_NUM_THREADS=1 && ./omp_wave_singletime_auto.out 4000 80 > measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 5657 80 with 2 threads" &&
export OMP_NUM_THREADS=2 && ./omp_wave_singletime_auto.out 5657 80 > measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 8001 80 with 4 threads" &&
export OMP_NUM_THREADS=4 && ./omp_wave_singletime_auto.out 8001 80 > measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 11316 80 with 8 threads" &&
export OMP_NUM_THREADS=8 && ./omp_wave_singletime_auto.out 11316 80 > measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./omp_wave_singletime_auto.out 16004 80 with 16 threads" &&
export OMP_NUM_THREADS=16 && ./omp_wave_singletime_auto.out 16004 80 > measurements/weak_scaling/omp_wave_singletime_auto/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 4000 80 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_optimisedserial.out 4000 80 > measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 5657 80 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_optimisedserial.out 5657 80 > measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 8001 80 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_optimisedserial.out 8001 80 > measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 11316 80 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_optimisedserial.out 11316 80 > measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_optimisedserial.out 16004 80 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_optimisedserial.out 16004 80 > measurements/weak_scaling/mpi_insiderank_optimisedserial/2022-12-15-16-48-41/16004_80_16.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 4000 80 with 1 MPI processes" &&
mpiexec -np 1 ./mpi_insiderank_serial.out 4000 80 > measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/4000_80_1.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 5657 80 with 2 MPI processes" &&
mpiexec -np 2 ./mpi_insiderank_serial.out 5657 80 > measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/5657_80_2.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 8001 80 with 4 MPI processes" &&
mpiexec -np 4 ./mpi_insiderank_serial.out 8001 80 > measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/8001_80_4.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 11316 80 with 8 MPI processes" &&
mpiexec -np 8 ./mpi_insiderank_serial.out 11316 80 > measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/11316_80_8.txt  &&

mkdir -p measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/ > /dev/null 2>&1&&
echo "benchmarking ./mpi_insiderank_serial.out 16004 80 with 16 MPI processes" &&
mpiexec -np 16 ./mpi_insiderank_serial.out 16004 80 > measurements/weak_scaling/mpi_insiderank_serial/2022-12-15-16-48-41/16004_80_16.txt  &&

echo "finished measurements"
