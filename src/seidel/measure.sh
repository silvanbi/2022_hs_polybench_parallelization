# parameters for benchmarking
N_list=(1000 1415 2002 2832 4006 5666)
T_list=(600)
NTHREADS_list=(1 2 4 8 16 32)

# programs to benchmark
target_list_seq=(
    "./original_seidel_no_lsb.out"
    "./seq_opt_seidel_ssa.out"
    "./seq_opt_seidel_v1.out"
    "./seq_opt_seidel_v2.out"
    )

target_list_omp=(
    "./omp_auto_seidel.out"
    "./omp_seidel_auto_nomax.out"
    "./seidel_auto_nomax_v2.out"
    "./seidel_auto_better_alloc.out"
    "./seidel_auto_better_alloc_v2.out"
    "./omp_wf_fixed_t_auto.out"
    "./omp_wf_fixed_t_v1_2_1.out"
    )

target_list_mpi=(
    "./mpi_seidel_insiderank_serial.out"
    "./mpi_seidel_insiderank_optimisedserial.out"
    "./mpi_seidel_insiderank_openmp.out"
    "./mpi_seidel_insiderank_openmp_handopt.out"
    "./mpi_seidel_insiderank_openmp_handopt_ssa.out"

)

# compile programs
make clean && make measure && 

# create folder to store measurements (named with increasing integers)
base_dir="measurements"
mkdir -p $base_dir &&
x=0
mkdir ${base_dir}/${x} > /dev/null 2>&1;
while [ $? -ne 0 ]
do
    x=$(( $x + 1 ))
    mkdir ${base_dir}/${x} > /dev/null 2>&1;
done

echo "result directory: ${base_dir}/${x}"

# run measurements
echo "running sequential measurements ..."
for target in ${target_list_seq[@]}; do
    mkdir ${base_dir}/${x}/${target}
    for N in ${N_list[@]}; do
        for T in ${T_list[@]}; do
                echo "measuring $target $N $T"
                $target $N $T > ${base_dir}/${x}/${target}/${N}_${T}
            done
        done
done
echo "finished sequential measurements ..."

echo "running openMP measurements"
for target in ${target_list_omp[@]}; do
    mkdir ${base_dir}/${x}/${target}
    for N in ${N_list[@]}; do
        for T in ${T_list[@]}; do
            for N_THREADS in ${NTHREADS_list[@]}; do
                export OMP_NUM_THREADS=$N_THREADS &&
                echo "measuring $target $N $T" &&
                $target $N $T > ${base_dir}/${x}/${target}/${N}_${T}_${N_THREADS}
            done
        done
    done
done
echo "finished openMP measurements ..."

echo "running MPI measurements\n"
for target in ${target_list_mpi[@]}; do
    mkdir ${base_dir}/${x}/${target}
    for N in ${N_list[@]}; do
        for T in ${T_list[@]}; do
            for N_THREADS in ${NTHREADS_list[@]}; do
                echo "measuring mpiexec -np $N_THREADS $target $N $T" &&
                mpiexec -np $N_THREADS $target $N $T > ${base_dir}/${x}/${target}/${N}_${T}_${N_THREADS}
            done
        done
    done
done
echo "finished MPI measurements ..."