/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* seidel-2d.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "../utils/test.h"


void kernel_seidel_2d(int n, int tsteps, double *A)
{
  for (int t = 0; t <= tsteps - 1; t++)
  {
    for (int i = 1; i<= n - 2; i++)
    {
      for (int j = 1; j <= n - 2; j++)
      {
        A[(i  )*(n)+j] = (A[(i-1)*n+(j-1)] + A[(i-1)*n+(j)] + A[(i-1)*n+(j+1)]
                        + A[(i  )*n+(j-1)] + A[(i  )*n+(j)] + A[(i  )*n+(j+1)]
                        + A[(i+1)*n+(j-1)] + A[(i+1)*n+(j)] + A[(i+1)*n+(j+1)])/9.0;
      }
    }
  }
}
