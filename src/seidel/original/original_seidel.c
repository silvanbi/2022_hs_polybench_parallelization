#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "../utils/common.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "seq_original"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    struct timeval start;
    struct timeval end;
    double times[NRUNS];


    parse_args(argc, argv, &n, &tsteps, &test);
    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        gettimeofday(&start, NULL);

        // Inlined from trmm.c to eliminate method call overhead
        for (int t = 0; t <= tsteps - 1; t++)
        {
            for (int i = 1; i<= n - 2; i++)
            {
                for (int j = 1; j <= n - 2; j++)
                {
                    A[(i  )*(n)+j] = (A[(i-1)*n+(j-1)] + A[(i-1)*n+(j)] + A[(i-1)*n+(j+1)]
                                    + A[(i  )*n+(j-1)] + A[(i  )*n+(j)] + A[(i  )*n+(j+1)]
                                    + A[(i+1)*n+(j-1)] + A[(i+1)*n+(j)] + A[(i+1)*n+(j+1)])/9.0;
                }
            }
        }

        gettimeofday(&end, NULL);
        times[r] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
    }

    if (test)
        printf("No test to be run for baseline version\n");

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
