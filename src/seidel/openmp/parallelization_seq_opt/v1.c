#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#include "../../utils/test.h"

#include "../../utils/common.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_par_seq_opt"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];
    

    parse_args(argc, argv, &n, &tsteps, &test);

    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {

        start = omp_get_wtime();
        
        /*
        IDEA
        ----
        each row is processed in 2 passes
        1. precompute as much as possible
        2. a sequential pass through the row to finish the computation
        */


        #pragma omp parallel 
        {  
            double inv9 = 1.0 / 9.0;
            int num_threads = omp_get_num_threads();
            int thread_num = omp_get_thread_num();
            int split = (n-2)/num_threads;

            // if cannot even assign a point to each process,
            // process it completely sequential on a single process
            if(split <= 0) {
                for (int t = 0; t <= tsteps - 1; t++)
                {
                    for (int i = 1; i<= n - 2; i++)
                    {
                        for(int j = 1; j <= n-2; j++)
                        {
                            double t0, t1, t2, t3, t4, t5, t6, t7;
                            double s0, s1, s2, s3;
                            double ss1, ss2;

                            t0 = A[(i-1)*n+j-1];
                            t1 = A[(i-1)*n+j];
                            t2 = A[(i-1)*n+j+1];
                            t3 = A[(i)*n+j];
                            t4 = A[(i)*n+j+1];
                            t5 = A[(i+1)*n+j-1];
                            t6 = A[(i+1)*n+j];
                            t7 = A[(i+1)*n+j+1];

                            s0 = t0 + t1;
                            s1 = t2 + t3;
                            s2 = t4 + t5;
                            s3 = t6 + t7;

                            ss1 = s0 + s1;
                            ss2 = s2 + s3;

                            A[(i)*n+j] = (ss1 + ss2)*inv9;

                        }  
                    }

                }
            } else {
                
            }
            for (int t = 0; t <= tsteps - 1; t++)
            {
                for (int i = 1; i<= n - 2; i++)
                {

                    // split the row in sequential chunks 
                    // and allocate to processes
                    // NOTE: currently, last process might 
                    // do much more work
                    int start_j = 1 + thread_num*split;
                    int end_j = start_j + split;
                    if(thread_num == num_threads-1) {
                        end_j = n-1;
                    }   

                    // boundary condition: last column of process p depends 
                    // on an element (next) that will get overwritten by process p+1
                    // => prevent read-after-write by using a barrier
                    double next;
                    if(thread_num != num_threads-1) {
                        next = A[i*n+end_j];
                    }

                    #pragma omp barrier

                    // process all but last element of the assigned chunk
                    // of the row.
                    // accumulate as much as possible of the seidel computation
                    // for the element, and store intermediate
                    // result inside the matrix
                    for(int j = start_j; j < end_j-1; j++)
                    {

                        double t0, t1, t2, t3, t4, t5, t6, t7;
                        double s0, s1, s2, s3;
                        double ss1, ss2;

                        t0 = A[(i-1)*n+j-1];
                        t1 = A[(i-1)*n+j];
                        t2 = A[(i-1)*n+j+1];
                        t3 = A[(i)*n+j];
                        t4 = A[(i)*n+j+1];
                        t5 = A[(i+1)*n+j-1];
                        t6 = A[(i+1)*n+j];
                        t7 = A[(i+1)*n+j+1];

                        s0 = t0 + t1;
                        s1 = t2 + t3;
                        s2 = t4 + t5;
                        s3 = t6 + t7;

                        ss1 = s0 + s1;
                        ss2 = s2 + s3;

                        A[(i)*n+j] = (ss1 + ss2)*inv9;

                    }   

                    // process last element of the chunk by making
                    // use of the previously read element of the 
                    // next processes' chunk
                    int j = end_j-1;
                    if(j >= 1) {
                        if(thread_num != num_threads-1) {
                            double t0, t1, t2, t3, t4, t5, t6, t7;
                            double s0, s1, s2, s3;
                            double ss1, ss2;

                            t0 = A[(i-1)*n+j-1];
                            t1 = A[(i-1)*n+j];
                            t2 = A[(i-1)*n+j+1];
                            t3 = A[(i)*n+j];
                            t4 = next;
                            t5 = A[(i+1)*n+j-1];
                            t6 = A[(i+1)*n+j];
                            t7 = A[(i+1)*n+j+1];

                            s0 = t0 + t1;
                            s1 = t2 + t3;
                            s2 = t4 + t5;
                            s3 = t6 + t7;

                            ss1 = s0 + s1;
                            ss2 = s2 + s3;

                            A[(i)*n+j] = (ss1 + ss2)*inv9;
                        } else {
                            double t0, t1, t2, t3, t4, t5, t6, t7;
                            double s0, s1, s2, s3;
                            double ss1, ss2;

                            t0 = A[(i-1)*n+j-1];
                            t1 = A[(i-1)*n+j];
                            t2 = A[(i-1)*n+j+1];
                            t3 = A[(i)*n+j];
                            t4 = A[(i)*n+j+1];
                            t5 = A[(i+1)*n+j-1];
                            t6 = A[(i+1)*n+j];
                            t7 = A[(i+1)*n+j+1];

                            s0 = t0 + t1;
                            s1 = t2 + t3;
                            s2 = t4 + t5;
                            s3 = t6 + t7;

                            ss1 = s0 + s1;
                            ss2 = s2 + s3;

                            A[(i)*n+j] = (ss1 + ss2)*inv9;
                        }
                    }   
                    
                    #pragma omp barrier

                    // 2. pass through the row
                    // this computation is inherently sequential
                    // NOTE: this is an FMA instruction
                    #pragma omp single
                    {
                        double inv9 = 1.0 / 9.0;
                        for (int j = 1; j <= n - 2; j++)
                        {
                            A[(i  )*(n)+j] += A[(i  )*(n)+j-1]*inv9;
                        } 

                    }

                }
            }
            #pragma omp barrier
        }
        times[r] = omp_get_wtime() - start;
    }

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    if (test)
        run_test(n, tsteps, A);

    free(A);
    return 0;
}
