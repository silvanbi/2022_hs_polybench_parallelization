#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include "../../utils/common.h"
#include "../../utils/test.h"

#define FMAX(a, b) ((a) > (b) ? (a) : (b))
#define FMIN(a, b) ((a) < (b) ? (a) : (b))
#define FFLOOR(a) ((int)((a) + 268435456.) - 268435456)
#define FCEIL(a) (268435456 - (int)(268435456. - (a)))

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_seidel_auto_nomax_v2"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];

    parse_args(argc, argv, &n, &tsteps, &test);

    double *A = malloc(n*n*sizeof(double));

    // Precomputed variables
    double tmp_0 = -0.75 * n;
    double tmp_1 = 0.25 * n;
    double tmp_2 = 0.5 * n;
    int tsteps_1 = tsteps - 1;

    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        for(int new_t = 3; new_t <= 3 * n + 4 * tsteps - 10; new_t++) {
            // Auxiliary variables
            double new_t_025 = 0.25 * new_t;
            double new_t_05 = 0.5 * new_t;
            double new_t_05_tmp_2 = new_t_05 - tmp_2;
            double new_t_025_tmp_2_075 = new_t_025 - tmp_2 + 0.75;
            int new_t_025_tmp_2_075_res = (((int)new_t_025_tmp_2_075) == new_t_025_tmp_2_075);
            double new_t_025_tmp_1 = new_t_025 - tmp_1;
            int new_t_025_tmp_1_res = (((int)new_t_025_tmp_1) == new_t_025_tmp_1);
            int Li1m = FCEIL(new_t_025_tmp_1);
            int Ui3m = FFLOOR(new_t_025_tmp_2_075);

            // Compute constants for lower and upper bounds
            int new_i_lb = FCEIL(FMAX(0, tmp_0 + new_t_025 + 1.5));
            int new_i_ub = FFLOOR(FMIN(tsteps_1, new_t_025 - 0.75));

            // First part bounds
            int Li1 = FMAX(Li1m, new_i_lb);
            int Uj1_const = FFLOOR(new_t_05 - 0.5);

            // Second part bounds
            int Li2 = FMAX(new_i_lb, FCEIL(new_t_025_tmp_2_075) + new_t_025_tmp_2_075_res);
            int Ui2 = FMIN(new_i_ub, FFLOOR(new_t_025_tmp_1) - new_t_025_tmp_1_res);
            int Lj2_const = FCEIL(new_t_05_tmp_2);
            int Uj2_const = Uj1_const;

            // Third part bounds
            int Ui3 = FMIN(Ui3m, new_i_ub);
            int Lj3_const = Lj2_const;

            // first part
            #pragma omp parallel for
            for(int new_i = Li1; new_i <= new_i_ub; new_i++) {
                for(int new_j = 1; new_j <= (Uj1_const - 2*new_i); new_j++) {
                    int i = new_j;
                    int j = -4 * new_i - 2 * new_j + new_t;
                    A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                                A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                                A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
                }
            }
            // second part
            #pragma omp parallel for
            for(int new_i = Li2; new_i <= Ui2; new_i++) {
                for(int new_j = (Lj2_const - 2*new_i + 1); new_j <= (Uj2_const - 2*new_i); new_j++) {
                    int i = new_j;
                    int j = -4 * new_i - 2 * new_j + new_t;
                    A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                                A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                                A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
                }
            }
            // third part
            #pragma omp parallel for
            for(int new_i = new_i_lb; new_i <= Ui3; new_i++) {
                for(int new_j = (Lj3_const - 2*new_i + 1); new_j <= n-2; new_j++) {
                    int i = new_j;
                    int j = -4 * new_i - 2 * new_j + new_t;
                    A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                                A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                                A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
                }
            }

            // automatic parallelization result commented below
            // #pragma omp parallel for
            // for(int new_i = new_i_lb; new_i <= new_i_ub; new_i++) {
            //     int new_j_lb = FCEIL(fmax(1, Ui3m - 2 * new_i + 0.5 * new_t + 1));
            //     int new_j_ub = FFLOOR(fmin(n - 2, -2 * new_i + 0.5 * new_t - 0.5));
            //     for(int new_j = new_j_lb; new_j <= new_j_ub; new_j++) {
                    // int i = new_j;
                    // int j = -4 * new_i - 2 * new_j + new_t;
                    // A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                    //             A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                    //             A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
            //     }
            // }
        }

        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);
    free(A);
    return 0;
}
