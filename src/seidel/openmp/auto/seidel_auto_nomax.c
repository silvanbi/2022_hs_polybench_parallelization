#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include "../../utils/common.h"
#include "../../utils/test.h"

#define FMAX(a, b) ((a) > (b) ? (a) : (b))
#define FMIN(a, b) ((a) < (b) ? (a) : (b))

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_seidel_auto_nomax"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];

    parse_args(argc, argv, &n, &tsteps, &test);

    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        for(int new_t = 3; new_t <= 3 * n + 4 * tsteps - 10; new_t++) {
            int new_i_lb = ceil(FMAX(0, (1.0 / 4.0) * (-3 * n + new_t + 6)));
            int new_i_ub = floor(FMIN(tsteps - 1, (1.0 / 4.0) * (new_t - 3)));

            // first part
            int tmp1 = ceil(0.25*(new_t - n));
            int Li1 = FMAX(tmp1, new_i_lb);
            #pragma omp parallel for
            for(int new_i = Li1; new_i <= new_i_ub; new_i++) {
                for(int new_j = 1; new_j <= floor(0.5*new_t - 0.5) - 2*new_i; new_j++) {
                    int i = new_j;
                    int j = -4 * new_i - 2 * new_j + new_t;
                    A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                                A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                                A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
                }
            }
            // second part
            int Li2 = FMAX(new_i_lb, ceil(0.25 * (new_t - 2*n + 3)) + ((new_t - 2*n + 3) % 4 == 0));
            int Ui2 = FMIN(new_i_ub, floor(0.25 * (new_t - n)) - ((new_t - n) % 4 == 0));
            #pragma omp parallel for
            for(int new_i = Li2; new_i <= Ui2; new_i++) {
                for(int new_j = ceil(0.5*(new_t - n)) - 2*new_i + 1; new_j <= floor(0.5*new_t - 0.5) - 2*new_i; new_j++) {
                    int i = new_j;
                    int j = -4 * new_i - 2 * new_j + new_t;
                    A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                                A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                                A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
                }
            }
            // third part
            int tmp3 = floor(0.25*(new_t - 2*n + 3));
            int Ui3 = FMIN(tmp3, new_i_ub);
            #pragma omp parallel for
            for(int new_i = new_i_lb; new_i <= Ui3; new_i++) {
                for(int new_j = ceil(0.5*(new_t - n)) - 2*new_i + 1; new_j <= n-2; new_j++) {
                    int i = new_j;
                    int j = -4 * new_i - 2 * new_j + new_t;
                    A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                                A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                                A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
                }
            }

            // automatic parallelization result commented below
            // #pragma omp parallel for
            // for(int new_i = new_i_lb; new_i <= new_i_ub; new_i++) {
            //     int new_j_lb = ceil(fmax(1, tmp3 - 2 * new_i + 0.5 * new_t + 1));
            //     int new_j_ub = floor(fmin(n - 2, -2 * new_i + 0.5 * new_t - 0.5));
            //     for(int new_j = new_j_lb; new_j <= new_j_ub; new_j++) {
                    // int i = new_j;
                    // int j = -4 * new_i - 2 * new_j + new_t;
                    // A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                    //             A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                    //             A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
            //     }
            // }
        }

        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);
    free(A);
    return 0;
}
