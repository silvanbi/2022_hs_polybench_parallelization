#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include "../../utils/common.h"
#include "../../utils/test.h"

#define FMAX(a, b) ((a) > (b) ? (a) : (b))
#define FMIN(a, b) ((a) < (b) ? (a) : (b))
#define FFLOOR(a) ((int)((a) + 268435456.) - 268435456)
#define FCEIL(a) (268435456 - (int)(268435456. - (a)))

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_wave_multitime_better_ssa_unroll2"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];

    parse_args(argc, argv, &n, &tsteps, &test);


    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        // division precomputation
        double _9 = 1.0 / 9.0;

        // automatic parallelization using opoly (with better allocation matrix)
        for(int new_t = 3; new_t <= 3 * n + 4 * tsteps - 10; new_t++) {
            int i_lb = FCEIL(FMAX(1, (1.0 / 2.0) * (-n + new_t - 4 * tsteps + 6)));
            int i_ub = FFLOOR(FMIN(n - 2, (1.0 / 2.0) * (new_t - 1)));
            #pragma omp parallel for
            for(int i = i_lb; i <= i_ub; i++) {
                int tmp = -2*i + new_t;
                int j_lb = tmp + -4 * FFLOOR(FMIN(tsteps - 1, (1.0 / 4.0) * (tmp - 1)));
                int j_ub = tmp + -4 * FCEIL(FMAX(0, (1.0 / 4.0) * (-n + tmp + 2)));

                // Swift for loop unrolling
                int remainder = ((j_ub - j_lb) % 8 == 0);
                j_ub -= (remainder == 1 ? 4 : 0);

                // Unrolled loop by factor of 2
                for(int j = j_lb; j <= j_ub; j += 8) {
                    // Declaration
                    double a00, a01, a02, a03, a04, a05, a06, a07, a08;
                    double a10, a11, a12, a13, a14, a15, a16, a17, a18;
                    double s00, s01, s02, s03, s04, s05, s06, s07;
                    double s10, s11, s12, s13, s14, s15, s16, s17;
                    double r0, r1;

                    // Load
                    a00 = A[(i-1)*n+j-1];
                    a01 = A[(i-1)*n+j  ];
                    a02 = A[(i-1)*n+j+1];
                    a03 = A[(i  )*n+j-1];
                    a04 = A[(i  )*n+j  ];
                    a05 = A[(i  )*n+j+1];
                    a06 = A[(i+1)*n+j-1];
                    a07 = A[(i+1)*n+j  ];
                    a08 = A[(i+1)*n+j+1];

                    a10 = A[(i-1)*n+(j+4)-1];
                    a11 = A[(i-1)*n+(j+4)  ];
                    a12 = A[(i-1)*n+(j+4)+1];
                    a13 = A[(i  )*n+(j+4)-1];
                    a14 = A[(i  )*n+(j+4)  ];
                    a15 = A[(i  )*n+(j+4)+1];
                    a16 = A[(i+1)*n+(j+4)-1];
                    a17 = A[(i+1)*n+(j+4)  ];
                    a18 = A[(i+1)*n+(j+4)+1];

                    // Compute
                    s00 = a00 + a01;
                    s01 = a02 + a03;
                    s02 = a04 + a05;
                    s03 = a06 + a07;

                    s10 = a10 + a11;
                    s11 = a12 + a13;
                    s12 = a14 + a15;
                    s13 = a16 + a17;

                    s04 = s00 + s01;
                    s05 = s02 + s03;

                    s14 = s10 + s11;
                    s15 = s12 + s13;

                    s06 = s04 + s05;
                    s07 = s06 + a08;

                    s16 = s14 + s15;
                    s17 = s16 + a18;
                    
                    r0 = s07 * _9;
                    r1 = s17 * _9;

                    // Store
                    A[i*n+j  ] = r0;
                    A[i*n+j+4] = r1;
                }

                // Fix odd unrolling
                if (remainder == 1) {
                    int j = j_ub + 4;
                    // Declaration
                    double a00, a01, a02, a03, a04, a05, a06, a07, a08;
                    double s00, s01, s02, s03, s04, s05, s06, s07;
                    double r0;

                    // Load
                    a00 = A[(i-1)*n+j-1];
                    a01 = A[(i-1)*n+j  ];
                    a02 = A[(i-1)*n+j+1];
                    a03 = A[(i  )*n+j-1];
                    a04 = A[(i  )*n+j  ];
                    a05 = A[(i  )*n+j+1];
                    a06 = A[(i+1)*n+j-1];
                    a07 = A[(i+1)*n+j  ];
                    a08 = A[(i+1)*n+j+1];

                    // Compute
                    s00 = a00 + a01;
                    s01 = a02 + a03;
                    s02 = a04 + a05;
                    s03 = a06 + a07;

                    s04 = s00 + s01;
                    s05 = s02 + s03;

                    s06 = s04 + s05;
                    s07 = s06 + a08;
                    
                    r0 = s07 * _9;

                    // Store
                    A[i*n+j  ] = r0;
                }
            }
        }
        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);
    free(A);
    return 0;
}
