#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include "../../utils/common.h"
#include "../../utils/test.h"

#define FMAX(a, b) ((a) > (b) ? (a) : (b))
#define FMIN(a, b) ((a) < (b) ? (a) : (b))
#define FFLOOR(a) ((int)((a) + 268435456.) - 268435456)
#define FCEIL(a) (268435456 - (int)(268435456. - (a)))

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_multitime_auto"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];

    parse_args(argc, argv, &n, &tsteps, &test);

    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    
    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        // automatic parallelization using opoly
        for(int new_t = 3; new_t <= 3 * n + 4 * tsteps - 10; new_t++) {
            int new_i_lb = ceil(fmax(0, (1.0 / 4.0) * (-3 * n + new_t + 6)));
            int new_i_ub = floor(fmin(tsteps - 1, (1.0 / 4.0) * (new_t - 3)));
            #pragma omp parallel for
            for(int new_i = new_i_lb; new_i <= new_i_ub; new_i++) {
                int new_j_lb = ceil(fmax(1, (1.0 / 2.0) * (-n - 4 * new_i + new_t + 2)));
                int new_j_ub = floor(fmin(n - 2, (1.0 / 2.0) * (-4 * new_i + new_t - 1)));
                for(int new_j = new_j_lb; new_j <= new_j_ub; new_j++) {
                    int i = new_j;
                    int j = -4 * new_i - 2 * new_j + new_t;
                    A[i*n+j] = (A[(i-1)*n+j-1] + A[(i-1)*n+j] + A[(i-1)*n+j+1] +
                                A[ i   *n+j-1] + A[ i   *n+j] + A[ i   *n+j+1] +
                                A[(i+1)*n+j-1] + A[(i+1)*n+j] + A[(i+1)*n+j+1] ) / 9.0;
                }
            }
        }


        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
