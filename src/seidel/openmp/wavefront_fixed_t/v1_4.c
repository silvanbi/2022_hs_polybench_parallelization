#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>
#include "../../utils/common.h"
#include "../../utils/test.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_wave_singletime_handopt_ssa_v3"


int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];
    


    parse_args(argc, argv, &n, &tsteps, &test);

    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    int n_odd;
    int full_mode_odd;
    int first_decreasing_odd;
    int first_decreasing_odd_1;

    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        n_odd = n % 2;
        full_mode_odd = (int)(((n-2)*0.5)) % 2;
        first_decreasing_odd = (int)(1 + (n-2-n*0.5)) % 2;
        first_decreasing_odd_1 = (int)((n-2-n*0.5-1)) % 2;


        for (int t = 0; t <= tsteps - 1; t++)
        {

            // process upper triangular
            #pragma omp parallel
            {
                double inv9 = 1.0/9.0;
                int j = 1;
                int num_steps = 1;
                for (; j <= n - 5; j+=4)
                {
                    int base_i = 1;
                    int base_j = j;

                    // parallel execution of first (odd-length) diagonal
                    #pragma omp for
                    for(int s = 0; s < num_steps-1; s+=2) {
                        
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        int cur_i_1 = base_i + (s+1);
                        int cur_j_1 = base_j - 2*(s+1);
                        int cur_j_2 = base_j - 2*(s+1)+1;
                        double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                        double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                        double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                        double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                        double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                        double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                        a0 = A[(cur_i-1)*n+(cur_j-1)];
                        a1 = A[(cur_i-1)*n+(cur_j)];
                        a2 = A[(cur_i-1)*n+(cur_j+1)];
                        a3 = A[(cur_i)*n+(cur_j-1)];
                        a4 = A[(cur_i)*n+(cur_j)];
                        a5 = A[(cur_i)*n+(cur_j+1)];
                        a6 = A[(cur_i+1)*n+(cur_j-1)];
                        a7 = A[(cur_i+1)*n+(cur_j)];
                        a8 = A[(cur_i+1)*n+(cur_j+1)];

                        a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                        a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                        a2_1 = a3;
                        a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                        a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                        a5_1 = a6;
                        a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                        a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                        a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                        intm0 = a3+a6;
                        intm1 = a4+a7;
                        intm2 = a0+a1;
                        intm3 = a2+a5;

                        intm4 = intm0+intm1;
                        intm5 = intm2+intm3;

                        intm6 = intm4+intm5;
                        intm7 = intm6+a8;
                        intm8 = intm7*inv9; 




                        intm0_1 = a7_1+a8_1;
                        intm1_1 = a0_1+a1_1;
                        intm2_1 = a3_1+a6_1;

                        intm3_1 = intm0_1+intm1_1;
                        intm4_1 = intm2_1+intm0;

                        intm5_1 = intm3_1+intm4_1;
                        intm6_1 = intm5_1+a4_1;
                        intm7_1 = intm6_1*inv9;

                        a0_2 = a1_1;
                        a1_2 = a2_1;
                        a2_2 = intm8;
                        a3_2 = intm7_1;
                        a4_2 = a6;
                        a5_2 = a7;
                        a6_2 = a7_1;
                        a7_2 = a8_1;
                        a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                        intm0_2 = intm0+a0_2;
                        intm1_2 = intm0_1+a8_2;
                        intm2_2 = a3_2+a2_2;
                        intm3_2 = intm0_2+intm1_2;
                        intm4_2 = intm2_2+a5_2;
                        intm5_2 = intm3_2 + intm4_2;
                        intm6_2 = intm5_2*inv9;


                        A[(cur_i  )*(n)+cur_j] = intm8;
                        A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                        A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;

                    }

                    // cleanup last element of current wave 
                    #pragma omp single 
                    {
                        int s = num_steps - 1;
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                        double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                        a0 = A[(cur_i-1)*n+(cur_j-1)];
                        a1 = A[(cur_i-1)*n+(cur_j)];
                        a2 = A[(cur_i-1)*n+(cur_j+1)];
                        a3 = A[(cur_i)*n+(cur_j-1)];
                        a4 = A[(cur_i)*n+(cur_j)];
                        a5 = A[(cur_i)*n+(cur_j+1)];
                        a6 = A[(cur_i+1)*n+(cur_j-1)];
                        a7 = A[(cur_i+1)*n+(cur_j)];
                        a8 = A[(cur_i+1)*n+(cur_j+1)];

                        intm0 = a0+a1;
                        intm1 = a2+a3;
                        intm2 = a5+a6;
                        intm3 = a7+a8;

                        intm5 = intm0+intm1;
                        intm6 = intm2+intm3;

                        intm7 = intm5+intm6;
                        intm8 = (intm7+a4)*inv9;

                        A[(cur_i  )*(n)+cur_j] = intm8; 

                    }

                    base_i = 1;
                    base_j = j+1;

                    // parallel execution of second (odd-length) diagonal
                    int num_steps_1 = num_steps*0.5;
                    #pragma omp for
                    for(int s = 0; s <= num_steps_1; s++) {
                        
                        int cur_i = base_i + 2*s;
                        int cur_j = base_j - 4*s;
                        double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                        double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                        a0 = A[(cur_i-1)*n+(cur_j-1)];
                        a1 = A[(cur_i-1)*n+(cur_j)];
                        a2 = A[(cur_i-1)*n+(cur_j+1)];
                        a3 = A[(cur_i)*n+(cur_j-1)];
                        a4 = A[(cur_i)*n+(cur_j)];
                        a5 = A[(cur_i)*n+(cur_j+1)];
                        a6 = A[(cur_i+1)*n+(cur_j-1)];
                        a7 = A[(cur_i+1)*n+(cur_j)];
                        a8 = A[(cur_i+1)*n+(cur_j+1)];

                        intm0 = a0+a1;
                        intm1 = a2+a3;
                        intm2 = a5+a6;
                        intm3 = a7+a8;

                        intm5 = intm0+intm1;
                        intm6 = intm2+intm3;

                        intm7 = intm5+intm6;
                        intm8 = (intm7+a4)*inv9;

                        A[(cur_i  )*(n)+cur_j] = intm8; 

                    }
                    
                    num_steps += 1;

                    base_i = 1;
                    base_j = j+2;

                    // parallel execution of third (even-length) diagonal
                    #pragma omp for
                    for(int s = 0; s < num_steps-1; s+=2) {
                        
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        int cur_i_1 = base_i + (s+1);
                        int cur_j_1 = base_j - 2*(s+1);
                        int cur_j_2 = base_j - 2*(s+1)+1;
                        double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                        double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                        double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                        double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                        double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                        double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                        a0 = A[(cur_i-1)*n+(cur_j-1)];
                        a1 = A[(cur_i-1)*n+(cur_j)];
                        a2 = A[(cur_i-1)*n+(cur_j+1)];
                        a3 = A[(cur_i)*n+(cur_j-1)];
                        a4 = A[(cur_i)*n+(cur_j)];
                        a5 = A[(cur_i)*n+(cur_j+1)];
                        a6 = A[(cur_i+1)*n+(cur_j-1)];
                        a7 = A[(cur_i+1)*n+(cur_j)];
                        a8 = A[(cur_i+1)*n+(cur_j+1)];

                        a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                        a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                        a2_1 = a3;
                        a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                        a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                        a5_1 = a6;
                        a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                        a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                        a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                        intm0 = a3+a6;
                        intm1 = a4+a7;
                        intm2 = a0+a1;
                        intm3 = a2+a5;

                        intm4 = intm0+intm1;
                        intm5 = intm2+intm3;

                        intm6 = intm4+intm5;
                        intm7 = intm6+a8;
                        intm8 = intm7*inv9; 




                        intm0_1 = a7_1+a8_1;
                        intm1_1 = a0_1+a1_1;
                        intm2_1 = a3_1+a6_1;

                        intm3_1 = intm0_1+intm1_1;
                        intm4_1 = intm2_1+intm0;

                        intm5_1 = intm3_1+intm4_1;
                        intm6_1 = intm5_1+a4_1;
                        intm7_1 = intm6_1*inv9;

                        a0_2 = a1_1;
                        a1_2 = a2_1;
                        a2_2 = intm8;
                        a3_2 = intm7_1;
                        a4_2 = a6;
                        a5_2 = a7;
                        a6_2 = a7_1;
                        a7_2 = a8_1;
                        a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                        intm0_2 = intm0+a0_2;
                        intm1_2 = intm0_1+a8_2;
                        intm2_2 = a3_2+a2_2;
                        intm3_2 = intm0_2+intm1_2;
                        intm4_2 = intm2_2+a5_2;
                        intm5_2 = intm3_2 + intm4_2;
                        intm6_2 = intm5_2*inv9;


                        A[(cur_i  )*(n)+cur_j] = intm8;
                        A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                        A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                    }

                    base_i = 1;
                    base_j = j+3;

                    // parallel execution of fourth (even-length) diagonal
                    num_steps_1 = num_steps*0.5;
                    #pragma omp for
                    for(int s = 0; s < num_steps_1; s++) {
                        
                        int cur_i = base_i + 2*s;
                        int cur_j = base_j - 4*s;
                        double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                        double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                        a0 = A[(cur_i-1)*n+(cur_j-1)];
                        a1 = A[(cur_i-1)*n+(cur_j)];
                        a2 = A[(cur_i-1)*n+(cur_j+1)];
                        a3 = A[(cur_i)*n+(cur_j-1)];
                        a4 = A[(cur_i)*n+(cur_j)];
                        a5 = A[(cur_i)*n+(cur_j+1)];
                        a6 = A[(cur_i+1)*n+(cur_j-1)];
                        a7 = A[(cur_i+1)*n+(cur_j)];
                        a8 = A[(cur_i+1)*n+(cur_j+1)];

                        intm0 = a0+a1;
                        intm1 = a2+a3;
                        intm2 = a5+a6;
                        intm3 = a7+a8;

                        intm5 = intm0+intm1;
                        intm6 = intm2+intm3;

                        intm7 = intm5+intm6;
                        intm8 = (intm7+a4)*inv9;

                        A[(cur_i  )*(n)+cur_j] = intm8; 
                    }
                
                    num_steps += 1;
                }

                // cleanup
                for (; j <= n - 2; j++)
                {
                    int base_i = 1;
                    int base_j = j;
                    num_steps = 1 + (j - 1)*0.5;

                    // parallel execution of diagonal
                    #pragma omp for
                    for(int s = 0; s < num_steps; s++) {
                        
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                        double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                        a0 = A[(cur_i-1)*n+(cur_j-1)];
                        a1 = A[(cur_i-1)*n+(cur_j)];
                        a2 = A[(cur_i-1)*n+(cur_j+1)];
                        a3 = A[(cur_i)*n+(cur_j-1)];
                        a4 = A[(cur_i)*n+(cur_j)];
                        a5 = A[(cur_i)*n+(cur_j+1)];
                        a6 = A[(cur_i+1)*n+(cur_j-1)];
                        a7 = A[(cur_i+1)*n+(cur_j)];
                        a8 = A[(cur_i+1)*n+(cur_j+1)];

                        intm0 = a0+a1;
                        intm1 = a2+a3;
                        intm2 = a5+a6;
                        intm3 = a7+a8;

                        intm5 = intm0+intm1;
                        intm6 = intm2+intm3;

                        intm7 = intm5+intm6;
                        intm8 = (intm7+a4)*inv9;

                        A[(cur_i  )*(n)+cur_j] = intm8; 
                    }
                }
            }
            

            // in case n is odd, one of the two diagonals starting on the same
            // row have different number of points to process
            if(n_odd) {
                #pragma omp parallel 
                {
                    double inv9 = 1.0/9.0;
                    int i = 2;
                    int num_steps = (n-2)*0.5;
                    int num_steps_1 = (n-2)*0.5+1 - (n-2)*0.25; 
                    int i_max = n*0.5;
                    if(full_mode_odd) {

                        for(; i <= i_max; i++) {

                            // first diagonal
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            // NOTE: if diagonals have unequal length, also clean up
                            // last element of second wave
                            #pragma omp single 
                            {
                                int s = num_steps - 1;
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8;  

                                cur_i += 1;
                                cur_j -= 1;   

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8;                                
                            }

                            // second diagonal
                            int num_steps_1 = 0.5*num_steps+1;
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            }     
                        }
                    } else {
                        for(; i <= i_max; i++) {

                            // parallel execution of first diagonal
                            // (also process half of next diagonal)
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }


                            // parallel execution of second diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            }     
                        }
                    } 


                    // no full utilization anymore, diagonals get smaller
                    i = n*0.5+1;
                    num_steps = (n-2-i);
                    
                    if(first_decreasing_odd_1) {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (odd-length) diagonal
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            #pragma omp for
                            for(int s = 0; s <= num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            #pragma omp single 
                            {
                                int s = num_steps;
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8;                                    
                            }

                            // parallel execution of second (odd-length) diagonal
                            num_steps_1 = 0.5*num_steps;
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            } 

                            // parallel execution of third (even-length) diagonal
                            // (also compute half of fourth even-length diagonal)
                            num_steps -= 1;
                            int num_steps_1 = num_steps*0.5;
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s <= num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            // // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            } 
                            num_steps -= 1;
                        }

                        
                    } else {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (even-length) diagonal
                            // (also compute half of second even-length diagonal)
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            int num_steps_1 = num_steps*0.5;
                            #pragma omp for
                            for(int s = 0; s <= num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            } 
                            num_steps -= 1;

                            // parallel execution of third (odd-length) diagonal
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s <= num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            #pragma omp single 
                            {
                                int s = num_steps;
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8;                                  
                            } 

                            // parallel execution of fourth (odd-length) diagonal
                            num_steps_1 = 0.5*num_steps;
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            }
                            num_steps -= 1; 
                        }
                    }
                    
                    // cleanup
                    for(; i <= n-2; i++) {

                        // first diagonal
                        int base_i = i;
                        int base_j_0 = n-3;
                        int base_j_1 = n-2;

                        #pragma omp for
                        for(int s = 0; s <= num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_0 - 2*s;
                            double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                            double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                            a0 = A[(cur_i-1)*n+(cur_j-1)];
                            a1 = A[(cur_i-1)*n+(cur_j)];
                            a2 = A[(cur_i-1)*n+(cur_j+1)];
                            a3 = A[(cur_i)*n+(cur_j-1)];
                            a4 = A[(cur_i)*n+(cur_j)];
                            a5 = A[(cur_i)*n+(cur_j+1)];
                            a6 = A[(cur_i+1)*n+(cur_j-1)];
                            a7 = A[(cur_i+1)*n+(cur_j)];
                            a8 = A[(cur_i+1)*n+(cur_j+1)];

                            intm0 = a0+a1;
                            intm1 = a2+a3;
                            intm2 = a5+a6;
                            intm3 = a7+a8;

                            intm5 = intm0+intm1;
                            intm6 = intm2+intm3;

                            intm7 = intm5+intm6;
                            intm8 = (intm7+a4)*inv9;

                            A[(cur_i  )*(n)+cur_j] = intm8; 
                        }  

                        // second diagonal
                        #pragma omp for
                        for(int s = 0; s <= num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_1 - 2*s;
                            double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                            double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                            a0 = A[(cur_i-1)*n+(cur_j-1)];
                            a1 = A[(cur_i-1)*n+(cur_j)];
                            a2 = A[(cur_i-1)*n+(cur_j+1)];
                            a3 = A[(cur_i)*n+(cur_j-1)];
                            a4 = A[(cur_i)*n+(cur_j)];
                            a5 = A[(cur_i)*n+(cur_j+1)];
                            a6 = A[(cur_i+1)*n+(cur_j-1)];
                            a7 = A[(cur_i+1)*n+(cur_j)];
                            a8 = A[(cur_i+1)*n+(cur_j+1)];

                            intm0 = a0+a1;
                            intm1 = a2+a3;
                            intm2 = a5+a6;
                            intm3 = a7+a8;

                            intm5 = intm0+intm1;
                            intm6 = intm2+intm3;

                            intm7 = intm5+intm6;
                            intm8 = (intm7+a4)*inv9;

                            A[(cur_i  )*(n)+cur_j] = intm8; 
                        } 
                        num_steps -= 1;
                    }
                }
                    

            // n is even, thus both diagonals on the same row have same number of elements to process
            } else {
                #pragma omp parallel 
                {
                    double inv9 = 1.0/9.0;
                    int i = 2;
                    int i_max = n*0.5;
                    int num_steps = (n-2)*0.5;
                    if(full_mode_odd) {
                        for(; i < i_max; i++) {

                            // parallel execution of first diagonal
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            #pragma omp single 
                            {
                                int s = num_steps - 1;
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8;                                    
                            }

                            // parallel execution of second diagonal
                            int num_steps_1 = num_steps*0.5;
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            }     
                        }
                    // in case the number of steps on the diagonal
                    // is divisible by 2, can again precompute
                    // parts of next diagonal without cleanup
                    } else {
                        int num_steps_1 = num_steps*0.5;
                        for(; i < i_max; i++) {

                            // parallel execution of first diagonal
                            // (also process half of next diagonal)
                            // parallel execution of first diagonal
                            // (also process half of next diagonal)
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            // parallel execution of second diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            }     
                        }                            
                    }

                

                    // diagonals become smaller
                    num_steps = 1 + (n-2-i);                    
                    if(first_decreasing_odd) {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (odd-length) diagonal
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            #pragma omp single 
                            {
                                int s = num_steps - 1;
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8;                                    
                            }


                            // parallel execution of second (odd-length) diagonal
                            int num_steps_1 = num_steps*0.5;
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            } 

                            // parallel execution of third (even-length) diagonal
                            // (also compute half of fourth even-length diagonal)
                            num_steps -= 1;
                            num_steps_1 = num_steps*0.5;
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            // // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            } 
                            num_steps -= 1;
                        }                    
                        
                        // cleanup loop
                        for(; i <= n-2; i++) {

                            // first diagonal
                            
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            #pragma omp for
                            for(int s = 0; s < num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            }  

                            // second diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_1 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            } 

                            num_steps -= 1;
                        }

                        
                    } else {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (even-length) diagonal
                            // (also compute half of second even-length diagonal)
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            int num_steps_1 = num_steps*0.5;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; 
                            } 
                            num_steps -= 1;

                            // parallel execution of third (odd-length) diagonal
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                double a0_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1;
                                double intm0_1, intm1_1, intm2_1, intm3_1, intm4_1, intm5_1, intm6_1, intm7_1, intm8_1;
                                double a0_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2;
                                double intm0_2, intm1_2, intm2_2, intm3_2, intm4_2, intm5_2, intm6_2, intm7_2, intm8_2;

                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                a0_1 = A[(cur_i_1-1)*n+(cur_j_1-1)];
                                a1_1 = A[(cur_i_1-1)*n+(cur_j_1)];
                                a2_1 = a3;
                                a3_1 = A[(cur_i_1)*n+(cur_j_1-1)];
                                a4_1 = A[(cur_i_1)*n+(cur_j_1)];
                                a5_1 = a6;
                                a6_1 = A[(cur_i_1+1)*n+(cur_j_1-1)];
                                a7_1 = A[(cur_i_1+1)*n+(cur_j_1)];
                                a8_1 = A[(cur_i_1+1)*n+(cur_j_1+1)];

                                intm0 = a3+a6;
                                intm1 = a4+a7;
                                intm2 = a0+a1;
                                intm3 = a2+a5;

                                intm4 = intm0+intm1;
                                intm5 = intm2+intm3;

                                intm6 = intm4+intm5;
                                intm7 = intm6+a8;
                                intm8 = intm7*inv9; 




                                intm0_1 = a7_1+a8_1;
                                intm1_1 = a0_1+a1_1;
                                intm2_1 = a3_1+a6_1;

                                intm3_1 = intm0_1+intm1_1;
                                intm4_1 = intm2_1+intm0;

                                intm5_1 = intm3_1+intm4_1;
                                intm6_1 = intm5_1+a4_1;
                                intm7_1 = intm6_1*inv9;

                                a0_2 = a1_1;
                                a1_2 = a2_1;
                                a2_2 = intm8;
                                a3_2 = intm7_1;
                                a4_2 = a6;
                                a5_2 = a7;
                                a6_2 = a7_1;
                                a7_2 = a8_1;
                                a8_2 = A[(cur_i_1+1)*n+(cur_j_2+1)];

                                intm0_2 = intm0+a0_2;
                                intm1_2 = intm0_1+a8_2;
                                intm2_2 = a3_2+a2_2;
                                intm3_2 = intm0_2+intm1_2;
                                intm4_2 = intm2_2+a5_2;
                                intm5_2 = intm3_2 + intm4_2;
                                intm6_2 = intm5_2*inv9;


                                A[(cur_i  )*(n)+cur_j] = intm8;
                                A[(cur_i_1  )*(n)+cur_j_1] = intm7_1;
                                A[(cur_i_1  )*(n)+cur_j_2] = intm6_2;
                            }

                            #pragma omp single 
                            {
                                int s = num_steps-1;
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8;                                     
                            } 

                            // parallel execution of fourth (odd-length) diagonal
                            num_steps_1 = 0.5*num_steps;
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                                double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                                a0 = A[(cur_i-1)*n+(cur_j-1)];
                                a1 = A[(cur_i-1)*n+(cur_j)];
                                a2 = A[(cur_i-1)*n+(cur_j+1)];
                                a3 = A[(cur_i)*n+(cur_j-1)];
                                a4 = A[(cur_i)*n+(cur_j)];
                                a5 = A[(cur_i)*n+(cur_j+1)];
                                a6 = A[(cur_i+1)*n+(cur_j-1)];
                                a7 = A[(cur_i+1)*n+(cur_j)];
                                a8 = A[(cur_i+1)*n+(cur_j+1)];

                                intm0 = a0+a1;
                                intm1 = a2+a3;
                                intm2 = a5+a6;
                                intm3 = a7+a8;

                                intm5 = intm0+intm1;
                                intm6 = intm2+intm3;

                                intm7 = intm5+intm6;
                                intm8 = (intm7+a4)*inv9;

                                A[(cur_i  )*(n)+cur_j] = intm8; ;
 
                            }
                            num_steps -= 1; 
                        }
                    }                  
                    
                    // cleanup loop
                    for(; i <= n-2; i++) {

                        // first diagonal
                        
                        int base_i = i;
                        int base_j_0 = n-3;
                        int base_j_1 = n-2;

                        #pragma omp for
                        for(int s = 0; s < num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_0 - 2*s;
                            double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                            double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                            a0 = A[(cur_i-1)*n+(cur_j-1)];
                            a1 = A[(cur_i-1)*n+(cur_j)];
                            a2 = A[(cur_i-1)*n+(cur_j+1)];
                            a3 = A[(cur_i)*n+(cur_j-1)];
                            a4 = A[(cur_i)*n+(cur_j)];
                            a5 = A[(cur_i)*n+(cur_j+1)];
                            a6 = A[(cur_i+1)*n+(cur_j-1)];
                            a7 = A[(cur_i+1)*n+(cur_j)];
                            a8 = A[(cur_i+1)*n+(cur_j+1)];

                            intm0 = a0+a1;
                            intm1 = a2+a3;
                            intm2 = a5+a6;
                            intm3 = a7+a8;

                            intm4 = a4*inv9;

                            intm5 = intm0+intm1;
                            intm6 = intm2+intm3;

                            intm7 = intm4+intm5*inv9;
                            intm8 = intm7+intm6*inv9;

                            A[(cur_i  )*(n)+cur_j] = intm8;
                        }  

                        // second diagonal
                        #pragma omp for
                        for(int s = 0; s < num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_1 - 2*s;
                            double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                            double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                            a0 = A[(cur_i-1)*n+(cur_j-1)];
                            a1 = A[(cur_i-1)*n+(cur_j)];
                            a2 = A[(cur_i-1)*n+(cur_j+1)];
                            a3 = A[(cur_i)*n+(cur_j-1)];
                            a4 = A[(cur_i)*n+(cur_j)];
                            a5 = A[(cur_i)*n+(cur_j+1)];
                            a6 = A[(cur_i+1)*n+(cur_j-1)];
                            a7 = A[(cur_i+1)*n+(cur_j)];
                            a8 = A[(cur_i+1)*n+(cur_j+1)];

                            intm0 = a0+a1;
                            intm1 = a2+a3;
                            intm2 = a5+a6;
                            intm3 = a7+a8;

                            intm4 = a4*inv9;

                            intm5 = intm0+intm1;
                            intm6 = intm2+intm3;

                            intm7 = intm4+intm5*inv9;
                            intm8 = intm7+intm6*inv9;

                            A[(cur_i  )*(n)+cur_j] = intm8;
                        } 
                        num_steps -= 1;
                    }
                }
            }
        }
        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
