#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>
#include "../../utils/common.h"
#include "../../utils/test.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_wave_singletime_handopt"

/* TODO
    strength reduction for %
    strength reduction for integer mults
    maybe unroll loops once

*/

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];

    parse_args(argc, argv, &n, &tsteps, &test);

    double *A = malloc(n*n*sizeof(double));

    init_array(n, A);

    int n_odd;
    int full_mode_odd;
    int first_decreasing_odd;
    int first_decreasing_odd_1;

    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        n_odd = n % 2;
        full_mode_odd = (int)(((n-2)*0.5)) % 2;
        first_decreasing_odd = (int)(1 + (n-2-n*0.5)) % 2;
        first_decreasing_odd_1 = (int)((n-2-n*0.5-1)) % 2;

        for (int t = 0; t <= tsteps - 1; t++)
        {

            // process upper triangular
            #pragma omp parallel
            {
                int j = 1;
                int num_steps = 1;
                for (; j <= n - 5; j+=4)
                {
                    int base_i = 1;
                    int base_j = j;

                    // parallel execution of first (odd-length) diagonal
                    #pragma omp for
                    for(int s = 0; s < num_steps; s++) {
                        
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }

                    base_i = 1;
                    base_j = j+1;

                    // parallel execution of second (odd-length) diagonal
                    #pragma omp for
                    for(int s = 0; s < num_steps; s++) {
                        
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }
                    
                    num_steps += 1;

                    base_i = 1;
                    base_j = j+2;

                    // parallel execution of third (even-length) diagonal
                    // NOTE: we can already process one element of the next diagonal
                    // and thus reduce the nr of iterations of fourth diagonal
                    #pragma omp for
                    for(int s = 0; s < num_steps-1; s+=2) {
                        
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        int cur_i_1 = base_i + (s+1);
                        int cur_j_1 = base_j - 2*(s+1);
                        int cur_j_2 = base_j - 2*(s+1)+1;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                        A[(cur_i_1  )*(n)+cur_j_1] = (A[(cur_i_1-1)*n+(cur_j_1-1)] + A[(cur_i_1-1)*n+(cur_j_1)] + A[(cur_i_1-1)*n+(cur_j_1+1)]
                                        + A[(cur_i_1  )*n+(cur_j_1-1)] + A[(cur_i_1  )*n+(cur_j_1)] + A[(cur_i_1  )*n+(cur_j_1+1)]
                                        + A[(cur_i_1+1)*n+(cur_j_1-1)] + A[(cur_i_1+1)*n+(cur_j_1)] + A[(cur_i_1+1)*n+(cur_j_1+1)])/9.0;
                        A[(cur_i_1  )*(n)+cur_j_2] = (A[(cur_i_1-1)*n+(cur_j_2-1)] + A[(cur_i_1-1)*n+(cur_j_2)] + A[(cur_i_1-1)*n+(cur_j_2+1)]
                                        + A[(cur_i_1  )*n+(cur_j_2-1)] + A[(cur_i_1  )*n+(cur_j_2)] + A[(cur_i_1  )*n+(cur_j_2+1)]
                                        + A[(cur_i_1+1)*n+(cur_j_2-1)] + A[(cur_i_1+1)*n+(cur_j_2)] + A[(cur_i_1+1)*n+(cur_j_2+1)])/9.0;
                    }

                    base_i = 1;
                    base_j = j+3;

                    // parallel execution of fourth (even-length) diagonal
                    int num_steps_1 = num_steps*0.5;
                    #pragma omp for
                    for(int s = 0; s < num_steps_1; s++) {
                        
                        int cur_i = base_i + 2*s;
                        int cur_j = base_j - 4*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }
                
                    num_steps += 1;
                }

                
                for (; j <= n - 2; j++)
                {
                    int base_i = 1;
                    int base_j = j;
                    num_steps = 1 + (j - 1)*0.5;

                    // parallel execution of diagonal
                    #pragma omp for
                    for(int s = 0; s < num_steps; s++) {
                        
                        int cur_i = base_i + s;
                        int cur_j = base_j - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }
                }
            }
            

            // in case n is odd, one of the two diagonals starting on the same
            // row have different number of points to process
            if(n_odd) {
                #pragma omp parallel 
                {
                    int i = 2;
                    int num_steps = (n-2)*0.5;
                    int num_steps_1 = (n-2)*0.5+1 - (n-2)*0.25; 
                    int i_max = n*0.5;
                    if(full_mode_odd) {

                        // TODO: optimize this case
                        for(; i <= i_max; i++) {

                            // first diagonal
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            #pragma omp for
                            for(int s = 0; s < num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }

                            // second diagonal
                            #pragma omp for
                            for(int s = 0; s <= num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_1 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }     
                        }
                    } else {

                        for(; i <= i_max; i++) {

                            // parallel execution of first diagonal
                            // (also process half of next diagonal)
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_1] = (A[(cur_i_1-1)*n+(cur_j_1-1)] + A[(cur_i_1-1)*n+(cur_j_1)] + A[(cur_i_1-1)*n+(cur_j_1+1)]
                                                + A[(cur_i_1  )*n+(cur_j_1-1)] + A[(cur_i_1  )*n+(cur_j_1)] + A[(cur_i_1  )*n+(cur_j_1+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_1-1)] + A[(cur_i_1+1)*n+(cur_j_1)] + A[(cur_i_1+1)*n+(cur_j_1+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_2] = (A[(cur_i_1-1)*n+(cur_j_2-1)] + A[(cur_i_1-1)*n+(cur_j_2)] + A[(cur_i_1-1)*n+(cur_j_2+1)]
                                                + A[(cur_i_1  )*n+(cur_j_2-1)] + A[(cur_i_1  )*n+(cur_j_2)] + A[(cur_i_1  )*n+(cur_j_2+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_2-1)] + A[(cur_i_1+1)*n+(cur_j_2)] + A[(cur_i_1+1)*n+(cur_j_2+1)])/9.0;
                            }


                            // parallel execution of second diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }     
                        }
                    }


                    // INSERT OPTIMIZATION HERE 

                    // no full utilization anymore, diagonals get smaller
                    i = n*0.5+1;
                    num_steps = (n-2-i);
                    
                    if(first_decreasing_odd_1) {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (odd-length) diagonal
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            #pragma omp for
                            for(int s = 0; s <= num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }  

                            // parallel execution of second (odd-length) diagonal
                            #pragma omp for
                            for(int s = 0; s <= num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_1 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            } 

                            // parallel execution of third (even-length) diagonal
                            // (also compute half of fourth even-length diagonal)
                            num_steps -= 1;
                            int num_steps_1 = num_steps*0.5;
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s <= num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_1] = (A[(cur_i_1-1)*n+(cur_j_1-1)] + A[(cur_i_1-1)*n+(cur_j_1)] + A[(cur_i_1-1)*n+(cur_j_1+1)]
                                                + A[(cur_i_1  )*n+(cur_j_1-1)] + A[(cur_i_1  )*n+(cur_j_1)] + A[(cur_i_1  )*n+(cur_j_1+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_1-1)] + A[(cur_i_1+1)*n+(cur_j_1)] + A[(cur_i_1+1)*n+(cur_j_1+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_2] = (A[(cur_i_1-1)*n+(cur_j_2-1)] + A[(cur_i_1-1)*n+(cur_j_2)] + A[(cur_i_1-1)*n+(cur_j_2+1)]
                                                + A[(cur_i_1  )*n+(cur_j_2-1)] + A[(cur_i_1  )*n+(cur_j_2)] + A[(cur_i_1  )*n+(cur_j_2+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_2-1)] + A[(cur_i_1+1)*n+(cur_j_2)] + A[(cur_i_1+1)*n+(cur_j_2+1)])/9.0;
                            }

                            // // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            } 
                            num_steps -= 1;
                        }

                        
                    } else {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (even-length) diagonal
                            // (also compute half of second even-length diagonal)
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            int num_steps_1 = num_steps*0.5;
                            #pragma omp for
                            for(int s = 0; s <= num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_1] = (A[(cur_i_1-1)*n+(cur_j_1-1)] + A[(cur_i_1-1)*n+(cur_j_1)] + A[(cur_i_1-1)*n+(cur_j_1+1)]
                                                + A[(cur_i_1  )*n+(cur_j_1-1)] + A[(cur_i_1  )*n+(cur_j_1)] + A[(cur_i_1  )*n+(cur_j_1+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_1-1)] + A[(cur_i_1+1)*n+(cur_j_1)] + A[(cur_i_1+1)*n+(cur_j_1+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_2] = (A[(cur_i_1-1)*n+(cur_j_2-1)] + A[(cur_i_1-1)*n+(cur_j_2)] + A[(cur_i_1-1)*n+(cur_j_2+1)]
                                                + A[(cur_i_1  )*n+(cur_j_2-1)] + A[(cur_i_1  )*n+(cur_j_2)] + A[(cur_i_1  )*n+(cur_j_2+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_2-1)] + A[(cur_i_1+1)*n+(cur_j_2)] + A[(cur_i_1+1)*n+(cur_j_2+1)])/9.0;
                            }

                            // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s <= num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            } 
                            num_steps -= 1;

                            // parallel execution of third (odd-length) diagonal
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s <= num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }  

                            // parallel execution of fourth (odd-length) diagonal
                            #pragma omp for
                            for(int s = 0; s <= num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_1 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }
                            num_steps -= 1; 
                        }
                    }
                    
                    
                    for(; i <= n-2; i++) {

                        // first diagonal
                        int base_i = i;
                        int base_j_0 = n-3;
                        int base_j_1 = n-2;

                        #pragma omp for
                        for(int s = 0; s <= num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_0 - 2*s;
                            A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                            + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                            + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                        }  

                        // second diagonal
                        #pragma omp for
                        for(int s = 0; s <= num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_1 - 2*s;
                            A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                            + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                            + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                        } 
                        num_steps -= 1;
                    }
                }
                    

            // n is even, thus both diagonals on the same row have same number of elements to process
            } else {
                #pragma omp parallel 
                {
                    int i = 2;
                    int i_max = n*0.5;
                    int num_steps = (n-2)*0.5;
                        if(full_mode_odd) {
                            for(; i < i_max; i++) {

                                // parallel execution of first diagonal
                                int base_i = i;
                                int base_j_0 = n-3;
                                int base_j_1 = n-2;
                                #pragma omp for
                                for(int s = 0; s < num_steps; s++) {
                                    int cur_i = base_i + s;
                                    int cur_j = base_j_0 - 2*s;
                                    A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                    + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                    + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;

                                }

                                // parallel execution of second diagonal
                                #pragma omp for
                                for(int s = 0; s < num_steps; s++) {
                                    int cur_i = base_i + s;
                                    int cur_j = base_j_1 - 2*s;
                                    A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                    + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                    + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                }     
                            }
                        // in case the number of steps on the diagonal
                        // is divisible by 2, can again precompute
                        // parts of next diagonal
                        } else {
                            int num_steps_1 = num_steps*0.5;
                            for(; i < i_max; i++) {

                                // parallel execution of first diagonal
                                // (also process half of next diagonal)
                                int base_i = i;
                                int base_j_0 = n-3;
                                int base_j_1 = n-2;
                                #pragma omp for
                                for(int s = 0; s < num_steps-1; s+=2) {
                                    int cur_i = base_i + s;
                                    int cur_j = base_j_0 - 2*s;
                                    int cur_i_1 = base_i + (s+1);
                                    int cur_j_1 = base_j_0 - 2*(s+1);
                                    int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                    A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                    + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                    + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                    A[(cur_i_1  )*(n)+cur_j_1] = (A[(cur_i_1-1)*n+(cur_j_1-1)] + A[(cur_i_1-1)*n+(cur_j_1)] + A[(cur_i_1-1)*n+(cur_j_1+1)]
                                                    + A[(cur_i_1  )*n+(cur_j_1-1)] + A[(cur_i_1  )*n+(cur_j_1)] + A[(cur_i_1  )*n+(cur_j_1+1)]
                                                    + A[(cur_i_1+1)*n+(cur_j_1-1)] + A[(cur_i_1+1)*n+(cur_j_1)] + A[(cur_i_1+1)*n+(cur_j_1+1)])/9.0;
                                    A[(cur_i_1  )*(n)+cur_j_2] = (A[(cur_i_1-1)*n+(cur_j_2-1)] + A[(cur_i_1-1)*n+(cur_j_2)] + A[(cur_i_1-1)*n+(cur_j_2+1)]
                                                    + A[(cur_i_1  )*n+(cur_j_2-1)] + A[(cur_i_1  )*n+(cur_j_2)] + A[(cur_i_1  )*n+(cur_j_2+1)]
                                                    + A[(cur_i_1+1)*n+(cur_j_2-1)] + A[(cur_i_1+1)*n+(cur_j_2)] + A[(cur_i_1+1)*n+(cur_j_2+1)])/9.0;
                                }

                                // parallel execution of second diagonal
                                #pragma omp for
                                for(int s = 0; s < num_steps_1; s++) {
                                    int cur_i = base_i + 2*s;
                                    int cur_j = base_j_1 - 4*s;
                                    A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                    + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                    + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                }     
                            }                            
                        }

                

                    // diagonals become smaller
                    num_steps = 1 + (n-2-i);                    
                    if(first_decreasing_odd) {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (odd-length) diagonal
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            #pragma omp for
                            for(int s = 0; s < num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }  

                            // parallel execution of second (odd-length) diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_1 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            } 

                            // parallel execution of third (even-length) diagonal
                            // (also compute half of fourth even-length diagonal)
                            num_steps -= 1;
                            int num_steps_1 = num_steps*0.5;
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_1] = (A[(cur_i_1-1)*n+(cur_j_1-1)] + A[(cur_i_1-1)*n+(cur_j_1)] + A[(cur_i_1-1)*n+(cur_j_1+1)]
                                                + A[(cur_i_1  )*n+(cur_j_1-1)] + A[(cur_i_1  )*n+(cur_j_1)] + A[(cur_i_1  )*n+(cur_j_1+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_1-1)] + A[(cur_i_1+1)*n+(cur_j_1)] + A[(cur_i_1+1)*n+(cur_j_1+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_2] = (A[(cur_i_1-1)*n+(cur_j_2-1)] + A[(cur_i_1-1)*n+(cur_j_2)] + A[(cur_i_1-1)*n+(cur_j_2+1)]
                                                + A[(cur_i_1  )*n+(cur_j_2-1)] + A[(cur_i_1  )*n+(cur_j_2)] + A[(cur_i_1  )*n+(cur_j_2+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_2-1)] + A[(cur_i_1+1)*n+(cur_j_2)] + A[(cur_i_1+1)*n+(cur_j_2+1)])/9.0;
                            }

                            // // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            } 
                            num_steps -= 1;
                        }

                        
                    } else {
                        for(; i <= n-3; i+=2) {

                            // parallel execution of first (even-length) diagonal
                            // (also compute half of second even-length diagonal)
                            int base_i = i;
                            int base_j_0 = n-3;
                            int base_j_1 = n-2;

                            int num_steps_1 = num_steps*0.5;
                            #pragma omp for
                            for(int s = 0; s < num_steps-1; s+=2) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                int cur_i_1 = base_i + (s+1);
                                int cur_j_1 = base_j_0 - 2*(s+1);
                                int cur_j_2 = base_j_0 - 2*(s+1)+1;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_1] = (A[(cur_i_1-1)*n+(cur_j_1-1)] + A[(cur_i_1-1)*n+(cur_j_1)] + A[(cur_i_1-1)*n+(cur_j_1+1)]
                                                + A[(cur_i_1  )*n+(cur_j_1-1)] + A[(cur_i_1  )*n+(cur_j_1)] + A[(cur_i_1  )*n+(cur_j_1+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_1-1)] + A[(cur_i_1+1)*n+(cur_j_1)] + A[(cur_i_1+1)*n+(cur_j_1+1)])/9.0;
                                A[(cur_i_1  )*(n)+cur_j_2] = (A[(cur_i_1-1)*n+(cur_j_2-1)] + A[(cur_i_1-1)*n+(cur_j_2)] + A[(cur_i_1-1)*n+(cur_j_2+1)]
                                                + A[(cur_i_1  )*n+(cur_j_2-1)] + A[(cur_i_1  )*n+(cur_j_2)] + A[(cur_i_1  )*n+(cur_j_2+1)]
                                                + A[(cur_i_1+1)*n+(cur_j_2-1)] + A[(cur_i_1+1)*n+(cur_j_2)] + A[(cur_i_1+1)*n+(cur_j_2+1)])/9.0;
                            }

                            // parallel execution of second (even-length) diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps_1; s++) {
                                int cur_i = base_i + 2*s;
                                int cur_j = base_j_1 - 4*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            } 
                            num_steps -= 1;

                            // parallel execution of third (odd-length) diagonal
                            base_i = i+1;
                            #pragma omp for
                            for(int s = 0; s < num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_0 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }  

                            // parallel execution of fourth (odd-length) diagonal
                            #pragma omp for
                            for(int s = 0; s < num_steps; s++) {
                                int cur_i = base_i + s;
                                int cur_j = base_j_1 - 2*s;
                                A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                                + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                                + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                            }
                            num_steps -= 1; 
                        }
                    }
                    
                    // cleanup loop
                    for(; i <= n-2; i++) {

                        // first diagonal
                        
                        int base_i = i;
                        int base_j_0 = n-3;
                        int base_j_1 = n-2;

                        #pragma omp for
                        for(int s = 0; s < num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_0 - 2*s;
                            A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                            + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                            + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                        }  

                        // second diagonal
                        #pragma omp for
                        for(int s = 0; s < num_steps; s++) {
                            int cur_i = base_i + s;
                            int cur_j = base_j_1 - 2*s;
                            A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                            + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                            + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                        } 
                        num_steps -= 1;
                    }
                }
            }
        }
        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
