#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>
#include "../../utils/common.h"
#include "../../utils/test.h"

#define FMAX(a, b) ((a) > (b) ? (a) : (b))
#define FMIN(a, b) ((a) < (b) ? (a) : (b))
#define FFLOOR(a) ((int)((a) + 268435456.) - 268435456)
#define FCEIL(a) (268435456 - (int)(268435456. - (a)))

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_wave_singletime_auto"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];


    parse_args(argc, argv, &n, &tsteps, &test);
    double *A = malloc(n*n*sizeof(double));
    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        // Inlined from trmm.c to eliminate method call overhead
        
        for (int t = 0; t <= tsteps - 1; t++)
        {

            #pragma omp parallel 
            {
                double inv9 = 1.0 / 9.0;
                for(int new_i = 3; new_i <= 3 * (n - 2); new_i++) {
                    int new_j_lb = FCEIL(FMAX(1, (1.0 / 2.0) * (-n + new_i + 2)));
                    int new_j_ub = FFLOOR(FMIN(n - 2, (1.0 / 2.0) * (new_i - 1)));
                    #pragma omp for
                    for(int new_j = new_j_lb; new_j <= new_j_ub; new_j++) {
                                                int i = new_j;
                        int j = new_i - 2 * new_j;

                        double a0, a1, a2, a3, a4, a5, a6, a7, a8;
                        double intm0, intm1, intm2, intm3, intm4, intm5, intm6, intm7, intm8;
                        a0 = A[(i-1)*n+(j-1)];
                        a1 = A[(i-1)*n+(j)];
                        a2 = A[(i-1)*n+(j+1)];
                        a3 = A[(i)*n+(j-1)];
                        a4 = A[(i)*n+(j)];
                        a5 = A[(i)*n+(j+1)];
                        a6 = A[(i+1)*n+(j-1)];
                        a7 = A[(i+1)*n+(j)];
                        a8 = A[(i+1)*n+(j+1)];

                        intm0 = a0+a1;
                        intm1 = a2+a3;
                        intm2 = a5+a6;
                        intm3 = a7+a8;

                        intm5 = intm0+intm1;
                        intm6 = intm2+intm3;

                        intm7 = intm5+intm6+a4;
                        intm8 = intm7*inv9;

                        A[(i  )*(n)+j] = intm8;               
                    }
                }
            }
        }
        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
