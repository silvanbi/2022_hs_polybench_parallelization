#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>
#include "../../utils/common.h"
#include "../../utils/test.h"

#define FMAX(a, b) ((a) > (b) ? (a) : (b))
#define FMIN(a, b) ((a) < (b) ? (a) : (b))
#define FFLOOR(a) ((int)((a) + 268435456.) - 268435456)
#define FCEIL(a) (268435456 - (int)(268435456. - (a)))

#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_wave_singletime_auto"

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    double start;
    double times[NRUNS];


    parse_args(argc, argv, &n, &tsteps, &test);
    double *A = malloc(n*n*sizeof(double));
    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        start = omp_get_wtime();

        // Inlined from trmm.c to eliminate method call overhead
        for (int t = 0; t <= tsteps - 1; t++)
        {

            #pragma omp parallel 
            {
                for(int new_i = 3; new_i <= 3 * (n - 2); new_i++) {
                    int new_j_lb = FCEIL(FMAX(1, (1.0 / 2.0) * (-n + new_i + 2)));
                    int new_j_ub = FFLOOR(FMIN(n - 2, (1.0 / 2.0) * (new_i - 1)));
                    #pragma omp for
                    for(int new_j = new_j_lb; new_j <= new_j_ub; new_j++) {
                        int i = new_j;
                        int j = new_i - 2 * new_j;
                        A[(i  )*(n)+j] = (A[(i-1)*n+(j-1)] + A[(i-1)*n+(j)] + A[(i-1)*n+(j+1)]
                                        + A[(i  )*n+(j-1)] + A[(i  )*n+(j)] + A[(i  )*n+(j+1)]
                                        + A[(i+1)*n+(j-1)] + A[(i+1)*n+(j)] + A[(i+1)*n+(j+1)])/9.0;                }
                }
            }
        }
        times[r] = omp_get_wtime() - start;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
