#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "../../utils/common.h"
#include "../../utils/test.h"


#ifndef NRUNS
#define NRUNS 1
#endif

#define PGM_NAME "omp_new_wavefront_v1_1"

/* TODO
    strength reduction for %
    strength reduction for integer mults
    maybe unroll loops once

*/

int main(int argc, char** argv)
{
    int test = 0;
    int n;
    int tsteps;
    struct timeval start;
    struct timeval end;
    double times[NRUNS];
    int n_odd;


    parse_args(argc, argv, &n, &tsteps, &test);
    double *A = malloc(n*n*sizeof(double));
    init_array(n, A);

    for (int r  = 0; r < NRUNS; r++) {
        gettimeofday(&start, NULL);

        n_odd = n % 2;

        for (int t = 0; t <= tsteps - 1; t++)
        {

            // process upper triangular
            int j = 1;
            int num_steps = 1;
            for (; j <= n - 3; j+=2)
            {
                int base_i = 1;
                int base_j = j;

                // parallel execution of diagonal
                #pragma omp parallel for
                for(int s = 0; s < num_steps; s++) {
                    
                    int cur_i = base_i + s;
                    int cur_j = base_j - 2*s;
                    A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                    + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                    + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                }

                base_i = 1;
                base_j = j+1;

                // parallel execution of diagonal
                #pragma omp parallel for
                for(int s = 0; s < num_steps; s++) {
                    
                    int cur_i = base_i + s;
                    int cur_j = base_j - 2*s;
                    A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                    + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                    + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                }
                num_steps += 1;

            }
            
            for (; j <= n - 2; j++)
            {
                int num_steps = 1 + (j - 1)*0.5;
                int base_i = 1;
                int base_j = j;

                // parallel execution of diagonal
                #pragma omp parallel for
                for(int s = 0; s < num_steps; s++) {
                    
                    int cur_i = base_i + s;
                    int cur_j = base_j - 2*s;
                    A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                    + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                    + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                }

            }
            

            // in case n is odd, one of the two diagonals starting on the same
            // row have different number of points to process
            if(n_odd) {
                int i = 2;
                int num_steps = (n-2)*0.5;

                for(; i <= n*0.5; i++) {

                    // first diagonal
                    int base_i = i;
                    int base_j_0 = n-3;
                    int base_j_1 = n-2;

                    #pragma omp parallel for
                    for(int s = 0; s < num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_0 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }

                    // second diagonal
                    #pragma omp parallel for
                    for(int s = 0; s <= num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_1 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }     
                }

                // no full utilization anymore, diagonals get smaller
                num_steps = (n-2-i);
                for(; i <= n-2; i++) {

                    // first diagonal
                    int base_i = i;
                    int base_j_0 = n-3;
                    int base_j_1 = n-2;

                    #pragma omp parallel for
                    for(int s = 0; s <= num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_0 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }  

                    // second diagonal
                    #pragma omp parallel for
                    for(int s = 0; s <= num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_1 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    } 
                    num_steps -= 1;
                }

            // n is even, thus both diagonals on the same row have same number of elements to process
            } else {
                int i = 2;
                int i_max = n*0.5;
                int num_steps = (n-2)*0.5;
                for(; i < i_max; i++) {

                    // first diagonal
                    
                    int base_i = i;
                    int base_j_0 = n-3;
                    int base_j_1 = n-2;
                    #pragma omp parallel for
                    for(int s = 0; s < num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_0 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }

                    // second diagonal
                    #pragma omp parallel for
                    for(int s = 0; s < num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_1 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }     
                }

                // diagonals become smaller
                num_steps = 1 + (n-2-i);
                for(; i <= n-2; i++) {

                    // first diagonal
                    
                    int base_i = i;
                    int base_j_0 = n-3;
                    int base_j_1 = n-2;

                    #pragma omp parallel for
                    for(int s = 0; s < num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_0 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    }  

                    // second diagonal
                    #pragma omp parallel for
                    for(int s = 0; s < num_steps; s++) {
                        int cur_i = base_i + s;
                        int cur_j = base_j_1 - 2*s;
                        A[(cur_i  )*(n)+cur_j] = (A[(cur_i-1)*n+(cur_j-1)] + A[(cur_i-1)*n+(cur_j)] + A[(cur_i-1)*n+(cur_j+1)]
                                        + A[(cur_i  )*n+(cur_j-1)] + A[(cur_i  )*n+(cur_j)] + A[(cur_i  )*n+(cur_j+1)]
                                        + A[(cur_i+1)*n+(cur_j-1)] + A[(cur_i+1)*n+(cur_j)] + A[(cur_i+1)*n+(cur_j+1)])/9.0;
                    } 
                num_steps -= 1;
                }
            }
        }
        gettimeofday(&end, NULL);
        times[r] = (end.tv_sec  - start.tv_sec) + (end.tv_usec - start.tv_usec) * 1.e-6;
    }

    if (test)
        run_test(n, tsteps, A);

    printf("%s\n", PGM_NAME);
    print_times(times, NRUNS, 0);

    free(A);
    return 0;
}
