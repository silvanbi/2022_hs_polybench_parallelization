#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

void init_array(int n, double* A) {
  srand(42);
  for (int i = 0; i < n; i++)
  {
    for (int j = 0; j < n; j++)
    {
      A[i*n+j] = ((double)rand())/RAND_MAX;
    }
  }
}

void print_array(int n, double* A) {
  for (int i = 0; i < n; i++)
  {
    for (int j = 0; j < n; j++)
    {
      printf("%0.2lf ", A[i*n+j]);
    }
    printf("\n");
  }
  printf("\n");
  printf("\n");
  printf("\n");
}

void print_times(double* times, int nruns, int rank) {
  printf("Process %d: ", rank);
  for (int i = 0; i < nruns; i++) {
    if (i < nruns-1)
      printf("%lf, ", times[i]);
    else
      printf("%lf", times[i]);
  }
  printf("\n");
}

void parse_args(int argc, char** argv, int* n, int* tsteps, int* run_tests) {
  if (argc == 4 && strcmp(argv[1], "-t") == 0) {
    *run_tests = 1;
    *n = atoi(argv[2]);
    *tsteps = atoi(argv[3]);
    if (*n == 0) {
      printf("Invalid 'n' supplied (must be an integer number)\n");
      exit(1);
    }
    if (*tsteps == 0) {
      printf("Invalid 'tsteps' supplied (must be an integer number)\n");
      exit(1);
    }
  }
  else if (argc == 3) {
    *n = atoi(argv[1]);
    *tsteps = atoi(argv[2]);
    if (*n == 0) {
      printf("Invalid 'n' supplied (must be an integer number)\n");
      exit(1);
    }
    if (*tsteps == 0) {
      printf("Invalid 'tsteps' supplied (must be an integer number)\n");
      exit(1);
    }
  }
  else {
    printf("Invalid arguments - correct usage:\n");
    printf("  ./program -t <n> <tsteps>    [runs benchmark and test]\n");
    printf("  ./program <n> <tsteps>       [runs benchmark]\n");
    exit(1);
  }
}
