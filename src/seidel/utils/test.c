#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "test.h"
#include "common.h"
#include "../original/seidel.h"

void assert_matrices(int n, double* A, double* B, double EPS) {
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      // Note: precision problems may arise - consider adding epsilon
      if (fabs(A[i*n+j] - B[i*n+j]) < EPS)
        continue;
      printf("Assert FAIL - expected: %0.2lf, was: %0.2lf (at: [%d, %d])\n", A[i*n+j], B[i*n+j], i, j);
      printf("Expected matrix: \n");
      print_array(n, A);
      printf("\n");
      printf("Actual matrix was: \n");
      print_array(n, B);
      exit(1);
    }
  }
}

void run_test(int n, int tsteps, double* A) {
  printf("TEST: ");
  double* real_A = (double*)malloc(n * n * sizeof(double));
  init_array(n, real_A);
  kernel_seidel_2d(n, tsteps, real_A);
  assert_matrices(n, real_A, A, 1E-10);
  free(real_A);
  printf("Passed\n");
}
