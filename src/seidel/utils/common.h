#pragma once

void init_array(int n, double* A);
void print_array(int n, double* A);
void print_times(double* times, int nruns, int rank);

void parse_args(int argc, char** argv, int* n, int* tsteps, int* run_tests);
