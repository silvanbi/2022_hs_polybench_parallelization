import utils
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.ticker import FormatStrFormatter

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["10000_40_1.txt", "10000_40_2.txt", "10000_40_4.txt", "10000_40_8.txt", "10000_40_16.txt", "10000_40_32.txt", "10000_40_64.txt", "10000_40_128.txt"]
    files2 = ["10000_40_1.txt", "10000_40_2.txt", "10000_40_4.txt", "10000_40_8.txt", "10000_40_16.txt", "10000_40_32.txt"]
    
    threads = [int(file.split(".")[0].split("_")[2]) for file in files]
    version = "mid_presentation/euler"

    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/sequential_optimized", ["10000_40_1.txt"], confidence, median_percent)
    omp_par_seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_parallelize_seq_opt/2022-12-15-17-46-52", files2, confidence, median_percent)
    omp_singletime = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_singletime/2022-12-14-18-21-14", files, confidence, median_percent)
    omp_multitime = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_multitime_better_ssa_unroll2/2022-12-14-18-21-28", files, confidence, median_percent)
    print("\nRaw runtime data:")
    print(seq_opt)
    print(omp_par_seq_opt)
    print(omp_singletime)
    print(omp_multitime)

    baseline = seq_opt[0]
    omp_par_seq_opt = baseline / np.array(omp_par_seq_opt)
    omp_singletime = baseline / np.array(omp_singletime)
    omp_multitime = baseline / np.array(omp_multitime)

    print("\nRaw speedup data:")
    print(omp_par_seq_opt)
    print(omp_singletime)
    print(omp_multitime)

    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Speedup plot of Seidel-2D OpenMP versions\n(N = 10000, T = 40)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Speedup [x]", color="black")

    ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    ax1.set_xscale("log", base=2)
    ax1.set_yscale("log", base=2)
    ax1.set_xticks(2 ** np.arange(0, 7))
    ax1.set_yticks(2.0 ** (np.arange(-2, 8)))
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax1.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)

    ax1.plot(2 ** np.arange(0, 8), 2 ** np.arange(0, 8), color="black", linestyle="-", linewidth=1.0, label="Linear speedup")
    ax1.plot(threads[:-2], omp_par_seq_opt, color="saddlebrown", mfc="saddlebrown", mec="saddlebrown", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP sequential opt")
    ax1.plot(threads, omp_singletime, color="darkorange", mfc="darkorange", mec="darkorange", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP single time")
    ax1.plot(threads, omp_multitime, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP multi time")

    ax1.legend()

    plt.xticks(threads)

    plt.savefig("seidel_omp_speedup.png")

