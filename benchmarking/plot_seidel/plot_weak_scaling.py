import utils
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
import numpy as np
import math

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    initial_n = 4000
    n = initial_n
    t = 80
    n_steps = 7
    n_threads = 1
    files = ["{}_{}_{}.txt".format(n, t, 1)]
    ns = [n]
    threads = [1]
    for i in range(n_steps):
        n_threads *= 2
        n = math.ceil(n*math.sqrt(2))
        ns.append(n)
        threads.append(n_threads)
        files.append("{}_{}_{}.txt".format(n, t, n_threads))


    seq_pb = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/seq_original/2023-01-11-16-05-16", ["{}_{}_{}.txt".format(initial_n,t,1)], confidence, median_percent)
    seq_pb_label = "Base PolyBench"   
    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/seq_optimized/2023-01-11-16-05-17", ["{}_{}_{}.txt".format(initial_n,t,1)], confidence, median_percent)
    seq_opt_label = "Base SO"
    omp_best_multitime = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/omp_wave_multitime_better_ssa_unroll2/2023-01-11-16-06-15", files, confidence, median_percent)
    omp_best_multitime_label = "OpenMP MT"
    omp_best_singletime_auto = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/omp_wave_singletime_auto_ssa/2023-01-11-16-06-36", files, confidence, median_percent)
    omp_best_singletime_auto_label = "OpenMP ST"

    mpi_opt_serial = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/mpi_insiderank_optimisedserial/2023-01-11-17-07-14", files, confidence, median_percent)
    mpi_opt_serial_label = "MPI w/ SO"

    mpi_opt = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/mpi_insiderank_serial/2023-01-11-17-07-07", files[:-4]+files[-2:], confidence, median_percent)
    mpi_opt_label = "MPI"

    # Plot
    baseline = seq_opt[0]
    baseline_str = seq_opt_label
    if omp_best_multitime[0] < baseline:
        baseline = omp_best_multitime[0]
        baseline_str = omp_best_multitime_label
    if omp_best_singletime_auto[0] < baseline:
        baseline = omp_best_singletime_auto[0]
        baseline_str = omp_best_singletime_auto_label
    if mpi_opt_serial[0] < baseline:
        baseline = mpi_opt_serial[0]
        baseline_str = mpi_opt_serial_label

    if mpi_opt[0] < baseline:
        baseline = mpi_opt[0]
        baseline_str = mpi_opt_label

    seq_pb = (baseline / seq_pb)*100
    seq_opt = (baseline / seq_opt)*100
    omp_best_multitime = (baseline / omp_best_multitime)*100
    omp_best_singletime_auto = (baseline / omp_best_singletime_auto)*100 
    mpi_opt_serial = (baseline / mpi_opt_serial)*100 
    mpi_opt = (baseline / mpi_opt)*100 



    fig1, ax1 = plt.subplots(figsize=(7, 4))
    ax1.set_title("Weak Scaling Efficiency - Euler \n(T = {}, Baseline: {})".format(t, baseline_str))

    ax1.set_xlabel("Input size (N)", color="black")
    ax1.set_ylabel("Efficiency [%]", color="black")
    ax1.set_xscale('log', base=2)
    ax1.xaxis.set_major_formatter(tick.FormatStrFormatter("%d"))
    ax1.set_facecolor('#EBEBEB')
       
    ax1.grid(axis="y", color="white", linewidth=1)
    ax1.plot(ns[0], seq_pb, color="red", mfc="red", mec="red", linestyle=":", linewidth=0.8, marker="o", markersize=6, label=seq_pb_label, zorder=10)
    ax1.plot(ns[0], seq_opt, color="red", mfc="red", mec="red", linestyle=":", linewidth=0.8, marker="^", markersize=6, label=seq_opt_label, zorder = 10)

    ax1.plot(ns, omp_best_multitime, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=0.8, marker="o", markersize=6, label=omp_best_multitime_label)
    ax1.plot(ns, omp_best_singletime_auto, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=0.8, marker="^", markersize=6, label=omp_best_singletime_auto_label)
    ax1.plot(ns, mpi_opt_serial, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="o", markersize=6, label=mpi_opt_serial_label)
    
    ax1.plot(ns[:-4]+ns[-2:], mpi_opt, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="^", markersize=6, label=mpi_opt_label)

    ax1.legend()

    plt.xticks(ns)
    plt.show()

