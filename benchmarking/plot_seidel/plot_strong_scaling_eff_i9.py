import utils
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    size = 10000
    nruns = 20
    threads = [2, 4, 8, 12, 13, 14, 15, 16]
    files = ["{}_{}_{}.txt".format(str(size), str(nruns), t) for t in threads]

    threads = [int(file.split("_")[2][:-4]) for file in files]
    version = "mid_presentation/i9_combine"

    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/seq_optimized/2022-12-15-16-40-58", ["{}_{}_{}.txt".format(str(size), str(nruns), "1")], confidence, median_percent)
    omp_par_seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_parallelize_seq_opt/2022-12-15-16-40-58", files, confidence, median_percent)
    omp_best_multitime = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_multitime_better_ssa_unroll2/2022-12-15-16-40-58", files, confidence, median_percent)
    omp_best_singletime = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_singletime/2022-12-15-16-40-58", files, confidence, median_percent)
    mpi_insiderank_serial = utils.get_medians_for_experiment(f"../data/seidel/{version}/mpi_insiderank_serial/2022-12-15-16-40-58", files, confidence, median_percent)
    mpi_insiderank_seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/mpi_insiderank_optimisedserial/2022-12-15-16-40-58", files, confidence, median_percent)

    baseline = seq_opt[0]

    omp_par_seq_opt = baseline / np.array(omp_par_seq_opt)
    omp_best_singletime = baseline / np.array(omp_best_singletime)
    omp_best_multitime = baseline / np.array(omp_best_multitime)
    mpi_insiderank_serial = baseline / np.array(mpi_insiderank_serial)
    mpi_insiderank_seq_opt = baseline / np.array(mpi_insiderank_seq_opt)

    omp_par_seq_opt = omp_par_seq_opt / np.array(threads) * 100
    omp_best_multitime = omp_best_multitime / np.array(threads) * 100
    omp_best_singletime = omp_best_singletime / np.array(threads) * 100
    mpi_insiderank_serial = mpi_insiderank_serial / np.array(threads) * 100
    mpi_insiderank_seq_opt = mpi_insiderank_seq_opt / np.array(threads) * 100
    print("\nRaw strong scaling efficiency data:")


    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Strong scaling efficiency of seidel versions\n(N = 10000, T = 20)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Efficiency [%]", color="black")
    ax1.xaxis.set_major_formatter(tick.FormatStrFormatter("%d"))

    ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    ax1.plot(threads, omp_best_multitime, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP multi time")
    ax1.plot(threads, omp_best_singletime, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP single time")
    ax1.plot(threads, omp_par_seq_opt, color="silver", mfc="silver", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP sequential opt")
    ax1.plot(threads, mpi_insiderank_serial, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="MPI")
    ax1.plot(threads, mpi_insiderank_seq_opt, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="MPI w/ sequential opt")


    ax1.legend()

    plt.xticks(threads)
    
    plt.savefig("seidel_strong_scaling_i9.png")

