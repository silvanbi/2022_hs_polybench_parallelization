import utils
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.1

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["8000_40_1.txt", "8000_40_2.txt", "8000_40_4.txt", "8000_40_8.txt", "8000_40_16.txt", "8000_40_32.txt", "8000_40_64.txt", "8000_40_128.txt"]
    threads = [int(file.split("_")[2][:-4]) for file in files]

    original = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/seq_original/2023-01-11-16-05-16", ["8000_40_1.txt"], confidence, median_percent)
    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/weak_scaling/seq_optimized/2023-01-11-16-05-17", ["8000_40_1.txt"], confidence, median_percent)
    omp_mt = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/strong_scaling/omp_wave_multitime_better_ssa_unroll2/2023-01-11-23-36-17", files, confidence, median_percent)
    omp_st = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/strong_scaling/omp_wave_singletime_auto_ssa/2023-01-11-23-36-17", files, confidence, median_percent)
    mpi = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/strong_scaling/mpi_insiderank_serial/2023-01-11-23-36-17", files, confidence, median_percent)
    mpi_opt = utils.get_medians_for_experiment(f"../data/seidel/report/euler/measurements/strong_scaling/mpi_insiderank_optimisedserial/2023-01-11-17-07-18", files, confidence, median_percent)


    baseline = seq_opt[0]
    omp_mt = baseline / np.array(omp_mt)
    omp_st = baseline / np.array(omp_st)
    mpi = baseline / np.array(mpi)
    mpi_opt = baseline / np.array(mpi_opt)
    original = baseline / np.array(original)


    # Plot
    fig1, ax1 = plt.subplots(figsize=(7, 4))
    ax1.set_title("Speedup - Euler \n(N = 8000, T = 40, Baseline: SO) ")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Speedup [x]", color="black")

    ax1.set_xscale("log", base=2)
    ax1.set_yscale("log", base=2)
    ax1.set_xticks(2 ** np.arange(0, 5))
    ax1.set_yticks(2.0 ** (np.arange(-2, 8)))
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax1.xaxis.set_major_formatter(FormatStrFormatter('%d'))

    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)

    ax1.plot(1, original[0], color="red", mfc="red", mec="red", linestyle=":", linewidth=0.8, marker="o", markersize=6, label="Base Polybench", zorder=10)
    ax1.plot(threads, omp_mt, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="OpenMP MT")  
    ax1.plot(threads, omp_st, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP ST")   
    ax1.plot(threads, mpi_opt, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="MPI w/ SO")
    ax1.plot(threads, mpi, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="MPI")

    ax1.plot(threads, threads, color="black", linestyle="-", linewidth=1.0, marker=",", label="Linear speedup")


    ax1.legend()

    plt.xticks(threads)
    plt.show()
    

