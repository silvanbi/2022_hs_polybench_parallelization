import utils
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["6000_80_1.txt", "8000_60_1.txt", "10000_40_1.txt", "12000_20_1.txt"]
    sizes = [int(file.split("_")[0]) for file in files]
    timesteps = [int(file.split("_")[1]) for file in files]
    version = "mid_presentation/euler"

    peak_perf = 7.86

    original = utils.get_medians_for_experiment(f"../data/seidel/{version}/sequential", files, confidence, median_percent)
    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/sequential_optimized", files, confidence, median_percent)

    print(original)
    print(seq_opt)

    perf_original = original.copy()
    perf_seq_opt = seq_opt.copy()

    for i in range(len(original)):
        perf_original[i] = 9 * sizes[i]**2 * timesteps[i] / original[i] / 1e9
        perf_seq_opt[i] = 9 * sizes[i]**2 * timesteps[i] / seq_opt[i] / 1e9

    x = np.arange(len(sizes)) * 2  # the label locations
    width = 0.50  # the width of the bars

    fig, ax = plt.subplots()
    ax.set_axisbelow(True)
    ax.grid(axis="x", color="silver", linestyle=":", alpha=0.8)
    ax.grid(axis="y", color="silver", linestyle=":", alpha=0.8)
    rects1 = ax.bar(x - width/2, perf_original, width, label='Original', color="grey")
    rects2 = ax.bar(x + width/2, perf_seq_opt, width, label='Optimized', color="black")

    # # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_xlabel("Input sizes (N, T)", color="black")
    ax.set_ylabel("Performance [GFlop/s]", color="black")

    ax.bar_label(rects1, padding=3, fmt="%.3f", rotation=30, color="grey")
    ax.bar_label(rects2, padding=3, fmt="%.3f", rotation=30, color="black")
    ax.plot(np.arange(-2, 8), peak_perf * np.ones(len(np.arange(-2, 8))), color="black", linestyle="--")

    ax.set_xticks(x, map(lambda a: f"{a[0], a[1]}", zip(sizes, timesteps)))
    ax.legend(loc='upper right', bbox_to_anchor=(1.0, 0.9))
    ax.set_xlim(-1, 7)
    ax.set_ylim(0, 8.2)

    ax.annotate("Teff = 7.86 [GFlop/s]", (-0.5, 7.4))

    ax.set_title("Performance of Seidel-2D sequential versions\n")

    plt.savefig("seidel_seq_performance.png")
