import utils
import matplotlib.pyplot as plt

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["8000_60_1", "8000_60_2", "8000_60_4", "8000_60_8", "8000_60_16"]
    threads = [int(file.split("_")[2]) for file in files]
    version = "mid_presentation/i9"

    original = utils.get_medians_for_experiment(f"../data/seidel/{version}/original_seidel_no_lsb", ["8000_60"], confidence, median_percent)
    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/seq_opt_seidel_v1", ["8000_60"], confidence, median_percent)
    omp_opt1 = utils.get_medians_for_experiment(f"../data/seidel/{version}/seidel_auto_better_alloc", files, confidence, median_percent)
    omp_opt2 = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_parallelize_seq_opt_v1", files, confidence, median_percent)
    mpi = utils.get_medians_for_experiment(f"../data/seidel/{version}/mpi_seidel", files, confidence, median_percent)

    print("\nRaw runtime data:")
    print(original)
    print(seq_opt)
    print(omp_opt1)
    print(omp_opt2)
    print(mpi)

    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Runtime of Seidel-2D parallelizations\n(N = 8000, T = 60)\n")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Runtime [s]", color="black")

    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)

    ax1.plot([1], original, color="black", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="Original")
    ax1.plot([1], seq_opt, color="dimgray", mfc="dimgray", mec="dimgray", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="Sequential (opt)")
    ax1.plot(threads, omp_opt1, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP (wf)")
    ax1.plot(threads, omp_opt2, color="olive", mfc="olive", mec="olive", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP (seq)")
    ax1.plot(threads, mpi, color="blue", mfc="blue", mec="blue", linestyle=":", linewidth=1.0, marker="s", markersize=6, label="MPI")

    ax1.legend()

    plt.xticks(threads)
    plt.show()
