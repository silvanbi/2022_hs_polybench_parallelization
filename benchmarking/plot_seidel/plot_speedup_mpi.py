import utils
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.ticker import FormatStrFormatter

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["8000_40_1.txt", "8000_40_2.txt", "8000_40_4.txt", "8000_40_8.txt", "8000_40_16.txt", "8000_40_32.txt", "8000_40_64.txt", "8000_40_128.txt"]

    threads = [int(file.split(".")[0].split("_")[2]) for file in files]
    version = "mid_presentation/euler"

    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/sequential_optimized", ["8000_40_1.txt"], confidence, median_percent)
    mpi_serial = utils.get_medians_for_experiment(f"../data/seidel/{version}/mpi_insiderank_serial/2022-12-14-11-53-43", files, confidence, median_percent)
    mpi_opt_serial = utils.get_medians_for_experiment(f"../data/seidel/{version}/mpi_insiderank_optimisedserial/2022-12-14-11-53-45", files, confidence, median_percent)
    
    print("\nRaw runtime data:")
    print(seq_opt)
    print(mpi_serial)
    print(mpi_opt_serial)

    baseline = seq_opt[0]
    mpi_serial = baseline / np.array(mpi_serial)
    mpi_opt_serial = baseline / np.array(mpi_opt_serial)

    print("\nRaw speedup data:")
    print(seq_opt)
    print(mpi_serial)
    print(mpi_opt_serial)

    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Speedup plot of Seidel-2D MPI versions\n(N = 8000, T = 40)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Speedup [x]", color="black")

    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)

    ax1.set_xscale("log", base=2)
    ax1.set_yscale("log", base=2)
    ax1.set_xticks(2 ** np.arange(0, 7))
    ax1.set_yticks(2.0 ** (np.arange(-2, 8)))
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax1.xaxis.set_major_formatter(FormatStrFormatter('%d'))

    ax1.plot(2 ** np.arange(0, 8), 2 ** np.arange(0, 8), color="black", linestyle="-", linewidth=1.0, label="Linear speedup")
    ax1.plot(threads, mpi_serial, color="darkviolet", mfc="darkviolet", mec="darkviolet", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="MPI")
    ax1.plot(threads, mpi_opt_serial, color="blue", mfc="blue", mec="blue", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="MPI w/ sequential opt")

    ax1.legend()

    plt.xticks(threads)

    plt.savefig("seidel_mpi_speedup.png")

