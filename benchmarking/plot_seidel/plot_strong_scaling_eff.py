import utils
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    size = 10000
    nruns = 40
    threads = [2, 4, 8, 16, 32, 64, 128]
    threads2 = [2, 4, 8, 16, 32]
    files = ["{}_{}_{}.txt".format(str(size), str(nruns), t) for t in threads]
    files2 = ["{}_{}_{}.txt".format(str(size), str(nruns), t) for t in threads2]

    threads = [int(file.split("_")[2][:-4]) for file in files]
    version = "mid_presentation/euler"

    seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/sequential_optimized", ["{}_{}_{}.txt".format(str(size), str(nruns), "1")], confidence, median_percent)
    omp_par_seq_opt = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_parallelize_seq_opt/2022-12-15-17-46-52", files2, confidence, median_percent)
    omp_best_multitime = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_multitime_better_ssa_unroll2/2022-12-14-18-21-28", files, confidence, median_percent)
    omp_best_singletime = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_singletime/2022-12-14-18-21-14", files, confidence, median_percent)
    mpi_ = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_multitime_better_ssa_unroll2/2022-12-14-18-21-28", files, confidence, median_percent)
    omp_best_singletime = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_singletime/2022-12-14-18-21-14", files, confidence, median_percent)


    baseline = seq_opt[0]

    omp_par_seq_opt = baseline / np.array(omp_par_seq_opt)
    omp_best_singletime = baseline / np.array(omp_best_singletime)
    omp_best_multitime = baseline / np.array(omp_best_multitime)

    omp_par_seq_opt = omp_par_seq_opt / np.array(threads2) * 100
    omp_best_multitime = omp_best_multitime / np.array(threads) * 100
    omp_best_singletime = omp_best_singletime / np.array(threads) * 100
    print("\nRaw strong scaling efficiency data:")


    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Strong Scaling Efficiency of Seidel-2D OpenMP versions\n(N = 10000, T = 40)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Efficiency [%]", color="black")
    ax1.set_xscale('log', base=2)
    ax1.xaxis.set_major_formatter(tick.FormatStrFormatter("%d"))
    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)

    ax1.plot(threads, omp_best_multitime, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP multi time")
    ax1.plot(threads, omp_best_singletime, color="darkorange", mfc="darkorange", mec="darkorange", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP single time")
    ax1.plot(threads2, omp_par_seq_opt, color="saddlebrown", mfc="saddlebrown", mec="saddlebrown", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP sequential opt")

    ax1.legend()

    plt.xticks(threads)
    
    plt.savefig("seidel_strong_scaling.png")

