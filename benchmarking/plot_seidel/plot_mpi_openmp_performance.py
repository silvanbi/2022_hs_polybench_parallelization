import utils
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["10000_20_2_12.txt", "10000_20_4_6.txt", "10000_20_12_2.txt"]
    files2 = ["10000_20_24.txt"]
    
    sizes = [int(file.split(".")[0].split("_")[0]) for file in files]
    timesteps = [int(file.split(".")[0].split("_")[1]) for file in files]
    MPI_procs = [int(file.split(".")[0].split("_")[2]) for file in files]
    omp_procs = [int(file.split(".")[0].split("_")[3]) for file in files]
    version = "mid_presentation/euler"

    omp = utils.get_medians_for_experiment(f"../data/seidel/{version}/omp_wave_singletime_ssa", files2, confidence, median_percent)
    mpi_omp = utils.get_medians_for_experiment(f"../data/seidel/{version}/mpi_insiderank_openmp_handopt_ssa/2022-12-14-12-09-20", files, confidence, median_percent)
    mpi = utils.get_medians_for_experiment(f"../data/seidel/{version}/mpi_insiderank_optimisedserial", files2, confidence, median_percent)

    for i in range(len(mpi_omp)):
        mpi_omp[i] = 9 * (sizes[i]**2) * timesteps[i] / mpi_omp[i] / 1e9
    
    omp[0] = 9 * (10000**2) * 20 / omp[0] / 1e9
    mpi[0] = 9 * (10000**2) * 20 / mpi[0] / 1e9

    print(omp)
    print(mpi_omp)
    print(mpi)

    mpi_omp = omp + mpi_omp + mpi

    x = np.arange(len(mpi_omp))  # the label locations
    width = 0.50  # the width of the bars

    fig, ax = plt.subplots()
    ax.set_axisbelow(True)
    ax.grid(axis="x", color="silver", linestyle=":", alpha=0.8)
    ax.grid(axis="y", color="silver", linestyle=":", alpha=0.8)
    rects1 = ax.bar(x, mpi_omp, width, color="black")

    # # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_xlabel("MPI / OpenMP thread mix", color="black")
    ax.set_ylabel("Performance [GFlop/sec]", color="black")

    ax.bar_label(rects1, padding=3, fmt="%.3f", rotation=30, color="black")
    # ax.plot(np.arange(-2, 8), peak_perf * np.ones(len(np.arange(-2, 8))), color="black", linestyle="--")

    ax.set_xticks(x, ["pure OMP"] + list(map(lambda a: f"{a[0]} / {a[1]}", zip(MPI_procs, omp_procs))) + ["pure MPI"])
    # ax.set_xlim(-1, 7)
    ax.set_ylim(0, 30)

    # ax.annotate("Peak performance [8.9 GFlop/sec]", (-0.5, 9.2))

    ax.set_title("Performance of MPI + OpenMP seidel on 24 cores\n(N = 10000, T = 20)")

    plt.savefig("seidel_mpi_omp_performance.png")
