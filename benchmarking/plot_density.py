import utils
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy as np

if __name__ == '__main__':
    time_df = utils.get_nolsb_time_dataframe("data/trmm/euler/m_original_trmm", "5088_5088")
    experiment_df = utils.summarize_experiment(time_df)

    density = stats.gaussian_kde(experiment_df)
    xs = np.linspace(experiment_df.min() - 1, experiment_df.max() + 1, 200)

    confidence = 0.99
    median_percent = 0.05

    mean_ci_low, mean, mean_ci_high = utils.arithmetic_mean_ci(experiment_df, confidence)
    median_ci_low, median, median_ci_high = utils.median_ci(experiment_df, confidence)

    print(f"Mean CI:   [{mean_ci_low:.5f}, {mean:.5f}, {mean_ci_high:.5f}]")
    print(f"Median CI: [{median_ci_low:.5f}, {median:.5f}, {median_ci_high:.5f}]")

    # Plot
    fig1, ax1 = plt.subplots()
    ax1.set_title("Experiment density")

    ax1.fill_between(xs, density(xs), color="silver", alpha=0.6)
    ax1.set_xlabel("Completion Time [s]", color="black")
    ax1.set_ylabel("Density", color="black")
    ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    max_density = max(density(xs))
    upper_position = max_density * 0.6
    lower_position = max_density * 0.3
    ax1.plot((median_ci_low, median_ci_high), (upper_position, upper_position),
             color="black", linestyle="-", linewidth=1.0, marker="|")
    ax1.plot((mean_ci_low, mean_ci_high), (lower_position, lower_position),
             color="black", linestyle=":", linewidth=1.0, marker="|")

    ax1.vlines(x=median, ymin=0, ymax=max_density, colors="black", label="Median CI", linestyle="-", linewidth=1.0)
    ax1.vlines(x=mean, ymin=0, ymax=max_density, colors="black", label="Arithmetic mean CI", linestyle=":", linewidth=1.0)

    ax1.legend()

    offset = median * median_percent
    if (median_ci_high < median + offset) and (median_ci_low > median - offset):
        print(f"{confidence * 100}% confidence interval IS within {median_percent * 100}% of the median")
    else:
        print(f"{confidence * 100}% confidence interval is NOT within {median_percent * 100}% of the median")

    plt.show()
