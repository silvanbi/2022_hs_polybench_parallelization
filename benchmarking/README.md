# Benchmarking

This sub-folder contains raw LibSciBench files for all designed measurements as well as supporting scripts to perform statistical analysis upon the data. 

## Setup
To ease the setup of dependencies it is recommended to use Anaconda / Miniconda (tested on [Miniconda](https://docs.conda.io/en/latest/miniconda.html) setup). 
- install latest version of Miniconda
- open Anaconda / Miniconda prompt (command line interface)
- navigate into the benchmarking folder of this repository on your local system
- create environment from the `environment.yml` file by running `conda env create -f environment.yml`
- check environment was correctly created by running `conda env list`
- activate the environment by running `conda activate plotting` (plotting is the name of the environment)

Further useful commands can be found in the following [cheat-sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf).

## Usage
The following statistical analysis is available:

### `plot_variance.py`
Plots variation across processes for the specific experiment (specified in the code - on test_data by default). This enables visual inspection of how fast / slow each process was for the total number of runs of the specific experiment. Additionally, 2 ANOVA tests are performed to inspect if statistically significant difference exists between runs and between processes. Output of these tests is printed into the console. It is expected that the two ANOVA tests fail (there is statistically significant difference - some summarization technique needs to be used).

### `plot_density.py`
Summarises the 2D data into a vector (using max) and plots density of this data along with median (+ its confidence intervals) and arithmetic mean (+ its confidence intervals). Additionally, a test is performed if the confidence interval of the median lies within the 5% of the median. Ideally this should be the case, such that a single number (median) can be used to represent this particular experiment.

`TODO: Check correctness + add further analysis / plots`
