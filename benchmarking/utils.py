"""LibSciBench Utility Functions

This script allows easy parsing of benchmark files produced by LibSciBench that can be further
used for plotting or other statistical analysis.

This file can also be imported as a module and contains the following
functions:
    * get_time_dataframe - returns time DataFrame for specific experiment
"""
import glob
import natsort
import math
import scipy.stats as stats
import numpy as np
import pandas as pd


def get_lsb_time_dataframe(folder: str, experiment: str) -> pd.DataFrame:
    files = glob.glob(f"{folder}*{experiment}*")
    sorted_files = natsort.natsorted(files)
    df_list = []
    for file in sorted_files:
        df = pd.read_csv(file, comment="#", sep='\s+')
        # TODO: Remove .head(4000) - testing dataset only
        df_list.append(df["time"].head(4000))
    return pd.concat(df_list, axis=1)


def get_nolsb_time_dataframe(folder: str, experiment: str) -> pd.DataFrame:
    file = open(f"{folder}/{experiment}", "r").readlines()[1:]
    for i in range(len(file)):
        start = file[i].find(":") + 2
        file[i] = file[i][start:].rstrip()
    df = pd.DataFrame([line.split(",") for line in file]).astype("float64")
    return df.transpose()


def get_medians_for_experiment(exp_folder, exp_files, confidence, median_percent) -> list[float]:
    medians = []
    for exp in exp_files:
        exp_time_df = get_nolsb_time_dataframe(exp_folder, exp)
        exp_summarized_df = summarize_experiment(exp_time_df)
        median_ci_low, median, median_ci_high = median_ci(exp_summarized_df, confidence)
        offset = median * median_percent
        if not ((median_ci_high < median + offset) and (median_ci_low > median - offset)):
            print(f"WARNING: {exp_folder}/{exp} - {confidence * 100}% confidence interval is NOT within {median_percent * 100}% of the median")
        medians.append(median)
    return medians


def summarize_experiment(time_df: pd.DataFrame) -> pd.DataFrame:
    return time_df.max(axis=1)


def arithmetic_mean_ci(exp_df: pd.DataFrame, confidence: float) -> tuple[float, float, float]:
    data = exp_df.to_numpy()
    n = data.size
    mean = np.mean(data)
    std = np.std(data)

    # Normal distribution
    z = stats.norm.ppf(confidence)
    # Student's t-distribution - alternative
    # z = stats.t.ppf(confidence, df=n-1)
    ci_low = mean - z * (std / math.sqrt(n))
    ci_high = mean + z * (std / math.sqrt(n))
    result_norm = ci_low, mean, ci_high

    data = (data,)
    ci = stats.bootstrap(data, np.mean, confidence_level=confidence, random_state=1, method='percentile')
    result_bootstrap = ci.confidence_interval.low, mean, ci.confidence_interval.high

    # Possible to switch between norm / bootstrapping
    return result_bootstrap


def median_ci(exp_df: pd.DataFrame, confidence: float) -> tuple[float, float, float]:
    data = exp_df.to_numpy()
    n = data.size
    median = np.median(data)

    # Normal distribution
    z = stats.norm.ppf(confidence)
    # Student's t-distribution - alternative
    # z = stats.t.ppf(confidence, df=n-1)
    ci_low = int(math.floor(n * 0.5 - math.sqrt(n) * 0.5 * z))
    ci_high = int(math.ceil(n * 0.5 + math.sqrt(n) * 0.5 * z))
    sorted_data = np.sort(data)
    result_norm = sorted_data[ci_low], median, sorted_data[ci_high]

    data = (data,)
    ci = stats.bootstrap(data, np.median, confidence_level=confidence, random_state=1, method='percentile')
    result_bootstrap = ci.confidence_interval.low, median, ci.confidence_interval.high

    # Possible to switch between norm / bootstrapping
    return result_bootstrap

