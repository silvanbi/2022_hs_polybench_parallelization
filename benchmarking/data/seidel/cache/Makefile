UNAME := $(shell uname)

##### Compilers #########################################################################################################

OPT_FLAGS := -O3 -march=native -ffast-math
C_STD := -std=gnu99
UTILS := ../utils/common.c

ifeq (${UNAME}, Darwin)
	COMPILER := clang
	CXX_COMPILER := clang++
	OMP_FLAGS := -I/usr/local/opt/libomp/include -Wl,-rpath,/usr/local/opt/libomp/lib -L/usr/local/opt/libomp/lib -lomp
else
	COMPILER := gcc
	CXX_COMPILER := g++
	OMP_FLAGS := -fopenmp
endif

##### Source files #########################################################################################################

IN_SEQ_ORIGINAL := original_seidel.c

IN_OMP_WAVE_MULTITIME_BETTER_SSA := seidel_auto_better_alloc_ssa.c
IN_OMP_WAVE_MULTITIME_BETTER_SSA_UNROLL2 := seidel_auto_better_alloc_ssa_unroll2.c

##### Source files #########################################################################################################

OUTPUT_FOLDER := ./OUT

OUT_SEQ_ORIGINAL := ${OUTPUT_FOLDER}/seq_original.out

OUT_OMP_WAVE_MULTITIME_BETTER_SSA := ${OUTPUT_FOLDER}/omp_wave_multitime_better_ssa.out
OUT_OMP_WAVE_MULTITIME_BETTER_SSA_UNROLL2 := ${OUTPUT_FOLDER}/omp_wave_multitime_better_ssa_unroll2.out


##### Builds #########################################################################################################

.PHONY: outdir
outdir:
	mkdir -p OUT

.PHONY: buildserial
buildserial: outdir
	${COMPILER} ${OPT_FLAGS} -o ${OUT_SEQ_ORIGINAL}  ${IN_SEQ_ORIGINAL}  ${UTILS} ${C_STD}


.PHONY: buildomp
buildomp: outdir
	${COMPILER} ${OPT_FLAGS} -o ${OUT_OMP_WAVE_MULTITIME_BETTER_SSA}         ${IN_OMP_WAVE_MULTITIME_BETTER_SSA}         ${UTILS} ${OMP_FLAGS} ${C_STD}
	${COMPILER} ${OPT_FLAGS} -o ${OUT_OMP_WAVE_MULTITIME_BETTER_SSA_UNROLL2} ${IN_OMP_WAVE_MULTITIME_BETTER_SSA_UNROLL2} ${UTILS} ${OMP_FLAGS} ${C_STD}

.PHONY: build
build: buildserial buildomp

##### Cleans #########################################################################################################

.PHONY: clean
clean:
	rm -f ${OUTPUT_FOLDER}/*

##### Measures #########################################################################################################

.PHONY: measure
measure: build
