#include <stdio.h>
#include <stdlib.h>
#include "../utils/common.h"


int main(int argc, char** argv)
{
    int n;
    int tsteps;
    int test;

    parse_args(argc, argv, &n, &tsteps, &test);
    double *A = (double*)malloc(n*n*sizeof(double));

    for (int t = 0; t <= tsteps - 1; t++)
    {
        for (int i = 1; i<= n - 2; i++)
        {
            for (int j = 1; j <= n - 2; j++)
            {
                A[(i  )*(n)+j] = (A[(i-1)*n+(j-1)] + A[(i-1)*n+(j)] + A[(i-1)*n+(j+1)]
                                + A[(i  )*n+(j-1)] + A[(i  )*n+(j)] + A[(i  )*n+(j+1)]
                                + A[(i+1)*n+(j-1)] + A[(i+1)*n+(j)] + A[(i+1)*n+(j+1)])/9.0;
            }
        }
    }

    // do something with this
    if (A[2] == 1e5) fprintf(stderr, "%f\n", A[2]);

    free(A);
    return 0;
}