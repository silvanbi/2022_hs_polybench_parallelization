
 Performance counter stats for './OUT/seq_original.out 500 1' (30 runs):

                12      cpu_core/LLC-load-misses/                                     ( +- 80.48% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.003075 +- 0.000291 seconds time elapsed  ( +-  9.46% )


 Performance counter stats for './OUT/seq_original.out 500 5' (30 runs):

                 4      cpu_core/LLC-load-misses/                                     ( +- 66.04% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0079541 +- 0.0000251 seconds time elapsed  ( +-  0.32% )


 Performance counter stats for './OUT/seq_original.out 500 10' (30 runs):

                 9      cpu_core/LLC-load-misses/                                     ( +- 38.46% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

        0.01472591 +- 0.00000741 seconds time elapsed  ( +-  0.05% )


 Performance counter stats for './OUT/seq_original.out 500 20' (30 runs):

                 4      cpu_core/LLC-load-misses/                                     ( +-3447.41% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0286432 +- 0.0000191 seconds time elapsed  ( +-  0.07% )


 Performance counter stats for './OUT/seq_original.out 500 30' (30 runs):

                23      cpu_core/LLC-load-misses/                                     ( +-200.06% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.042024 +- 0.000112 seconds time elapsed  ( +-  0.27% )


 Performance counter stats for './OUT/seq_original.out 500 40' (30 runs):

                16      cpu_core/LLC-load-misses/                                     ( +-766.69% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0558248 +- 0.0000320 seconds time elapsed  ( +-  0.06% )


 Performance counter stats for './OUT/seq_original.out 500 50' (30 runs):

                23      cpu_core/LLC-load-misses/                                     ( +-176.52% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0689809 +- 0.0000199 seconds time elapsed  ( +-  0.03% )


 Performance counter stats for './OUT/seq_original.out 1000 1' (30 runs):

                 4      cpu_core/LLC-load-misses/                                     ( +-4290.92% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.009437 +- 0.000172 seconds time elapsed  ( +-  1.82% )


 Performance counter stats for './OUT/seq_original.out 1000 5' (30 runs):

                15      cpu_core/LLC-load-misses/                                     ( +-851.66% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0312280 +- 0.0000617 seconds time elapsed  ( +-  0.20% )


 Performance counter stats for './OUT/seq_original.out 1000 10' (30 runs):

             1’319      cpu_core/LLC-load-misses/                                     ( +- 18.79% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0588376 +- 0.0000253 seconds time elapsed  ( +-  0.04% )


 Performance counter stats for './OUT/seq_original.out 1000 20' (30 runs):

                20      cpu_core/LLC-load-misses/                                     ( +-602.67% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1142382 +- 0.0000170 seconds time elapsed  ( +-  0.01% )


 Performance counter stats for './OUT/seq_original.out 1000 30' (30 runs):

                 7      cpu_core/LLC-load-misses/                                     ( +-1222.13% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1695245 +- 0.0000340 seconds time elapsed  ( +-  0.02% )


 Performance counter stats for './OUT/seq_original.out 1000 40' (30 runs):

                24      cpu_core/LLC-load-misses/                                     ( +-345.35% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.2247663 +- 0.0000223 seconds time elapsed  ( +-  0.01% )


 Performance counter stats for './OUT/seq_original.out 1000 50' (30 runs):

                48      cpu_core/LLC-load-misses/                                     ( +-162.39% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.2800813 +- 0.0000294 seconds time elapsed  ( +-  0.01% )


 Performance counter stats for './OUT/seq_original.out 2000 1' (30 runs):

             3’043      cpu_core/LLC-load-misses/                                     ( +- 17.61% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.036960 +- 0.000130 seconds time elapsed  ( +-  0.35% )


 Performance counter stats for './OUT/seq_original.out 2000 5' (30 runs):

            13’810      cpu_core/LLC-load-misses/                                     ( +- 64.80% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1263732 +- 0.0000810 seconds time elapsed  ( +-  0.06% )


 Performance counter stats for './OUT/seq_original.out 2000 10' (30 runs):

            14’494      cpu_core/LLC-load-misses/                                     ( +-122.52% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.238112 +- 0.000159 seconds time elapsed  ( +-  0.07% )


 Performance counter stats for './OUT/seq_original.out 2000 20' (30 runs):

           423’566      cpu_core/LLC-load-misses/                                     ( +-  3.87% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.464400 +- 0.000144 seconds time elapsed  ( +-  0.03% )


 Performance counter stats for './OUT/seq_original.out 2000 30' (30 runs):

           625’116      cpu_core/LLC-load-misses/                                     ( +-  6.96% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.688175 +- 0.000358 seconds time elapsed  ( +-  0.05% )


 Performance counter stats for './OUT/seq_original.out 2000 40' (30 runs):

            18’461      cpu_core/LLC-load-misses/                                     ( +-396.75% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.910272 +- 0.000622 seconds time elapsed  ( +-  0.07% )


 Performance counter stats for './OUT/seq_original.out 2000 50' (30 runs):

            20’255      cpu_core/LLC-load-misses/                                     ( +-293.34% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          1.138264 +- 0.000521 seconds time elapsed  ( +-  0.05% )


 Performance counter stats for './OUT/seq_original.out 4000 1' (30 runs):

            17’469      cpu_core/LLC-load-misses/                                     ( +- 14.56% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1501234 +- 0.0000650 seconds time elapsed  ( +-  0.04% )


 Performance counter stats for './OUT/seq_original.out 4000 5' (30 runs):

            42’506      cpu_core/LLC-load-misses/                                     ( +-105.51% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.512168 +- 0.000437 seconds time elapsed  ( +-  0.09% )


 Performance counter stats for './OUT/seq_original.out 4000 10' (30 runs):

         1’115’555      cpu_core/LLC-load-misses/                                     ( +-  7.37% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.965253 +- 0.000790 seconds time elapsed  ( +-  0.08% )


 Performance counter stats for './OUT/seq_original.out 4000 20' (30 runs):

         1’858’462      cpu_core/LLC-load-misses/                                     ( +-  8.51% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           1.86825 +- 0.00160 seconds time elapsed  ( +-  0.09% )


 Performance counter stats for './OUT/seq_original.out 4000 30' (30 runs):

            54’478      cpu_core/LLC-load-misses/                                     ( +-436.36% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           2.76649 +- 0.00243 seconds time elapsed  ( +-  0.09% )


 Performance counter stats for './OUT/seq_original.out 4000 40' (30 runs):

           695’645      cpu_core/LLC-load-misses/                                     ( +- 37.36% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           3.66310 +- 0.00246 seconds time elapsed  ( +-  0.07% )


 Performance counter stats for './OUT/seq_original.out 4000 50' (30 runs):

            58’430      cpu_core/LLC-load-misses/                                     ( +-518.97% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           4.56644 +- 0.00284 seconds time elapsed  ( +-  0.06% )


 Performance counter stats for './OUT/seq_original.out 8000 1' (30 runs):

            29’085      cpu_core/LLC-load-misses/                                     ( +- 19.39% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.599663 +- 0.000214 seconds time elapsed  ( +-  0.04% )


 Performance counter stats for './OUT/seq_original.out 8000 5' (30 runs):

         1’854’358      cpu_core/LLC-load-misses/                                     ( +-  7.26% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           2.04557 +- 0.00175 seconds time elapsed  ( +-  0.09% )


 Performance counter stats for './OUT/seq_original.out 8000 10' (30 runs):

           390’609      cpu_core/LLC-load-misses/                                     ( +- 57.91% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           3.85031 +- 0.00280 seconds time elapsed  ( +-  0.07% )


 Performance counter stats for './OUT/seq_original.out 8000 20' (30 runs):

         1’010’027      cpu_core/LLC-load-misses/                                     ( +- 16.62% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           7.43596 +- 0.00197 seconds time elapsed  ( +-  0.03% )


 Performance counter stats for './OUT/seq_original.out 8000 30' (30 runs):

           112’918      cpu_core/LLC-load-misses/                                     ( +-223.64% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          11.04465 +- 0.00305 seconds time elapsed  ( +-  0.03% )


 Performance counter stats for './OUT/seq_original.out 8000 40' (30 runs):

           122’042      cpu_core/LLC-load-misses/                                     ( +-190.93% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          14.63655 +- 0.00283 seconds time elapsed  ( +-  0.02% )


 Performance counter stats for './OUT/seq_original.out 8000 50' (30 runs):

           107’341      cpu_core/LLC-load-misses/                                     ( +- 62.85% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         18.221191 +- 0.000930 seconds time elapsed  ( +-  0.01% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 500 1' (30 runs):

                 6      cpu_core/LLC-load-misses/                                     ( +-635.00% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0031514 +- 0.0000106 seconds time elapsed  ( +-  0.34% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 500 5' (30 runs):

                 5      cpu_core/LLC-load-misses/                                     ( +-466.65% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.007612 +- 0.000197 seconds time elapsed  ( +-  2.59% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 500 10' (30 runs):

                 2      cpu_core/LLC-load-misses/                                     ( +-176.01% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0118803 +- 0.0000161 seconds time elapsed  ( +-  0.14% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 500 20' (30 runs):

               135      cpu_core/LLC-load-misses/                                     ( +- 25.59% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.019327 +- 0.000101 seconds time elapsed  ( +-  0.52% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 500 30' (30 runs):

                12      cpu_core/LLC-load-misses/                                     ( +- 32.91% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0287906 +- 0.0000114 seconds time elapsed  ( +-  0.04% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 500 40' (30 runs):

                12      cpu_core/LLC-load-misses/                                     ( +- 39.79% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0361366 +- 0.0000190 seconds time elapsed  ( +-  0.05% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 500 50' (30 runs):

                28      cpu_core/LLC-load-misses/                                     ( +- 57.43% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0457443 +- 0.0000260 seconds time elapsed  ( +-  0.06% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 1000 1' (30 runs):

                28      cpu_core/LLC-load-misses/                                     ( +-320.13% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0112709 +- 0.0000288 seconds time elapsed  ( +-  0.26% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 1000 5' (30 runs):

                 5      cpu_core/LLC-load-misses/                                     ( +-553.16% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0269847 +- 0.0000842 seconds time elapsed  ( +-  0.31% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 1000 10' (30 runs):

                31      cpu_core/LLC-load-misses/                                     ( +-223.00% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0458665 +- 0.0000388 seconds time elapsed  ( +-  0.08% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 1000 20' (30 runs):

                26      cpu_core/LLC-load-misses/                                     ( +- 49.72% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0736818 +- 0.0000194 seconds time elapsed  ( +-  0.03% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 1000 30' (30 runs):

                19      cpu_core/LLC-load-misses/                                     ( +-336.67% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1132022 +- 0.0000150 seconds time elapsed  ( +-  0.01% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 1000 40' (30 runs):

                15      cpu_core/LLC-load-misses/                                     ( +-593.42% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1420126 +- 0.0000922 seconds time elapsed  ( +-  0.06% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 1000 50' (30 runs):

                21      cpu_core/LLC-load-misses/                                     ( +-320.94% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1813010 +- 0.0000323 seconds time elapsed  ( +-  0.02% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 2000 1' (30 runs):

            16’114      cpu_core/LLC-load-misses/                                     ( +-  1.69% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0446867 +- 0.0000364 seconds time elapsed  ( +-  0.08% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 2000 5' (30 runs):

            11’171      cpu_core/LLC-load-misses/                                     ( +-  6.07% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.108111 +- 0.000384 seconds time elapsed  ( +-  0.36% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 2000 10' (30 runs):

            14’629      cpu_core/LLC-load-misses/                                     ( +-  5.24% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1831076 +- 0.0000813 seconds time elapsed  ( +-  0.04% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 2000 20' (30 runs):

            11’253      cpu_core/LLC-load-misses/                                     ( +-  9.09% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.2945779 +- 0.0000523 seconds time elapsed  ( +-  0.02% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 2000 30' (30 runs):

            11’202      cpu_core/LLC-load-misses/                                     ( +- 13.75% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.456497 +- 0.000196 seconds time elapsed  ( +-  0.04% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 2000 40' (30 runs):

            10’733      cpu_core/LLC-load-misses/                                     ( +- 26.75% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           0.58474 +- 0.00378 seconds time elapsed  ( +-  0.65% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 2000 50' (30 runs):

            48’169      cpu_core/LLC-load-misses/                                     ( +-  6.96% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            0.8235 +- 0.0114 seconds time elapsed  ( +-  1.38% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 4000 1' (30 runs):

           143’437      cpu_core/LLC-load-misses/                                     ( +-  3.39% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.190353 +- 0.000109 seconds time elapsed  ( +-  0.06% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 4000 5' (30 runs):

            98’697      cpu_core/LLC-load-misses/                                     ( +-  4.92% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           0.44299 +- 0.00169 seconds time elapsed  ( +-  0.38% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 4000 10' (30 runs):

           272’779      cpu_core/LLC-load-misses/                                     ( +-  1.94% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.742878 +- 0.000767 seconds time elapsed  ( +-  0.10% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 4000 20' (30 runs):

           528’728      cpu_core/LLC-load-misses/                                     ( +-  4.20% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           1.30804 +- 0.00530 seconds time elapsed  ( +-  0.41% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 4000 30' (30 runs):

            82’938      cpu_core/LLC-load-misses/                                     ( +- 21.11% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            1.9858 +- 0.0150 seconds time elapsed  ( +-  0.75% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 4000 40' (30 runs):

           271’799      cpu_core/LLC-load-misses/                                     ( +- 12.88% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            2.5402 +- 0.0362 seconds time elapsed  ( +-  1.43% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 4000 50' (30 runs):

            84’137      cpu_core/LLC-load-misses/                                     ( +- 46.31% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            3.2984 +- 0.0590 seconds time elapsed  ( +-  1.79% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 8000 1' (30 runs):

           614’092      cpu_core/LLC-load-misses/                                     ( +-  4.31% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            0.8995 +- 0.0102 seconds time elapsed  ( +-  1.13% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 8000 5' (30 runs):

         1’161’625      cpu_core/LLC-load-misses/                                     ( +-  6.91% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            2.4245 +- 0.0105 seconds time elapsed  ( +-  0.43% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 8000 10' (30 runs):

         1’840’787      cpu_core/LLC-load-misses/                                     ( +-  3.55% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           3.35981 +- 0.00549 seconds time elapsed  ( +-  0.16% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 8000 20' (30 runs):

           399’227      cpu_core/LLC-load-misses/                                     ( +- 25.78% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           5.75193 +- 0.00589 seconds time elapsed  ( +-  0.10% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 8000 30' (30 runs):

           234’089      cpu_core/LLC-load-misses/                                     ( +- 18.85% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            8.2781 +- 0.0233 seconds time elapsed  ( +-  0.28% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 8000 40' (30 runs):

           618’589      cpu_core/LLC-load-misses/                                     ( +-  3.30% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           10.0773 +- 0.0173 seconds time elapsed  ( +-  0.17% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa.out 8000 50' (30 runs):

           481’912      cpu_core/LLC-load-misses/                                     ( +-  7.35% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           12.7497 +- 0.0441 seconds time elapsed  ( +-  0.35% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 500 1' (30 runs):

                 7      cpu_core/LLC-load-misses/                                     ( +-427.62% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0031875 +- 0.0000515 seconds time elapsed  ( +-  1.62% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 500 5' (30 runs):

                13      cpu_core/LLC-load-misses/                                     ( +-440.10% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0070852 +- 0.0000233 seconds time elapsed  ( +-  0.33% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 500 10' (30 runs):

                 6      cpu_core/LLC-load-misses/                                     ( +-367.75% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0096198 +- 0.0000727 seconds time elapsed  ( +-  0.76% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 500 20' (30 runs):

                11      cpu_core/LLC-load-misses/                                     ( +-350.85% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.013900 +- 0.000225 seconds time elapsed  ( +-  1.62% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 500 30' (30 runs):

                 6      cpu_core/LLC-load-misses/                                     ( +-472.09% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.017606 +- 0.000211 seconds time elapsed  ( +-  1.20% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 500 40' (30 runs):

                 5      cpu_core/LLC-load-misses/                                     ( +- 63.60% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.020366 +- 0.000138 seconds time elapsed  ( +-  0.68% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 500 50' (30 runs):

                38      cpu_core/LLC-load-misses/                                     ( +- 59.65% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.024764 +- 0.000245 seconds time elapsed  ( +-  0.99% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 1000 1' (30 runs):

                 9      cpu_core/LLC-load-misses/                                     ( +-1129.68% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0111419 +- 0.0000845 seconds time elapsed  ( +-  0.76% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 1000 5' (30 runs):

                11      cpu_core/LLC-load-misses/                                     ( +-346.62% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0267539 +- 0.0000239 seconds time elapsed  ( +-  0.09% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 1000 10' (30 runs):

                57      cpu_core/LLC-load-misses/                                     ( +-133.72% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.036235 +- 0.000150 seconds time elapsed  ( +-  0.41% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 1000 20' (30 runs):

                27      cpu_core/LLC-load-misses/                                     ( +-140.46% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.050823 +- 0.000312 seconds time elapsed  ( +-  0.61% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 1000 30' (30 runs):

                50      cpu_core/LLC-load-misses/                                     ( +-196.07% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.063888 +- 0.000211 seconds time elapsed  ( +-  0.33% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 1000 40' (30 runs):

                43      cpu_core/LLC-load-misses/                                     ( +-110.88% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.079495 +- 0.000353 seconds time elapsed  ( +-  0.44% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 1000 50' (30 runs):

                35      cpu_core/LLC-load-misses/                                     ( +-513.11% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.104276 +- 0.000690 seconds time elapsed  ( +-  0.66% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 2000 1' (30 runs):

            16’228      cpu_core/LLC-load-misses/                                     ( +-  2.30% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.0434620 +- 0.0000572 seconds time elapsed  ( +-  0.13% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 2000 5' (30 runs):

            22’439      cpu_core/LLC-load-misses/                                     ( +-  1.98% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.106852 +- 0.000183 seconds time elapsed  ( +-  0.17% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 2000 10' (30 runs):

            23’170      cpu_core/LLC-load-misses/                                     ( +-  2.27% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.143035 +- 0.000136 seconds time elapsed  ( +-  0.09% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 2000 20' (30 runs):

            27’031      cpu_core/LLC-load-misses/                                     ( +-  2.97% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.201296 +- 0.000593 seconds time elapsed  ( +-  0.29% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 2000 30' (30 runs):

            12’460      cpu_core/LLC-load-misses/                                     ( +- 18.12% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           0.32571 +- 0.00273 seconds time elapsed  ( +-  0.84% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 2000 40' (30 runs):

            40’187      cpu_core/LLC-load-misses/                                     ( +-  8.29% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           0.54304 +- 0.00104 seconds time elapsed  ( +-  0.19% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 2000 50' (30 runs):

            44’455      cpu_core/LLC-load-misses/                                     ( +-  7.91% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           0.78191 +- 0.00487 seconds time elapsed  ( +-  0.62% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 4000 1' (30 runs):

            81’927      cpu_core/LLC-load-misses/                                     ( +-  6.57% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

         0.1848660 +- 0.0000685 seconds time elapsed  ( +-  0.04% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 4000 5' (30 runs):

            87’546      cpu_core/LLC-load-misses/                                     ( +-  5.20% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.431616 +- 0.000547 seconds time elapsed  ( +-  0.13% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 4000 10' (30 runs):

           135’662      cpu_core/LLC-load-misses/                                     ( +-  8.85% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

          0.603965 +- 0.000450 seconds time elapsed  ( +-  0.07% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 4000 20' (30 runs):

           121’060      cpu_core/LLC-load-misses/                                     ( +- 22.01% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            1.2023 +- 0.0271 seconds time elapsed  ( +-  2.26% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 4000 30' (30 runs):

           589’898      cpu_core/LLC-load-misses/                                     ( +-  5.76% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            2.0117 +- 0.0326 seconds time elapsed  ( +-  1.62% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 4000 40' (30 runs):

            86’301      cpu_core/LLC-load-misses/                                     ( +- 52.33% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            2.7273 +- 0.0492 seconds time elapsed  ( +-  1.80% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 4000 50' (30 runs):

            83’794      cpu_core/LLC-load-misses/                                     ( +- 49.66% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            3.5568 +- 0.0519 seconds time elapsed  ( +-  1.46% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 8000 1' (30 runs):

           962’451      cpu_core/LLC-load-misses/                                     ( +-  1.97% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           0.84970 +- 0.00902 seconds time elapsed  ( +-  1.06% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 8000 5' (30 runs):

         1’456’883      cpu_core/LLC-load-misses/                                     ( +-  5.01% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           1.98454 +- 0.00531 seconds time elapsed  ( +-  0.27% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 8000 10' (30 runs):

         1’678’388      cpu_core/LLC-load-misses/                                     ( +-  6.83% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            3.1952 +- 0.0501 seconds time elapsed  ( +-  1.57% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 8000 20' (30 runs):

           934’435      cpu_core/LLC-load-misses/                                     ( +-  9.30% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            5.0121 +- 0.0791 seconds time elapsed  ( +-  1.58% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 8000 30' (30 runs):

           520’040      cpu_core/LLC-load-misses/                                     ( +- 13.77% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            7.1439 +- 0.0800 seconds time elapsed  ( +-  1.12% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 8000 40' (30 runs):

           629’224      cpu_core/LLC-load-misses/                                     ( +-  9.16% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

            9.8155 +- 0.0757 seconds time elapsed  ( +-  0.77% )


 Performance counter stats for './OUT/omp_wave_multitime_better_ssa_unroll2.out 8000 50' (30 runs):

           981’690      cpu_core/LLC-load-misses/                                     ( +-  5.14% )
     <not counted>      cpu_atom/LLC-load-misses/                                     (0.00%)

           12.8603 +- 0.0850 seconds time elapsed  ( +-  0.66% )

