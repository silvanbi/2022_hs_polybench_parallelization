#!/bin/bash

RES_FILE=res.txt

SIZES=(500 1000 2000 4000 8000)
TIMES=(1 5 10 20 30 40 50)

export OMP_NUM_THREADS=1

touch ${RES_FILE}
rm ${RES_FILE}
touch ${RES_FILE}

make measure

for i in ${!SIZES[@]}
do
    for j in ${!TIMES[@]}
    do
        perf stat -e LLC-load-misses -r 30 ./OUT/seq_original.out ${SIZES[$i]} ${TIMES[$j]} 2>> ${RES_FILE}
    done
done

for i in ${!SIZES[@]}
do
    for j in ${!TIMES[@]}
    do
        perf stat -e LLC-load-misses -r 30 ./OUT/omp_wave_multitime_better_ssa.out ${SIZES[$i]} ${TIMES[$j]} 2>> ${RES_FILE}
    done
done

for i in ${!SIZES[@]}
do
    for j in ${!TIMES[@]}
    do
        perf stat -e LLC-load-misses -r 30 ./OUT/omp_wave_multitime_better_ssa_unroll2.out ${SIZES[$i]} ${TIMES[$j]} 2>> ${RES_FILE}
    done
done
