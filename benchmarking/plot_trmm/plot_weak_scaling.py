import utils
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["3680_3680_1.txt", "4640_4640_2.txt", "5848_5848_4.txt", "7368_7368_8.txt", "9288_9288_16.txt", "11704_11704_32.txt", "14752_14752_64.txt", "18600_18600_128.txt"]
    threads = [int(file.split("_")[2][:-4]) for file in files]
    ns = [int(file.split("_")[0]) for file in files]
    version = "euler/report"

    original = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_original_trmm", ["3680_3680_1.txt"], confidence, median_percent)
    blas = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/mkl_blas_trmm", ["3680_3680_1.txt"], confidence, median_percent)
    omp_blas = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_mkl_blas_p_trmm", files, confidence, median_percent)
    omp_opt1 = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_omp_trmm12", files, confidence, median_percent)
    omp_opt2 = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_omp_trmm13", files, confidence, median_percent)
    omp_opt3 = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_omp_trmm14", files, confidence, median_percent)
    mpi = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_mpi_trmm_opt3", files, confidence, median_percent)
    pblas_rm = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_pblas_mkl_rm_trmm_weak", files, confidence, median_percent)
    pblas_cm = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_pblas_mkl_cm_trmm_weak", files, confidence, median_percent)

    # Plot
    baseline = blas[0]

    omp_blas = (baseline / np.array(omp_blas)) * 100
    omp_opt1 = (baseline / np.array(omp_opt1)) * 100
    omp_opt2 = (baseline / np.array(omp_opt2)) * 100
    omp_opt3 = (baseline / np.array(omp_opt3)) * 100
    mpi = (baseline / np.array(mpi)) * 100
    pblas_rm = (baseline / np.array(pblas_rm)) * 100
    pblas_cm = (baseline / np.array(pblas_cm)) * 100

    fig1, ax1 = plt.subplots(figsize=(7, 4))
    ax1.set_title("Weak Scaling Efficiency - Euler\n(Baseline: Base MKL BLAS)")
    # ax1.set_title("Weak Scaling Runtime - Euler")

    ax1.set_xlabel("Input size (M = N)", color="black")
    ax1.set_ylabel("Efficiency [%]", color="black")
    # ax1.set_ylabel("Runtime [s]", color="black")
    ax1.set_xscale('log', base=2)
    ax1.xaxis.set_major_formatter(tick.FormatStrFormatter("%d"))

    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)

    # ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    # ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    ax1.plot(ns[0], (baseline / original) * 100, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="Base PolyBench")
    ax1.plot(ns[0], (baseline / blas) * 100, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="Base MKL BLAS")
    ax1.plot(ns, omp_blas, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="OpenMP MKL BLAS")
    # ax1.plot(ns, omp_opt1, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP Opt1")
    ax1.plot(ns, omp_opt2, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP Opt2")
    # ax1.plot(ns, omp_opt3, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP Opt3")
    ax1.plot(ns, pblas_cm, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="PBLAS MKL CM")
    ax1.plot(ns, pblas_rm, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="PBLAS MKL RM")
    ax1.plot(ns, mpi, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="s", markersize=6, label="MPI Opt1")

    ax1.legend()

    plt.xticks(ns)
    plt.show()

