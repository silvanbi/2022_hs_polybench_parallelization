import utils
import matplotlib.pyplot as plt

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["5088_5088_1", "5088_5088_8", "5088_5088_16", "5088_5088_24", "5088_5088_32"]
    threads = [int(file.split("_")[2]) for file in files]
    version = "euler/mid_presentation"

    original = utils.get_medians_for_experiment(f"../data/trmm/{version}/m_original_trmm", ["5088_5088"], confidence, median_percent)
    blas = utils.get_medians_for_experiment(f"../data/trmm/{version}/m_blas_trmm", ["5088_5088"], confidence, median_percent)
    omp_opt2 = utils.get_medians_for_experiment(f"../data/trmm/{version}/m_omp_trmm2", files, confidence, median_percent)
    omp_opt6 = utils.get_medians_for_experiment(f"../data/trmm/{version}/m_omp_trmm6", files, confidence, median_percent)
    omp_opt7 = utils.get_medians_for_experiment(f"../data/trmm/{version}/m_omp_trmm7", files, confidence, median_percent)
    mpi_opt2 = utils.get_medians_for_experiment(f"../data/trmm/{version}/m_mpi_trmm_opt2", files, confidence, median_percent)

    print("\nRaw runtime data:")
    print(original)
    print(blas)
    print(omp_opt2)
    print(omp_opt6)
    print(omp_opt7)
    print(mpi_opt2)

    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Runtime of Trmm parallelizations\n(N = 5088, M = 5088)(TODO correct?)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Runtime [s]", color="black")
    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)
    #ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    #ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    #ax1.plot([1], original, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="Original")
    #ax1.plot([1], blas, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="BLAS (GSL)")
    #ax1.plot(threads, omp_opt6, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP (opt6)")
    #ax1.plot(threads, mpi_opt2, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="s", markersize=6, label="MPI (opt2)")

    ax1.plot([1], original, color="black", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="Original")
    ax1.plot([1], blas, color="dimgray", mfc="dimgray", mec="dimgray", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="BLAS (GSL)")
    ax1.plot(threads, omp_opt6, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP (opt6)")
    ax1.plot(threads, mpi_opt2, color="blue", mfc="blue", mec="blue", linestyle=":", linewidth=1.0, marker="s", markersize=6, label="MPI (opt2)")

    ax1.legend()

    plt.xticks(threads)
    plt.show()

    # Comparison between OMP versions
    # # Plot
    # fig1, ax1 = plt.subplots()
    # ax1.set_title("Runtime")
    #
    # ax1.set_xlabel("Threads", color="black")
    # ax1.set_ylabel("Runtime [s]", color="black")
    #
    # ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    # ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)
    #
    # ax1.plot(threads, omp_opt2, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="OpenMP (opt2)")
    # ax1.plot(threads, omp_opt6, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP (opt6)")
    # ax1.plot(threads, omp_opt7, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP (opt7)")
    #
    # ax1.legend()
    #
    # plt.xticks(threads)
    # plt.show()
