import utils
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["3680_3680_1.txt"]
    sizes = [int(file.split("_")[0]) for file in files]
    timesteps = [int(file.split("_")[1]) for file in files]
    version = "i9/report"

    original = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_original_trmm", files, confidence, median_percent)
    blas_opt = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_mkl_blas_p_trmm", files, confidence, median_percent)
    seq_opt = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_omp_trmm13", files, confidence, median_percent)
    seq_opt_icx = utils.get_medians_for_experiment(f"../data/trmm/{version}/weak_scaling/m_omp_trmm13_icx", files, confidence, median_percent)

    perf_original = original.copy()
    perf_blas = blas_opt.copy()
    perf_seq_opt = seq_opt.copy()
    perf_seq_opt_icx = seq_opt_icx.copy()

    for i in range(len(original)):
        # Assume m=n
        flops = sizes[i]**2 * sizes[i] + sizes[i]**2
        perf_original[i] = flops / original[i] / 1e9
        perf_blas[i] = flops / blas_opt[i] / 1e9
        perf_seq_opt[i] = flops / seq_opt[i] / 1e9
        perf_seq_opt_icx[i] = flops / seq_opt_icx[i] / 1e9

    x = np.arange(len(sizes)) * 2  # the label locations
    width = 0.6  # the width of the bars

    fig, ax = plt.subplots(figsize=(7, 4))
    ax.set_axisbelow(True)
    ax.set_facecolor('#EBEBEB')
    ax.grid(axis="y", color="white", linewidth=1)
    rects1 = ax.bar(x - 3 * (width/2.0), perf_original, width, label='Base PolyBench', color="salmon")
    rects2 = ax.bar(x - 1 * (width/2.0), perf_seq_opt, width, label='OpenMP Opt2 GCC', color="tomato")
    rects3 = ax.bar(x + 1 * (width/2.0), perf_seq_opt_icx, width, label='OpenMP Opt2 ICX', color="red")
    rects4 = ax.bar(x + 3 * (width/2.0), perf_blas, width, label='Base MKL BLAS', color="darkred")

    # # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_xlabel("Input size (M = N)", color="black")
    ax.set_ylabel("Performance [GFlop/s]", color="black")

    ax.bar_label(rects1, padding=3, fmt="%.1f", rotation=30, color="black")
    ax.bar_label(rects2, padding=3, fmt="%.1f", rotation=30, color="black")
    ax.bar_label(rects3, padding=3, fmt="%.1f", rotation=30, color="black")
    ax.bar_label(rects4, padding=3, fmt="%.1f", rotation=30, color="black")
    ax.plot(np.arange(-5, 51.2), 51.2 * np.ones(len(np.arange(-5, 51.2))), color="gray", linestyle=":")
    ax.plot(np.arange(-5, 12.8), 12.8 * np.ones(len(np.arange(-5, 12.8))), color="gray", linestyle=":")

    ax.set_xticks(x, sizes)
    ax.legend(loc='upper right', bbox_to_anchor=(1.0, 0.9))
    ax.set_xlim(-3, 5)
    ax.set_ylim(0, 55)

    ax.annotate("Peak [51.2 GFlop/s]", (-2.8, 52.1))
    ax.annotate("Non-vector [12.8 GFlop/s]", (-2.8, 13.8))

    ax.set_title("Sequential Performance - i9")

    plt.show()
