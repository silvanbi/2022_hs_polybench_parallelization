import utils
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["8192_8192_1.txt", "8192_8192_2.txt", "8192_8192_4.txt", "8192_8192_8.txt", "8192_8192_16.txt", "8192_8192_32.txt", "8192_8192_64.txt", "8192_8192_128.txt"]
    # files = ["8192_8192_1.txt", "8192_8192_4.txt", "8192_8192_8.txt", "8192_8192_12.txt", "8192_8192_13.txt", "8192_8192_14.txt", "8192_8192_15.txt", "8192_8192_16.txt"]
    threads = [int(file.split("_")[2][:-4]) for file in files]
    version = "euler"

    seq_opt = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_mkl_blas_p_trmm", ["8192_8192_1.txt"], confidence, median_percent)
    blas_p = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_mkl_blas_p_trmm", files, confidence, median_percent)
    omp_opt1 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm12", files, confidence, median_percent)
    omp_opt2 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm13", files, confidence, median_percent)
    omp_opt3 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm14", files, confidence, median_percent)
    mpi = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_mpi_trmm_opt3", files, confidence, median_percent)
    pblas = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_pblas_rm_trmm", files[1:], confidence, median_percent)

    baseline = seq_opt[0]

    blas_p = baseline / np.array(blas_p)
    omp_opt1 = baseline / np.array(omp_opt1)
    omp_opt2 = baseline / np.array(omp_opt2)
    omp_opt3 = baseline / np.array(omp_opt3)
    mpi = baseline / np.array(mpi)
    pblas = baseline / np.array(pblas)

    blas_p = blas_p / np.array(threads) * 100
    omp_opt1 = omp_opt1 / np.array(threads) * 100
    omp_opt2 = omp_opt2 / np.array(threads) * 100
    omp_opt3 = omp_opt3 / np.array(threads) * 100
    mpi = mpi / np.array(threads) * 100
    pblas = pblas / np.array(threads)[1:] * 100

    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Strong Scaling Efficiency\n(M = N = 8192)")
    # ax1.set_title("Strong Scaling Runtime\n(M = N = 8192)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Efficiency [%]", color="black")
    # ax1.set_ylabel("Runtime [s]", color="black")
    ax1.set_xscale('log', base=2)
    ax1.xaxis.set_major_formatter(tick.FormatStrFormatter("%d"))

    ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    ax1.plot(threads, blas_p, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="s", markersize=6, label="MKL BLAS")
    # ax1.plot(threads, omp_opt1, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="OpenMP Opt1")
    ax1.plot(threads, omp_opt2, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP Opt2")
    # ax1.plot(threads, omp_opt3, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="OpenMP Opt3")
    ax1.plot(threads, mpi, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="MPI")
    ax1.plot(threads[1:], pblas, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="PBLAS MKL (Row-major)")

    ax1.legend()

    plt.xticks(threads)
    plt.show()

