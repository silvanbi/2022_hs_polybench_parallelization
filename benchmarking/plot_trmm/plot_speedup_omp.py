import utils
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.ticker import FormatStrFormatter

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    # files = ["8192_8192_1.txt", "8192_8192_2.txt", "8192_8192_4.txt", "8192_8192_8.txt", "8192_8192_16.txt", "8192_8192_32.txt", "8192_8192_64.txt", "8192_8192_128.txt"]
    files = ["8192_8192_1.txt", "8192_8192_4.txt", "8192_8192_8.txt", "8192_8192_16.txt"]

    threads = [int(file.split(".")[0].split("_")[2]) for file in files]
    version = "i9"

    seq_opt = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_mkl_blas_trmm", ["8192_8192_1.txt"], confidence, median_percent)
    omp_opt1 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm12", files, confidence, median_percent)
    omp_opt2 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm13", files, confidence, median_percent)
    omp_opt3 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm14", files, confidence, median_percent)
    omp_blas = utils.get_medians_for_experiment( f"../data/trmm/{version}/strong_scaling/m_mkl_blas_p_trmm", files, confidence, median_percent)

    print("\nRaw runtime data:")
    print(seq_opt)
    print(omp_opt1)
    print(omp_opt2)
    print(omp_opt3)
    print(omp_blas)

    baseline = seq_opt[0]
    omp_opt1 = baseline / np.array(omp_opt1)
    omp_opt2 = baseline / np.array(omp_opt2)
    omp_opt3 = baseline / np.array(omp_opt3)
    omp_blas = baseline / np.array(omp_blas)

    print("\nRaw speedup data:")
    print(seq_opt)
    print(omp_opt1)
    print(omp_opt2)
    print(omp_opt3)
    print(omp_blas)

    # Plot
    fig1, ax1 = plt.subplots(figsize=(6, 5))
    ax1.set_title("Speedup plot of OpenMP trmm versions\n(M = N = 8192)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Speedup [x]", color="black")

    ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    ax1.set_xscale("log", base=2)
    ax1.set_yscale("log", base=2)
    ax1.set_xticks(2 ** np.arange(0, 5))
    ax1.set_yticks(2.0 ** (np.arange(-2, 8)))
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax1.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    
    ax1.plot(2 ** np.arange(0, 5), 2 ** np.arange(0, 5), color="black", linestyle="-", linewidth=1.0, label="linear speedup")
    ax1.plot(threads, omp_opt1, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="Opt1")
    ax1.plot(threads, omp_opt2, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="Opt2")
    ax1.plot(threads, omp_opt3, color="silver", mfc="white", mec="black", linestyle=":", linewidth=1.0, marker="h", markersize=6, label="Opt3")
    ax1.plot(threads, omp_blas, color="silver", mfc="black", mec="black", linestyle=":", linewidth=1.0, marker="s", markersize=6, label="MKL BLAS")

    ax1.legend()

    plt.xticks(threads)

    plt.show()

