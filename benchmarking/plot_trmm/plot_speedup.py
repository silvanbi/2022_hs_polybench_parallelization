import utils
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    print(f"All {confidence * 100}% confidence intervals are intended to be within {median_percent * 100}% of the median (warning if not)")

    files = ["8192_8192_1.txt", "8192_8192_2.txt", "8192_8192_4.txt", "8192_8192_8.txt", "8192_8192_16.txt", "8192_8192_32.txt", "8192_8192_64.txt", "8192_8192_128.txt"]
    threads = [int(file.split("_")[2][:-4]) for file in files]
    version = "euler/report"

    # original = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_original_trmm", ["8192_8192_1.txt"], confidence, median_percent)
    blas = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/mkl_blas_trmm", ["8192_8192_1.txt"], confidence, median_percent)
    omp_blas = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_mkl_blas_p_trmm", files, confidence, median_percent)
    omp_opt1 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm12", files, confidence, median_percent)
    omp_opt2 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm13", files, confidence, median_percent)
    omp_opt3 = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_omp_trmm14", files, confidence, median_percent)
    mpi = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_mpi_trmm_opt3", files, confidence, median_percent)
    pblas_rm = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_pblas_mkl_rm_trmm_strong", files[2:], confidence, median_percent)
    pblas_cm = utils.get_medians_for_experiment(f"../data/trmm/{version}/strong_scaling/m_pblas_mkl_cm_trmm_strong", files[2:], confidence, median_percent)

    baseline = blas[0]

    # original = baseline / np.array(original)
    blas = baseline / np.array(blas)
    omp_blas = baseline / np.array(omp_blas)
    omp_opt1 = baseline / np.array(omp_opt1)
    omp_opt2 = baseline / np.array(omp_opt2)
    omp_opt3 = baseline / np.array(omp_opt3)
    mpi = baseline / np.array(mpi)
    pblas_rm = baseline / np.array(pblas_rm)
    pblas_cm = baseline / np.array(pblas_cm)

    # Plot
    fig1, ax1 = plt.subplots(figsize=(7, 4))
    ax1.set_title("Speedup - Euler\n(M = N = 8192, Baseline: Base MKL BLAS)")

    ax1.set_xlabel("Number of threads", color="black")
    ax1.set_ylabel("Speedup [x]", color="black")

    ax1.set_xscale("log", base=2)
    ax1.set_yscale("log", base=2)
    ax1.set_xticks(2 ** np.arange(0, 5))
    ax1.set_yticks(2.0 ** (np.arange(-2, 8)))
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax1.xaxis.set_major_formatter(FormatStrFormatter('%d'))

    ax1.set_facecolor('#EBEBEB')
    ax1.grid(axis="y", color="white", linewidth=1)

    #ax1.grid(axis="x", color="silver", linestyle=":", alpha=0.4)
    #ax1.grid(axis="y", color="silver", linestyle=":", alpha=0.4)

    ax1.plot(2 ** np.arange(0, 8), 2 ** np.arange(0, 8), color="black", linestyle="-", linewidth=1.0)
    ax1.plot(threads[0], baseline / 3072, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="Base PolyBench")
    # ax1.plot(threads[0], blas, color="red", mfc="red", mec="red", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="Base MKL BLAS")
    ax1.plot(threads, omp_blas, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="OpenMP MKL BLAS")
    # ax1.plot(threads, omp_opt1, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP Opt1")
    ax1.plot(threads, omp_opt2, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP Opt2")
    # ax1.plot(threads, omp_opt3, color="forestgreen", mfc="forestgreen", mec="forestgreen", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="OpenMP Opt3")
    ax1.plot(threads[2:], pblas_cm, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="o", markersize=6, label="PBLAS MKL CM")
    ax1.plot(threads[2:], pblas_rm, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="^", markersize=6, label="PBLAS MKL RM")
    ax1.plot(threads, mpi, color="navy", mfc="navy", mec="navy", linestyle=":", linewidth=1.0, marker="s", markersize=6, label="MPI Opt1")

    ax1.legend()

    plt.xticks(threads)
    plt.show()
