import utils
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy as np

if __name__ == '__main__':
    confidence = 0.95
    median_percent = 0.10

    grid = [1, 2, 4, 8, 16, 32]
    blocks = [[2, 80, 159], [2, 159, 318], [2, 318, 636], [2, 318, 636], [2, 159, 318], [2, 80, 159]]
    ticks = []
    data = []

    for i in range(len(grid)):
        grid_n = grid[i]
        grid_m = int(32 / grid_n)
        for block in blocks[i]:
            data.append(utils.get_medians_for_experiment(f"../data/trmm/euler/pblas_grid_block", [f"{grid_n}_{grid_m}_{block}_{block}.txt"], confidence, median_percent))
            ticks.append(block)

    # Plot
    fig1, ax1 = plt.subplots()
    ax1.set_title("Process grid and block size selection (PBLAS)\n (M = N = 5088, Number of threads = 32)")

    ax1.plot(np.arange(0, 3), data[:3], color="silver", mfc="black", mec="black", linestyle="", marker="o", markersize=6, label="1x32")
    ax1.plot(np.arange(3, 6), data[3:6], color="silver", mfc="white", mec="black", linestyle="", marker="^", markersize=6, label="2x16")
    ax1.plot(np.arange(6, 9), data[6:9], color="silver", mfc="black", mec="black", linestyle="", marker="h", markersize=6, label="4x8")
    ax1.plot(np.arange(9, 12), data[9:12], color="silver", mfc="white", mec="black", linestyle="", marker="o", markersize=6, label="8x4")
    ax1.plot(np.arange(12, 15), data[12:15], color="silver", mfc="black", mec="black", linestyle="", marker="s", markersize=6, label="16x2")
    ax1.plot(np.arange(15, 18), data[15:], color="silver", mfc="white", mec="black", linestyle="", marker="h", markersize=6, label="32x1")

    ax1.plot([2.5, 2.5], [0, 3.5], color="silver", linestyle=":", markersize=0)
    ax1.plot([5.5, 5.5], [0, 3.5], color="silver", linestyle=":", markersize=0)
    ax1.plot([8.5, 8.5], [0, 3.5], color="silver", linestyle=":", markersize=0)
    ax1.plot([11.5, 11.5], [0, 3.5], color="silver", linestyle=":", markersize=0)
    ax1.plot([14.5, 14.5], [0, 3.5], color="silver", linestyle=":", markersize=0)

    ax1.set_xlabel("Block size", color="black")
    plt.xticks(np.arange(0, len(data)))
    ax1.set_xticklabels(ticks)
    ax1.set_ylabel("Completion Time [s]", color="black")
    ax1.grid(axis="y", color="silver", linestyle=":")

    ax1.legend()
    plt.show()
