import sys 
from os import listdir
from os.path import isfile, join
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


"""
USAGE:
python3 plot_lineplot.py [plot] [base directory] [nr of threads to plot] [list of nr of threads to plot many thread numbers] [list of directories to plot followed by "1" (plot threads) or "0" (plot only sequential data)]
example: python3 plot_lineplot.py plot_runtime measurements/euler/seidel/2022-11-20-17-23-55/ 4 1 2 4 8 seidel_auto_better_alloc_v2.out/ 1 seq_opt_seidel_v1.out/ 0 plots measurements for 1, 2, 4, 8 threads for seidel_auto_better_alloc_v2.out and sequential execution for seq_opt_seidel_v1.out
"""
def create_long_form_df(data_dir, skip_first_line, use_threads):
    files = [f for f in listdir(data_dir) if isfile(join(data_dir, f))]
    col_n = []
    col_t = []
    col_n_threads = []
    col_runtimes = []
    for f in files:
        params = f.split(".")[0].split("_")
        n = int(params[0])
        t = int(params[1])
        if use_threads:
            n_threads = int(params[2])
        with open(join(data_dir,f)) as of:
            lines = of.readlines()
            if(skip_first_line):
                lines = lines[1:]

            runtimes_str = lines[0].split(": ")[1].strip().split(",")
            runtimes = [float(r) for r in runtimes_str]
            col_n.extend([n]*len(runtimes))
            col_t.extend([t]*len(runtimes))
            if use_threads:
                col_n_threads.extend([n_threads]*len(runtimes))
            col_runtimes.extend(runtimes)
            
    if use_threads:
        data_map = {'N' : col_n, 'T': col_t, 'N_threads' : col_n_threads, 'runtime[s]' : col_runtimes}
    else:
        data_map = {'N' : col_n, 'T': col_t, 'runtime[s]' : col_runtimes}
    df = pd.DataFrame(data_map)
    return df


def create_runtime_plot(n_threads, data_list):
    for (name, data, use_threads) in data_list:
        if not use_threads:
            s = data
            sns.lineplot(data=s, x="N", y="runtime[s]", label=(name), estimator='median')
        else:
            for n in n_threads:
                s = data.loc[(data['N_threads'] == n)]
                sns.lineplot(data=s, x="N", y="runtime[s]", label=(name+"-"+str(n)), estimator='median')
    plt.show()
    
def plot_runtime():
    # parse arguments
    if len(sys.argv) < 2:
        print("too few arguments passed")
    else:
        base_dir = sys.argv[2]

        n_threads_in = int(sys.argv[3])

        threads = []
        c = 4
        for i in range(n_threads_in):
            threads.append(int(sys.argv[c]))
            c += 1
        
        data_list = []
        while c < len(sys.argv) - 1:
            data_dir = sys.argv[c]
            use_threads = False
            if sys.argv[c+1] == "1":
                use_threads = True
            data = create_long_form_df(base_dir+data_dir, True, use_threads)
            data_list.append((data_dir[:-1], data.copy(), use_threads))
            c += 2

        # create the plot
        create_runtime_plot(threads, data_list)

if __name__ == "__main__":

    sns.set_theme(style="darkgrid")
    if sys.argv[1] == "plot_runtime":
        plot_runtime()
    else:
        print("unrecognized command")
   






