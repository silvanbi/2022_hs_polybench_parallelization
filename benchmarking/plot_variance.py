import utils
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy as np

if __name__ == '__main__':
    time_df = utils.get_nolsb_time_dataframe("data/trmm/euler/m_mpi_trmm_opt2", "5088_5088_32")

    # Plot
    fig1, ax1 = plt.subplots()
    ax1.set_title("Variance across processes")
    median_props = dict(linestyle="-", linewidth=2.0, color="black")
    mean_props = dict(marker="D", markeredgecolor="black", markerfacecolor="black", markersize=3.0, solid_capstyle="butt")

    ax1.boxplot(time_df, notch=False, showfliers=False, showmeans=True, medianprops=median_props, meanprops=mean_props)
    ax1.set_xlabel("Processes", color="black")
    ax1.set_xticklabels(np.arange(0, time_df.shape[1]))
    ax1.set_ylabel("Completion Time [s]", color="black")
    ax1.grid(axis="y", color="silver", linestyle=":")

    # ANOVA
    btw_runs = time_df.to_numpy()
    btw_proc = time_df.transpose().to_numpy()

    f_value_runs, p_value_runs = stats.f_oneway(*btw_runs)
    f_value_proc, p_value_proc = stats.f_oneway(*btw_proc)

    anova_runs = "YES" if p_value_runs < 0.05 else "NO"
    anova_proc = "YES" if p_value_proc < 0.05 else "NO"

    print(f"ANOVA test - statistically significant difference between runs: {anova_runs}, p-value: {p_value_runs}")
    print(f"ANOVA test - statistically significant difference between processes: {anova_proc}, p-value: {p_value_proc}")

    plt.show()
